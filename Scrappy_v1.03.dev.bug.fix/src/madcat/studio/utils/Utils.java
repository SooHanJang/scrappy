package madcat.studio.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import madcat.studio.constants.Constants;
import madcat.studio.database.SBooksDBHelper;
import madcat.studio.dialog.DialogAddSBooks;
import madcat.studio.indexing.PageIndex;
import madcat.studio.position.CropViewPos;
import madcat.studio.position.TextViewPos;
import madcat.studio.scrap.R;
import madcat.studio.scrap.ScrapPage;
import madcat.studio.scrapdata.OriginScrapData;
import madcat.studio.scrapdata.SaveScrapData;
import madcat.studio.shelvesbooks.ShelvesBooks;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Debug;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Layout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class Utils {
	
	/**
	 * 파라미터에 해당하는 디버깅 정보 로그를 출력합니다.
	 * 
	 * @param cls
	 * @param text
	 */
	public static void logPrint(Class cls, String text) {
		if(Constants.DEVELOPE_MODE) {
			Log.d(cls.toString(), text);
		}
	}
	
	public static Bitmap getLoadBitmap(Bitmap bm, float newWidth, float newHeight, float degrees) {
		int width = bm.getWidth();
		int height = bm.getHeight();

		float scaleWidth = ((float) newWidth) / width; 
	    float scaleHeight = ((float) newHeight) / height;

		Matrix matrix = new Matrix();
		matrix.postScale(scaleWidth, scaleHeight);
		matrix.postRotate(degrees);
		Bitmap loadBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);

		return loadBitmap;
	}
	
	/**
	 * 파라미터로 넘어온 Bitmap 을 새로운 사이즈로 변경한 뒤 반환합니다.
	 * 
	 * @param bm
	 * @param newWidth
	 * @param newHeight
	 * @return
	 */
	public static Bitmap getResizedBitmap(Bitmap bm, float newWidth, float newHeight) {

		int width = bm.getWidth();
		int height = bm.getHeight();

		float scaleWidth = ((float) newWidth) / width; 
	    float scaleHeight = ((float) newHeight) / height;

		Matrix matrix = new Matrix();
		matrix.postScale(scaleWidth, scaleHeight);
		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);

		return resizedBitmap;

	}
	
	/**
	 * 파라미터로 넘어온 Bitmap 을 해당 degrees (각도) 로 변환한 뒤 반환합니다.
	 * 
	 * @param b
	 * @param degrees
	 * @return
	 */
   public static Bitmap getRotateBitmap(Bitmap b, int degrees) {
       if ( degrees != 0 && b != null ) {
           Matrix m = new Matrix();
           Matrix identity = new Matrix();
           m.set(identity);
           m.preRotate(degrees, (float) b.getWidth() / 2, (float) b.getHeight() / 2);
           try {
               Bitmap b2 = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, false);
               if (b != b2) {
                   b.recycle();
                   b = b2;
               }
           } catch (OutOfMemoryError ex) {
               // We have no memory to rotate. Return the original bitmap.
           }
       }
       
       return b;
   }

   /**
    * SrcBitmap 을 degrees 만큼 회전 한 뒤, 상하 반전 시킨다.
    * 
    * @param b
    * @param degrees
    * @return
    */
   public static Bitmap getRotateTopDownReverseBitmap(Bitmap b, int degrees) {
	   if ( degrees != 0 && b != null ) {
           Matrix m = new Matrix();
           Matrix identity = new Matrix();
           m.set(identity);
           m.preRotate(degrees, (float) b.getWidth() / 2, (float) b.getHeight() / 2);
           m.postScale(1, -1);
           try {
               Bitmap b2 = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, false);
               if (b != b2) {
                   b.recycle();
                   b = b2;
               }
           } catch (OutOfMemoryError ex) {
               // We have no memory to rotate. Return the original bitmap.
           }
       }
       
       return b;
   }
   
   public static BitmapFactory.Options getBitmapOptions() {
	   BitmapFactory.Options options = new BitmapFactory.Options();
	   options.inSampleSize = 1;
	   options.inPurgeable = true;
	   options.inDither = true;
	   
	   return options;
   }

   public static Bitmap getTestRotateBitmap(Bitmap b, int degrees, float newWidth, float newHeight) {
       if ( degrees != 0 && b != null ) {
    	   
           Matrix m = new Matrix();
           Matrix identity = new Matrix();
           m.set(identity);
           m.preRotate(degrees, (float) b.getWidth() / 2, (float) b.getHeight() / 2);
           try {
               Bitmap b2 = Bitmap.createBitmap(b, 0, 0, (int)newWidth, (int)newHeight, m, true );
               if (b != b2) {
                   b.recycle();
                   b = b2;
               }
           } catch (OutOfMemoryError ex) {
               // We have no memory to rotate. Return the original bitmap.
           }
       }
       
       return b;
   }
   
	/**
	 * EditText 의 프롬프트를 맨 마지막에 위치시킨다.
	 * 
	 * @param et
	 */
	public static void setLastEditCursor(EditText et) {
		int length = et.getText().length();
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(Utils.class, "EditText 길이 : " + length);
		}
		
		et.setSelection(length);
	}
	
	/**
	 * 화면에 키보드를 강제로 표시한다.
	 * 
	 * @param context
	 * @param et
	 */
	public static void showSoftKeyboard(Context context, EditText et) {
		InputMethodManager inputManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.showSoftInput(et, 0);
	}

	/**
	 * 0.1초 정도의 딜레이를 주고 화면에 키보드를 강제로 표시한다.
	 * 
	 * @param context
	 */
	public static void showVirturalKeyboard(final Context context){
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
             @Override
             public void run() {
                  InputMethodManager m = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);

                  if(m != null){
                    // m.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                    m.toggleSoftInput(0, InputMethodManager.SHOW_IMPLICIT);
                  } 
             }

        }, 100);         
    }
	
	/**
	 * 화면에 키보드를 강제로 숨긴다.
	 * 
	 * @param context
	 * @param et
	 */
	public static void hideSoftKeyboard(Context context, EditText et) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(Utils.class, "hideSoftKeyboard 호출");
		}
		
		InputMethodManager inputManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(et.getWindowToken(), 0);
	}
	
	/**
	 * 화면에 키보드가 떠있는 경우, 키보드를 숨기며 반대로 키보드가 숨겨져 있는 경우 키보드를 띄운다.
	 * 
	 * @param context
	 */
	public static void toggleSoftKeyboard(Context context) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(Utils.class, "toggleSoftKeyboard 호출");
		}
		
		InputMethodManager inputManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
		
		inputManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
	}
	
	/**
	 * 현재 화면 크기를 가져와서 Point 객체로 반환합니다.
	 * 
	 * @param context
	 * @return
	 */
	public static Point getScreenSize(Context context) {
		Display display = ((WindowManager)context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		
		Point point = new Point();
		point.x = display.getWidth();
		point.y = display.getHeight();
		
		return point;
	}
	
	/**
    * 파일 경로에 있는 이미지의 회전 각도를 반환합니다.
    * 
    * @param filepath
    * @return
    */
	public static int getExifOrientation(String filepath) {
       int degree = 0;
       ExifInterface exif = null;
       
       try {
           exif = new ExifInterface(filepath);
       } catch (IOException e) {
    	   if(Constants.DEVELOPE_MODE) {
    		   Utils.logPrint(Utils.class, "cannot read exif");
    	   }
    	   
           e.printStackTrace();
       }
       
       if (exif != null) {
           int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);
           
           if (orientation != -1) {
               // We only recognize a subset of orientation tag values.
               switch(orientation) {
                   case ExifInterface.ORIENTATION_ROTATE_90:
                       degree = 90;
                       break;
                       
                   case ExifInterface.ORIENTATION_ROTATE_180:
                       degree = 180;
                       break;
                       
                   case ExifInterface.ORIENTATION_ROTATE_270:
                       degree = 270;
                       break;
               }
           }
       }
       
       return degree;
   }
   
   /**
    * 파라미터로 넘어온 Bitmap 을 해당 filePath 에 저장합니다.
    * 
    * @param bitmap
    * @param filePath
    */
   public static void saveBitmapToFileCache(Bitmap bitmap, String filePath) {
	   
	   File fileCacheItem = new File(filePath);
	   FileOutputStream fos = null;

	   try {
		   fileCacheItem.createNewFile();
		   fos = new FileOutputStream(fileCacheItem);
   		            
		   bitmap.compress(CompressFormat.PNG, 100, fos);
		   
		   if(Constants.DEVELOPE_MODE) {
			   Utils.logPrint(Utils.class, "비트맵 파일 저장이 성공했습니다.");
		   }
	   } catch (Exception e) {
		   e.printStackTrace();
	   } finally {
		   Utils.logPrint(Utils.class, "FOS : " + fos);
		   if(fos != null) {
			   try {
				   fos.flush();
				   fos.close();
			   } catch (IOException e) {
				   e.printStackTrace();
			   }
		   }
	   }
   }
   
   public static byte[] serializeBitmapObject(Bitmap bitmap) {
	   
	   ByteArrayOutputStream baos = new ByteArrayOutputStream();
	   bitmap.compress(CompressFormat.PNG, 100, baos);
	   byte[] bitmapBytes = baos.toByteArray();
	   
	   try {
		   baos.close();
	   } catch(IOException ie) {
	   } finally {
		   baos = null;
	   }
	   
	   return bitmapBytes;
	   
   }
   
   /**
	 * Object 타입을 byte 타입으로 Serialize 합니다.
	 * 
	 * @param object
	 * @return
	 */
	
	public static byte[] serializeObject(Object object) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(Utils.class, "직렬화 시작");
		}
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
		try {
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(object);
			oos.close();
			
			byte[] buf = bos.toByteArray();
			
			if(Constants.DEVELOPE_MODE) {
				Utils.logPrint(Utils.class, "직렬화 성공");
			}
			
			return buf;
		} catch (IOException e) {
			e.printStackTrace();
			
			if(Constants.DEVELOPE_MODE) {
				Utils.logPrint(Utils.class, "직렬화 실패");
			}
			
			return null;
		}
	}
	
	/**
	 * byte 타입을 해당 Object 로 deSerialize 합니다. 
	 * 
	 * @param b
	 * @return
	 */
	
	public static Object deSerializeObject(byte[] b) {
		try {
			ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(b));
			Object object = ois.readObject();
			
			ois.close();
			
			if(Constants.DEVELOPE_MODE) {
				Utils.logPrint(Utils.class, "Deserialize 성공");
			}
			
			return object;
		} catch(IOException ie) {
			return null;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static ArrayList<ShelvesBooks> getShelvesBooksFromDB(Context context) {
		
		ArrayList<ShelvesBooks> items = new ArrayList<ShelvesBooks>();
		
		SQLiteDatabase sdb = new SBooksDBHelper(context).getReadableDatabase();

		String readSBooksSQL = "SELECT * FROM " + Constants.TABLE_HISTORY;
		Cursor readCursor = sdb.rawQuery(readSBooksSQL, null);
		
		while(readCursor.moveToNext()) {
			ShelvesBooks book = new ShelvesBooks(
					Utils.convertUnderBarToSpace(readCursor.getString(1)), 
					readCursor.getInt(2), 
					readCursor.getString(3),
					readCursor.getInt(4),
					readCursor.getInt(5));
			items.add(book);
		}
		
		readCursor.close();
		sdb.close();
		
		return items;
		
	}
	
	/**
	 * SaveScrapData 를 OriginScrapData 로 변환합니다.
	 * 
	 */
	public static OriginScrapData convertDataSaveToOrigin(SaveScrapData data, BitmapFactory.Options option) {
		OriginScrapData originData = null;
		ArrayList<CropViewPos> cropViewPosArray = null;
		ArrayList<TextViewPos> textViewPosArray = null;
		String pViewFilePath = null;
		String snapViewFilePath = null;
		
		int index = data.getIndex();
		String info = data.getInfo();
		
		if(data.getSViewByteArray() != null) {
			cropViewPosArray = (ArrayList<CropViewPos>) Utils.deSerializeObject(data.getSViewByteArray());
		}
		
		if(data.getTViewByteArray() != null) {
			textViewPosArray = (ArrayList<TextViewPos>) Utils.deSerializeObject(data.getTViewByteArray());
		}
		
		pViewFilePath = data.getPViewFilePath();
		
		originData = new OriginScrapData(index, info, cropViewPosArray, textViewPosArray, pViewFilePath);
		
		return originData;
    }
	
	/**
	 * OriginScrapData 를 SaveScrapData 로 변환합니다.
	 * 
	 * @param data
	 * @return
	 */
	public static SaveScrapData convertDataOriginToSave(OriginScrapData data) {
		byte[] sViewByteArray = null;
		byte[] tViewByteArray = null;
		
		int index = data.getIndex();
		String info = data.getInfo();
		
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(Utils.class, "convertDataOriginToSave 호출 중 data 상태");
		}
		
		// ScrapViewArray Serializable
		ArrayList<CropViewPos> cropViewPosArray = data.getCropViewPosArray();
		if(cropViewPosArray != null) {
			sViewByteArray = Utils.serializeObject(cropViewPosArray);
		}
		
		// TextViewArray Serializable
		ArrayList<TextViewPos> textViewPosArray = data.getTextViewPosArray();
		if(textViewPosArray != null) {
			tViewByteArray = Utils.serializeObject(textViewPosArray);
		}
		
		// PenViewArary Serializable
		String pViewFilePath = data.getPViewFilePath();
		
		SaveScrapData saveScrapData = new SaveScrapData(index, info, sViewByteArray, tViewByteArray, pViewFilePath);
		
		return saveScrapData;
	}
	
	/**
	 * SaveScrapData의 ArrayList 를 OriginScrapData 의 ArrayList 로 변환합니다.
	 * 
	 * @param saveArray
	 * @return
	 */
	public static ArrayList<OriginScrapData> convertArraySaveToOrigin(ArrayList<SaveScrapData> saveArray) {
		ArrayList<OriginScrapData> originArray = new ArrayList<OriginScrapData>();
		
		// Bitmap 옵션 설정
		BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
		bitmapOptions.inPurgeable = true;

		int saveArraySize = saveArray.size();
		for(int i=0; i < saveArraySize; i++) {
			originArray.add(Utils.convertDataSaveToOrigin(saveArray.get(i), bitmapOptions));
		}

		return originArray;
	}
	
	/**
	 * 데이터베이스에 존재하는 해당 스크랩 북의 페이지를 추가합니다.
	 * 
	 * @param context
	 * @param sBookTitle
	 * @param data
	 */
	public static void saveScrapPageToDB(Context context, String sBooksTitle, SaveScrapData data, int state) {
		String title = convertSpaceToUnderBar(sBooksTitle);
		
		SQLiteDatabase sdb = new SBooksDBHelper(context).getWritableDatabase();
		
		// 페이지 저장하기 위한 ContentValues 를 구성해준다.
		ContentValues insertValues = new ContentValues();
		insertValues.put(Constants.SCRAP_COLUMN_INDEX, data.getIndex());
		insertValues.put(Constants.SCRAP_COLUMN_INFO, data.getInfo());
		insertValues.put(Constants.SCRAP_COLUMN_SCRAP_VIEW_ARRAY, data.getSViewByteArray());
		insertValues.put(Constants.SCRAP_COLUMN_TEXT_VIEW_ARRAY, data.getTViewByteArray());
		insertValues.put(Constants.SCRAP_COLUMN_PEN_VIEW_FILE_PATH, data.getPViewFilePath());
		
		switch(state) {
			case ScrapPage.LOAD_NEW_PAGE:			//	새로운 페이지를 생성
				if(Constants.DEVELOPE_MODE) {
					Utils.logPrint(Utils.class, "데이터를 새롭게 저장합니다.");
				}
				
				// 새로운 페이지를 저장 시, 뒤에 다른 페이지들이 존재한다면 해당 페이지들의 index 를 +1 씩 업데이트 한다.
				String updateIndexSql = "UPDATE '" + title + "' SET " + Constants.SCRAP_COLUMN_INDEX + " = " + Constants.SCRAP_COLUMN_INDEX + " + 1 " +
				   						"WHERE " + Constants.SCRAP_COLUMN_INDEX + " >= " + data.getIndex();
				sdb.execSQL(updateIndexSql);
				sdb.insert(title, null, insertValues);
				
				// History Table 을 업데이트 한다.
				String countPageSql = "SELECT * FROM '" + title + "'";
				Cursor countPageCursor = sdb.rawQuery(countPageSql, null);
				
				int pageCount = countPageCursor.getCount();
				
				if(Constants.DEVELOPE_MODE) {
					Utils.logPrint(Utils.class, "읽어들인 총 페이지 : " + pageCount);
				}
				
				countPageCursor.close();
				
				// 가져온 현재 테이블의 총 페이지를 History 테이블에 업데이트한다.
				ContentValues updatePageValues = new ContentValues();
				updatePageValues.put(Constants.HISTORY_COLUMN_PAGE, pageCount);
				
				sdb.update(Constants.TABLE_HISTORY, updatePageValues, Constants.HISTORY_COLUMN_TITLE + " = '" + title + "'", null);
				
				
				break;
			case ScrapPage.LOAD_MODIFY_PAGE:		//	기존 페이지를 수정
				if(Constants.DEVELOPE_MODE) {
					Utils.logPrint(Utils.class, "데이터를 업데이트 합니다.");
				}

				sdb.update(title, insertValues, Constants.SCRAP_COLUMN_INDEX + " = " + data.getIndex(), null);
				break;
		}
		
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(Utils.class, "완료 되었습니다.");
		}
		
		sdb.close();
	}
	
	/**
	 * 데이터베이스에서 pageIndex 에 해당하는 페이지 정보를 SaveScrapData 타입으로 반환합니다.
	 * 
	 * @param context
	 * @param sBooksTitle
	 * @param pageIndex
	 * @return
	 */
	public static SaveScrapData loadScrapPageFromDB(Context context, String sBooksTitle, int pageIndex) {
		Utils.logPrint(Utils.class, "loadScrapPageFromnDB 호출");
		String title = convertSpaceToUnderBar(sBooksTitle);
		
		SaveScrapData data = null;
		SQLiteDatabase sdb = new SBooksDBHelper(context).getReadableDatabase();
		String loadSql = "SELECT * FROM '" + title + "' WHERE " + Constants.SCRAP_COLUMN_INDEX + " = " + pageIndex;
		Cursor loadCursor = sdb.rawQuery(loadSql, null);
		
		while(loadCursor.moveToNext()) {
			data = new SaveScrapData(loadCursor.getInt(1), loadCursor.getString(2), 
					loadCursor.getBlob(3), loadCursor.getBlob(4), loadCursor.getString(5));
			break;
		}

		loadCursor.close();
		sdb.close();
		
		return data;
	}
	
	/**
	 * 넘어온 이름으로 스크랩 북을 생성한다.
	 * 
	 * @param context
	 * @param sBooksTitle
	 */
	public static int createSBooks(Context context, String sBooksTitle, String date, int coverThemeIdx, int pageThemeIdx) {
		int bookId = -1;
		
		try {
		
			if(Constants.DEVELOPE_MODE) {
				Utils.logPrint(Utils.class, "createSBooks 호출");
			}
			
			String title = convertSpaceToUnderBar(sBooksTitle);
	
			SQLiteDatabase sdb = new SBooksDBHelper(context).getWritableDatabase();
			
			// 스크랩 북 테이블 생성
			sdb.execSQL(
					"CREATE TABLE '" + title + "' (" +
					Constants.SCRAP_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
					Constants.SCRAP_COLUMN_INDEX + " INTEGER NOT NULL, " + 
					Constants.SCRAP_COLUMN_INFO + " TEXT, " + 
					Constants.SCRAP_COLUMN_SCRAP_VIEW_ARRAY + " BLOB, " +
					Constants.SCRAP_COLUMN_TEXT_VIEW_ARRAY + " BLOB, " +
					Constants.SCRAP_COLUMN_PEN_VIEW_FILE_PATH + " BLOB);");
			
			// 히스토리에 저장
			ContentValues historyValues = new ContentValues();
			historyValues.put(Constants.HISTORY_COLUMN_TITLE, title);
			historyValues.put(Constants.HISTORY_COLUMN_DATE, date);
			historyValues.put(Constants.HISTORY_COLUMN_COVER_THEME_IDX, coverThemeIdx);
			historyValues.put(Constants.HISTORY_COLUMN_PAGE_THEME_IDX, pageThemeIdx);
			
			sdb.insert(Constants.TABLE_HISTORY, null, historyValues);
			
			
			// 히스토리에 저장 된 스크랩 북의 아이디 가져오기
			String readSookbsIdSql = "SELECT " + Constants.HISTORY_COLUMN_ID +  
								  	 " FROM " + Constants.TABLE_HISTORY + 
								  	 " WHERE " + Constants.HISTORY_COLUMN_TITLE + " = '" + title + "'";
			Cursor readSBooksIdCursor = sdb.rawQuery(readSookbsIdSql, null);
			readSBooksIdCursor.moveToFirst();
			bookId = readSBooksIdCursor.getInt(0);
			
			if(Constants.DEVELOPE_MODE) {
				Utils.logPrint(Utils.class, "새롭게 생성된 스크랩 북의 북 ID : " + bookId);
			}
			
			readSBooksIdCursor.close();
			sdb.close();
		} catch(SQLException sqlException) {
			return -1;
		}
		
		return bookId;
	}
	
	/**
	 * 변경 전 타이틀 제목과 변경 후 타이틀 제목을 가지고 와서 HISTORY
	 * 
	 * @param context
	 * @param preTitle
	 * @param changeTitle
	 */
	public static int updateSBooks(Context context, int changeMode, String preTitle, String changeTitle, int coverThemeIdx, int pageThemeIdx) {

		try {
			
			if(Constants.DEVELOPE_MODE) {
				Utils.logPrint(Utils.class, "변경 전 제목 : " + preTitle + ", 변경 후 제목 : " + changeTitle);
			}

			String pTitle = convertSpaceToUnderBar(preTitle);
			String cTitle = convertSpaceToUnderBar(changeTitle);
		
			SQLiteDatabase sdb = new SBooksDBHelper(context).getWritableDatabase();
			
			ContentValues updateValues = new ContentValues();
			updateValues.put(Constants.HISTORY_COLUMN_TITLE, cTitle);
			updateValues.put(Constants.HISTORY_COLUMN_COVER_THEME_IDX, coverThemeIdx);
			updateValues.put(Constants.HISTORY_COLUMN_PAGE_THEME_IDX, pageThemeIdx);
			
			// 커버 테마와 페이지 테마를 추후 파라미터로 받아서 업데이트 해줘야 한다. (데이터베이스도 수정해야 함)
			
			sdb.update(Constants.TABLE_HISTORY, updateValues, Constants.HISTORY_COLUMN_TITLE + " = '" + pTitle + "'", null);
			
			if(changeMode == DialogAddSBooks.CHANGE_TITLE) {
				String renameSql = "ALTER TABLE '" + pTitle + "' RENAME TO '" + cTitle + "'";   
				sdb.execSQL(renameSql);
			}
			
			sdb.close();
		} catch(SQLException sqlException) {
			return -1;
		} 
		
		return 1;
	}
	
	public static boolean deleteSBooks(Context context, String title) {
		try {
			
			if(Constants.DEVELOPE_MODE) {
				Utils.logPrint(Utils.class, "deleteSBooks 호출");
			}
			
			String cTitle = convertSpaceToUnderBar(title);

			SQLiteDatabase sdb = new SBooksDBHelper(context).getWritableDatabase();
			
			sdb.delete(Constants.TABLE_HISTORY, Constants.HISTORY_COLUMN_TITLE + " ='" + cTitle + "'", null);
			
			String dropSql = "DROP TABLE '" + cTitle + "'";
			sdb.execSQL(dropSql);
			
			sdb.close();
			
		} catch(SQLException sqlException) {
			return false;
		}
		
		return true;
	}
	
	public static boolean updatePageTheme(Context context, String sBooksTitle, int pageThemeIdx) {
		try {
			
			if(Constants.DEVELOPE_MODE) {
				Utils.logPrint(Utils.class, "updatePageTheme 호출(변경 테마 인덱스 : " + pageThemeIdx + ")");
			}

			String title = convertSpaceToUnderBar(sBooksTitle);
		
			SQLiteDatabase sdb = new SBooksDBHelper(context).getWritableDatabase();
			
			ContentValues updateValues = new ContentValues();
			updateValues.put(Constants.HISTORY_COLUMN_TITLE, title);
			updateValues.put(Constants.HISTORY_COLUMN_PAGE_THEME_IDX, pageThemeIdx);
			
			// 커버 테마와 페이지 테마를 추후 파라미터로 받아서 업데이트 해줘야 한다. (데이터베이스도 수정해야 함)
			
			sdb.update(Constants.TABLE_HISTORY, updateValues, Constants.HISTORY_COLUMN_TITLE + " = '" + title + "'", null);
			sdb.close();
			
		} catch(SQLException sqlException) {
			return false;
		} 
		
		return true;
	}
	
	/**
	 * 현재 사용자가 보고 있는 페이지를 데이터베이스에서 삭제합니다.
	 * 
	 * @param context
	 * @param sBooksTitle
	 * @param pageIndex
	 */
	public static void deleteAndLoadPage(Context context, String sBooksTitle, int pageIndex) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(Utils.class, "deleteAndLoadPage 호출, 삭제할 페이지 넘버(" + pageIndex + ")");
		}
		
		String title = convertSpaceToUnderBar(sBooksTitle);

		// 먼저, pageIndex 에 해당하는 레코드를 삭제한다.
		SQLiteDatabase sdb = new SBooksDBHelper(context).getWritableDatabase();
		sdb.delete(title, Constants.SCRAP_COLUMN_INDEX + " = " + pageIndex , null);
		
		// 삭제한 페이지 뒤에 남은 페이지가 있으면 페이지 넘버링 하나씩 감소한다.
		String remainSql = "UPDATE '" + title + "' SET " + Constants.SCRAP_COLUMN_INDEX + " = " + Constants.SCRAP_COLUMN_INDEX + " - 1 " +
						   "WHERE " + Constants.SCRAP_COLUMN_INDEX + " > " + pageIndex;
		sdb.execSQL(remainSql);
		
		// 현재 테이블의 총 페이지를 가져온다.
		String countPageSql = "SELECT * FROM '" + title + "'";
		Cursor countPageCursor = sdb.rawQuery(countPageSql, null);
		
		int pageCount = countPageCursor.getCount();
		
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(Utils.class, "읽어들인 총 페이지 : " + pageCount);
		}
		
		countPageCursor.close();
		
		// 가져온 현재 테이블의 총 페이지를 History 테이블에 업데이트한다.
		ContentValues updatePageValues = new ContentValues();
		updatePageValues.put(Constants.HISTORY_COLUMN_PAGE, pageCount);
		
		sdb.update(Constants.TABLE_HISTORY, updatePageValues, Constants.HISTORY_COLUMN_TITLE + " = '" + title + "'", null);
		
		sdb.close();
		
	}
	
	/**
	 * 데이터베이스에서 해당 스크랩 북의 모든 페이지를 SaveScrapData ArrayList 형태로 반환합니다.
	 * 
	 * @param context
	 * @param sBooksTitle
	 * @return
	 */
	public static ArrayList<SaveScrapData> getReadAllScrapPage(Context context, String sBooksTitle) {
		String title = convertSpaceToUnderBar(sBooksTitle);
		
		ArrayList<SaveScrapData> results = new ArrayList<SaveScrapData>();
		
		SQLiteDatabase sdb = new SBooksDBHelper(context).getReadableDatabase();
		
		String readAllSql = "SELECT * FROM '" + title + "' ORDER BY " + Constants.SCRAP_COLUMN_INDEX;
		Cursor readAllCursor = sdb.rawQuery(readAllSql, null);
		
		while(readAllCursor.moveToNext()) {
			SaveScrapData data = new SaveScrapData(readAllCursor.getInt(1), readAllCursor.getString(2), 
					readAllCursor.getBlob(3), readAllCursor.getBlob(4), readAllCursor.getString(5));
			results.add(data);
		}
		
		readAllCursor.close();
		sdb.close();
		
		return results;
	}
	
	public static ArrayList<PageIndex> getPageIndex(Context context, String sBooksTitle) {
		String title = convertSpaceToUnderBar(sBooksTitle);
		
		ArrayList<PageIndex> results = new ArrayList<PageIndex>();
		SQLiteDatabase sdb = new SBooksDBHelper(context).getReadableDatabase();

		// 해당 스크랩 북의 페이지 테마 인덱스를 가져온다.
		String readPageThemeSql = "SELECT " + Constants.HISTORY_COLUMN_ID + ", " + Constants.HISTORY_COLUMN_PAGE_THEME_IDX +  
							  	 " FROM " + Constants.TABLE_HISTORY + 
							  	 " WHERE " + Constants.HISTORY_COLUMN_TITLE + " = '" + title + "'";
		Cursor readPageThemeCursor = sdb.rawQuery(readPageThemeSql, null);
		readPageThemeCursor.moveToFirst();
		
		int sBooksColumnId = readPageThemeCursor.getInt(0);
		int pageThemeIdx = readPageThemeCursor.getInt(1);
		readPageThemeCursor.close();
		
		// 해당 스크랩 북의 인덱스와 정보들을 가져온다.
		String readIndexSql = "SELECT " + Constants.SCRAP_COLUMN_INDEX + ", " + Constants.SCRAP_COLUMN_INFO + " " +
							  "FROM '" + title + "'";
		Cursor readIndexCursor = sdb.rawQuery(readIndexSql, null);
		
		while(readIndexCursor.moveToNext()) {
			String info = readIndexCursor.getString(1);
			
			if(isInfoCheckInvalid(info)) {
				PageIndex pageIndex = new PageIndex(sBooksColumnId, readIndexCursor.getInt(0), info, pageThemeIdx);
				results.add(pageIndex);
			}
		}
		
		readIndexCursor.close();
		sdb.close();
		
		return results;
	}
	
	/**
	 * 스크랩 북 타이틀에 해당하는 스크랩 북의 ID 를 가져온다.
	 * ID 는 HISTORY 테이블에 저장된 해당 테이블의 ID 이다.
	 * 
	 * @param context
	 * @param sBooksTitle
	 * @return
	 */
	public static int getSBooksId(Context context, String sBooksTitle) {
		SQLiteDatabase sdb = new SBooksDBHelper(context).getReadableDatabase();
		
		String title = convertSpaceToUnderBar(sBooksTitle);

		String readSookbsIdSql = "SELECT " + Constants.HISTORY_COLUMN_ID +  
							  	 " FROM " + Constants.TABLE_HISTORY + 
							  	 " WHERE " + Constants.HISTORY_COLUMN_TITLE + " = '" + title + "'";
		Cursor readSBooksIdCursor = sdb.rawQuery(readSookbsIdSql, null);
		readSBooksIdCursor.moveToFirst();
		
		int sBooksId = readSBooksIdCursor.getInt(0);
		
		readSBooksIdCursor.close();
		sdb.close();
		
		return sBooksId;
	}
	
	public static int[] getPageThemeIndex(Context context, String sBooksTitle) {
		SQLiteDatabase sdb = new SBooksDBHelper(context).getReadableDatabase();
		
		String convertTitle = Utils.convertSpaceToUnderBar(sBooksTitle);

		// 해당 스크랩 북의 페이지 테마 인덱스를 가져온다.
		String readPageThemeSql = "SELECT " + Constants.HISTORY_COLUMN_ID + ", " + Constants.HISTORY_COLUMN_PAGE_THEME_IDX +  
							  	 " FROM " + Constants.TABLE_HISTORY + 
							  	 " WHERE " + Constants.HISTORY_COLUMN_TITLE + " = '" + convertTitle + "'";
		Cursor readPageThemeCursor = sdb.rawQuery(readPageThemeSql, null);
		readPageThemeCursor.moveToFirst();

		int sBooksId = readPageThemeCursor.getInt(0);
		int pageThemeIdx = readPageThemeCursor.getInt(1);
		
		int[] result = {sBooksId, pageThemeIdx};
		
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(Utils.class, "getPageThemeIndex 값(sBooksId : " + result[0] + "), (pageThemeIdx : " + result[1] + ")");
		}
		
		readPageThemeCursor.close();
		sdb.close();
		
		return result;
	}

	/**
	 * Bitmap 을 Recycle 합니다.
	 * 
	 * @param bitmap
	 */
	public static void recycleBitmap(Bitmap bitmap) {
		if(bitmap != null) {
			bitmap.recycle();
			bitmap = null;
		}
	}
	
	/**
	 * RootView 가 가지고 있던 모든 View 를 Recycle 합니다.
	 * 
	 * @param root
	 */
	public static void recurisveRecycle(View root){
		if( root == null ){
			return;
		}
		
		if(root instanceof ViewGroup){
			ViewGroup group = (ViewGroup)root;
			int count = group.getChildCount();
			for( int i = 0; i < count; i++ ){
				recurisveRecycle(group.getChildAt(i));
			}
			
			if( ! ( root instanceof AdapterView ) ){
				group.removeAllViews();
			}
		}
		
		if( root instanceof ImageView ){
			((ImageView)root).setImageDrawable(null);
		}
		
		root.setBackgroundDrawable(null);
		root = null;
	}
	
	/**
	 * 전면, 후면 카메라가 있다면 둘 중 하나를 선택하여 사용한다 (후면 카메라 우선)
	 * 
	 * @return
	 */
	@TargetApi(9)
	public static Camera getUsingCamera() {
		int cameraCount = 0;
	    Camera cam = null;
	    Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
	    cameraCount = Camera.getNumberOfCameras();
	    
	    for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
	        Camera.getCameraInfo(camIdx, cameraInfo);
	        if(cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
	        	if(Constants.DEVELOPE_MODE) {
	        		Utils.logPrint(Utils.class, "후면 카메라를 사용합니다.");
	        	}
	        	
	            try {
	                cam = Camera.open(camIdx);
	            } catch (RuntimeException e) {
	            	if(Constants.DEVELOPE_MODE) {
	            		Utils.logPrint(Utils.class, "Camera failed to open: " + e.getLocalizedMessage());
	            	}
	            }
	            
	            break;
	        } else if(cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
	        	if(Constants.DEVELOPE_MODE) {
	        		Utils.logPrint(Utils.class, "전면 카메라를 사용합니다.");
	        	}
	        	
	        	try {
	                cam = Camera.open(camIdx);
	            } catch (RuntimeException e) {
	            	if(Constants.DEVELOPE_MODE) {
	            		Utils.logPrint(Utils.class, "Camera failed to open: " + e.getLocalizedMessage());
	            	}
	            }

	            break;
	        }
	    }
	    
	    return cam;
	}
	
	/**
	 * 전, 후면 카메라 중 어떤 카메라를 사용할 수 있는지를 리턴한다.
	 * 
	 * @return
	 */
	@TargetApi(9)
	public static int getUsingCameraIndex() {
		int cameraCount = 0;
	    Camera cam = null;
	    Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
	    cameraCount = Camera.getNumberOfCameras();
	    
	    int result = -1;
	    
	    for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
	        Camera.getCameraInfo(camIdx, cameraInfo);
	        if(cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
	        	if(Constants.DEVELOPE_MODE) {
	        		Utils.logPrint(Utils.class, "후면 카메라를 사용합니다.");
	        	}
	        	
	        	result = Camera.CameraInfo.CAMERA_FACING_BACK;
	        	
	        	break; 
	        } else if(cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
	        	if(Constants.DEVELOPE_MODE) {
	        		Utils.logPrint(Utils.class, "전면 카메라를 사용합니다.");
	        	}
	        	
	        	result = Camera.CameraInfo.CAMERA_FACING_FRONT;
	        	
	        	break;
	        }
	    }
	    
	    return result;
	}
	
	/**
	 * 문자열에 들어 있는 공백 문자를 _ (언더바) 문자로 변경한다.
	 * 
	 * @param text
	 * @return
	 */
	public static String convertSpaceToUnderBar(String text) {
		return text.replaceAll(" ", "_");
	}
	
	/**
	 * 문자열에 들어 있는 _ (언더바) 문자를 공백 문자로 변경한다.
	 * 
	 * @param text
	 * @return
	 */
	public static String convertUnderBarToSpace(String text) {
		return text.replaceAll("_", " ");
	}
	
	/**
	 * 페이지 정보 문자열의 설정된 기준에 부합한지를 true, false 로 반환한다.
	 * 
	 * @param text
	 * @return
	 */
	public static boolean isInfoCheckInvalid(String text) {
		if(text == null) {
			return false;
		}
		
		if(text.length() <= 0) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * 스크랩 북 제목의 문자열이 설정된 기준에 부합한지에 대한 여부를 true, false 로 반환한다.
	 * 
	 * @param text
	 * @return
	 */
	public static boolean isTitleCheckInvalid(String text) {
		if(text == null) {
			return false;
		}
		
		if(text.length() <= 0) {
			return false;
		}
		
		Pattern pattern = Pattern.compile(".*[^가-힣a-zA-Z0-9\\s].*");
		Matcher matcher = pattern.matcher(text);
		
		if(matcher.matches()) {
			return false;
		}
		
		
		return true;
	}
	
	/**
	 * SD Card 가 마운트 되어있는지 여부를 boolean 으로 리턴한다.
	 * 
	 * @return
	 */
	public static boolean isSDCardMounted() {
		return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
	}
	
	/**
	 * 스크랩 앱에 사용할 File 경로를 전체 삭제한다.
	 * 
	 */
	public static void initFilePath() {
		if(isSDCardMounted()) {
			String sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();		//	SD 카드 경로
			String dirPath = sdCardPath + Constants.SCRAP_DIR_ROOT;
			
			deleteDirectory(dirPath);
		}
	}
	
	/**
	 * path 및 하위 디렉토리, 파일을 전부 삭제한다.
	 * 
	 * @param path
	 */
	public static void deleteDirectory(String path) {
		File file = new File(path);
		
		if(file.exists()) {
			File[] childFileList = file.listFiles();
			
			for(File childFile : childFileList) {
				if(childFile.isDirectory()) {
					deleteDirectory(childFile.getAbsolutePath());
				} else {
					childFile.delete();
				}
			}
			
			file.delete();
		}
	}
	
	
	/**
	 * fileSaveMode 에 따라, 파일명을 생성하며, 해당 디렉토리가 존재하지 않는다면 디렉토리를 생성한다.
	 * 
	 * @param fileSaveMode
	 * @return
	 */
	public static String getFilePathName(Context context, int fileSaveMode) {
		if(isSDCardMounted()) {
			String sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();		//	SD 카드 경로
			
			String dirPath = null;
			String filePath = null;
			
			String currentDate = DateUtils.getCurrentDate(DateUtils.FORMAT_FILE_DATE);
			
			switch(fileSaveMode) {
				case Constants.SAVE_FILE_PATH_SCRAP:
					dirPath = sdCardPath + Constants.SAVE_IMAGE_PATH;							//	이미지 파일을 저장할 경로
					filePath = dirPath + "/" + Constants.SAVE_IMAGE_PREFIX + currentDate + Constants.SAVE_IMAGE_POSTFIX;
					
					break;
					
				case Constants.SAVE_FILE_PATH_PVIEW:											//	펜 뷰 파일을 저장할 경로
					dirPath = sdCardPath + Constants.SAVE_IMAGE_PVIEW_PATH;
					filePath = dirPath + "/" + Constants.SAVE_IMAGE_PVIEW_PREFIX + currentDate + Constants.SAVE_IMAGE_POSTFIX;
					
					break;
					
			}
			
			// 백업할 경로가 존재하는지를 판단하여, 존재하지 않으면 경로를 생성한다.
			File dir = null;
			dir = new File(dirPath);
			if(!dir.exists()) {
				dir.mkdirs();
			}
			
			return filePath;
		} else {
			return null;
		}
	}
	
	/**
	 * pageIndex 에 해당하는 페이지의 스냅샷 파일 경로를 가져옵니다.
	 * 
	 * @param pageIndex
	 * @return
	 */
	public static String getSnapViewFilePathName(int sBookId, int pageIndex) {
		if(Utils.isSDCardMounted()) {
			String sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();		//	SD 카드 경로
			
			String currentDate = DateUtils.getCurrentDate(DateUtils.FORMAT_FILE_DATE);
			
			String dirPath = sdCardPath + Constants.SAVE_IMAGE_SNAP_VIEW_PATH;
			String filePath = dirPath + "/" + Constants.SAVE_IMAGE_SNAP_VIEW_PREFIX + sBookId + "_" + pageIndex + Constants.SAVE_IMAGE_POSTFIX;
			
			File dir = null;
			dir = new File(dirPath);
			if(!dir.exists()) {
				dir.mkdirs();
			}
			
			if(Constants.DEVELOPE_MODE) {
				Utils.logPrint(Utils.class, "리턴할 스냅뷰 파일경로 : " + filePath);
			}
			
			return filePath;
		} else {
			return null;
		}
		
	}
	
	/**
	 * EditText 커서의 X 좌표 위치를 가져온다.
	 * 
	 * @param editText
	 * @return
	 */
	public static float getCursorX(EditText editText){
	    float ret = -1;
	    try{
	        int pos = editText.getSelectionStart();
	        Layout layout = editText.getLayout();
	        float x = layout.getPrimaryHorizontal(pos);
	        
	            ret = x;
	        
	    }
	    catch(Exception exception){
	    	if(Constants.DEVELOPE_MODE) {
	    		Utils.logPrint(Utils.class, "getCursorX" + exception.toString());
	    	}
	    }
	    
	    return ret;
	}
	
	/**
	 * prefix + index 로 구성한 id 에 따른 실제 ID 값을 리턴한다.
	 * 
	 * @param context
	 * @param type (id 타입)
	 * @param prefix
	 * @param index
	 * @return
	 */
	public static int getId(Context context, String type, String prefix, int index) {
		int id = context.getResources().getIdentifier(prefix + index, type, context.getPackageName());
		
		return id;
	}
	
	public static Bitmap getScreenShot(View v) {
		Bitmap screenShot = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.RGB_565);
		v.draw(new Canvas(screenShot));
		
		return screenShot;
	}
	
	public static ArrayList<Uri> getSBookSnapShot(Context context, int sBookId) {
		ArrayList<Uri> results = new ArrayList<Uri>();

		String sdCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();		//	SD 카드 경로
		String dirPath = sdCardPath + Constants.SAVE_IMAGE_SNAP_VIEW_PATH;
		String filePath = dirPath + "/" + Constants.SAVE_IMAGE_SNAP_VIEW_PREFIX + sBookId + "_";
		
		File[] fileList = new File(dirPath).listFiles();
		
		if(fileList != null) {
			for(int i=0; i < fileList.length; i++) {
				String fileDirPath = fileList[i].getAbsolutePath();
				
				if(fileDirPath.startsWith(filePath)) {
					results.add(getFilePathUri(context, fileDirPath));
				}
			}
		}
		
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(Utils.class, "results 크기 : " + results.size());
		}
		
		return results;
		
	}
	
	public static Uri getFilePathUri(Context context, String filePath) {
    	File file = new File(filePath);
		ContentValues values = new ContentValues(2);
		values.put(MediaStore.Images.Media.MIME_TYPE, "image/png");
		values.put(MediaStore.Images.Media.DATA, file.getAbsolutePath());
		
		return context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }
	
	public static void killProcessToApp(Context context) {
		ActivityManager am = (ActivityManager)context.getSystemService(Activity.ACTIVITY_SERVICE);
		am.killBackgroundProcesses(context.getPackageName());
	}
	
	/**
	 * 해당 파일 경로의 이미지의 가로 크기를 구해온다.
	 * 
	 * @param fileName
	 * @return
	 */
	public static int getBitmapOfWidth(String fileName) {
		try {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			
			BitmapFactory.decodeFile(fileName, options);
			
			return options.outWidth;
		} catch(Exception e) {
			return 0;
		}
	}
	
	/**
	 * 해당 파일 경로의 이미지의 세로 크기를 구해온다.
	 * 
	 * @param fileName
	 * @return
	 */
	public static int getBitmapOfHeight(String fileName) {
		try {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			
			BitmapFactory.decodeFile(fileName, options);
			
			return options.outHeight;
		} catch(Exception e) {
			return 0;
		}
	}
	
	public static boolean getHeapMemoryCheck(Context context) {
		
		Double maxMemory = new Double(Runtime.getRuntime().maxMemory() / new Double(1024 * 1024));
		Double totalMemory = new Double(Runtime.getRuntime().totalMemory() / new Double(1024 * 1024));
		
		MemoryInfo info = new MemoryInfo();
		ActivityManager manager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
		manager.getMemoryInfo(info);
		
		Debug.MemoryInfo testInfo = new Debug.MemoryInfo();
		Debug.getMemoryInfo(testInfo);
		
		String memMessage = String.format(
			    "Memory: Pss=%.2f MB, Private=%.2f MB, Shared=%.2f MB",
			    testInfo.getTotalPss() / 1024.0,
			    testInfo.getTotalPrivateDirty() / 1024.0,
			    testInfo.getTotalSharedDirty() / 1024.0);
		
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(Utils.class, "메모리 정보 : " + memMessage);
			Utils.logPrint(Utils.class, "할당된 최대 메모리 : " + manager.getMemoryClass());
			
			Utils.logPrint(Utils.class, "Total memory : " + (Runtime.getRuntime().totalMemory() / (1024 * 1024)) + "MB");
			Utils.logPrint(Utils.class, "Max Memory : " + (Runtime.getRuntime().maxMemory() / (1024 * 1024)) + "MB");
			Utils.logPrint(Utils.class, "Free Memory : " + (Runtime.getRuntime().freeMemory()  / (1024 * 1024)) + "MB");
			Utils.logPrint(Utils.class, "Allocation Memory : " + ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024 * 1024)) + "MB");
		}
		
		if(info.lowMemory) {
			Toast.makeText(context, context.getString(R.string.memory_low), Toast.LENGTH_SHORT).show();
			return false;
		}
		
		return true;
		
	}
	
	/**
	 * 파라메터로 넘어온 Pixel 을 DP 로 변환하는 함수
	 * 
	 * @param context
	 * @param px
	 * @return
	 */
	public static float convertPixelsToDp(Context context, float px){
	    Resources resources = context.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    float dp = px / (metrics.densityDpi / 160f);
	    return dp;
	}
	
	/**
	 * 파라메터로 넘어온 DP 를 Pixel 로 변환하는 함수
	 * 
	 * @param dp
	 * @param context
	 * @return
	 */
	public static float convertDpToPixel(Context context, float dp){
	    Resources resources = context.getResources();
	    DisplayMetrics metrics = resources.getDisplayMetrics();
	    float px = dp * (metrics.densityDpi/160f);
	    return px;
	}

}















