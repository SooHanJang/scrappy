package madcat.studio.utils;

import madcat.studio.constants.Constants;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class ImageUtils {
	
	public static BitmapFactory.Options getBitmapOptions() {
		BitmapFactory.Options bmpOptions = new BitmapFactory.Options();
		bmpOptions.inPreferredConfig = Constants.IMAGE_QUALITY_TYPE;
		bmpOptions.inJustDecodeBounds = false;
		bmpOptions.inPurgeable = true;
		
		return bmpOptions;
	}
	
	public static Bitmap readImageWithSampling(String imagePath, float targetWidth, float targetHeight) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(Utils.class, "readImageWidthSampling �Լ� ȣ��");
		}
		
		int[] sampleExponent = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024};
		
		// Get the dimensions of the bitmap
		BitmapFactory.Options bmOptions = ImageUtils.getBitmapOptions();
		BitmapFactory.decodeFile(imagePath, bmOptions);
		 
		int photoWidth  = bmOptions.outWidth;
		int photoHeight = bmOptions.outHeight;
		
		// Determine how much to scale down the image
		int scaleFactor = (int) Math.ceil(Math.min(photoWidth / targetWidth, photoHeight / targetHeight));
		
		for(int i=0; i < sampleExponent.length; i++) {
			if(scaleFactor <= sampleExponent[i]) {
				scaleFactor = sampleExponent[i];
				break;
			}
		}
		
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(ImageUtils.class, "Sampling Scale Factor : " + scaleFactor);
		}
		
		// Decode the image file into a Bitmap sized to fill the View
		bmOptions.inSampleSize = scaleFactor;
		 
		Bitmap orgImage = BitmapFactory.decodeFile(imagePath, bmOptions);
		 
		return orgImage;
	}

}
