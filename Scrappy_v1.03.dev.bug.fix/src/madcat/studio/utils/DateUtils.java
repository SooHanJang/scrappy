package madcat.studio.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
	
	public static final int FORMAT_FILE_DATE					=	0;
	public static final int FORMAT_BOOK_DATE					=	1;
	
	public static String getCurrentDate(int format) {
		Date nowTime = new Date();
		SimpleDateFormat dateFormat = null;

		switch(format) {
			case FORMAT_FILE_DATE:
				dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
				break;
				
			case FORMAT_BOOK_DATE:
				dateFormat = new SimpleDateFormat("yyyy. MM. dd");
				break;
		}
		
		
		return dateFormat.format(nowTime);
	}

}
