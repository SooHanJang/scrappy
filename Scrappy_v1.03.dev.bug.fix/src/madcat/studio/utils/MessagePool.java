package madcat.studio.utils;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.position.CropViewPos;
import madcat.studio.position.TextViewPos;
import madcat.studio.scrapdata.OriginScrapData;
import madcat.studio.view.PenView;
import madcat.studio.view.ScrapView;
import madcat.studio.view.TextEditView;
import android.app.Application;
import android.graphics.Color;

public class MessagePool extends Application{
	
	private int mZOrderTopIndex								=	-1;
	private ArrayList<OriginScrapData> mOriginScrapArray	=	new ArrayList<OriginScrapData>();
	private ArrayList<ScrapView> mScrapViewArray			=	new ArrayList<ScrapView>();
	private ArrayList<CropViewPos> mCropViewPosArray		=	new ArrayList<CropViewPos>();
	private ArrayList<TextViewPos> mTextViewPosArray		=	new ArrayList<TextViewPos>();
	private String mPenViewFilePath;
	private byte[] mCameraBytes;
	private int mCurrentColor								=	Color.BLACK;
	private int mCurrentWidth								=	PenView.PEN_DEFAULT_WIDTH;
	private int mCurrentBrush								=	PenView.PEN_VIEW_STYLE_NORMAL;
	private int mFontSize									=	TextEditView.FONT_DEFAULT_SIZE;
	private int mFontStyle									=	TextEditView.TEXT_STYLE_NORMAL;
	
	private int mMode										=	Constants.MENU_MODE_VIEW;
	
	public ArrayList<OriginScrapData> getOriginScrapArray()	{	return mOriginScrapArray;	}
	public ArrayList<ScrapView> getScrapViewArray() {	return mScrapViewArray;	}
	public ArrayList<CropViewPos> getCropViewPosArray()	{	return mCropViewPosArray;	}
	public ArrayList<TextViewPos> getTextViewPosArray() {	return mTextViewPosArray;	}
	public int getZOrderTopIndex() {	return mZOrderTopIndex;	}
	public int getMode() {	return mMode;	}
	public String getPenViewFilePath() {	return mPenViewFilePath;	}
	public byte[] getCameraBytes() {	return mCameraBytes;	}
	public int getCurrentColor() {	return mCurrentColor;	}
	public int getCurrentWidth() {	return mCurrentWidth;	}
	public int getCurrentBrush() {	return mCurrentBrush;	}
	public int getFontSize() {	return mFontSize;	}
	public int getFontStyle() {	return mFontStyle;	}

	public void setOriginScrapArray(ArrayList<OriginScrapData> originScrapArray) {	this.mOriginScrapArray = originScrapArray;	}
	public void setScrapViewArray(ArrayList<ScrapView> scrapViewArray) {	this.mScrapViewArray = scrapViewArray;	}
	public void setCropviewPosArray(ArrayList<CropViewPos> cropViewPosArray) {	this.mCropViewPosArray = cropViewPosArray;	}
	public void setTextViewPosArray(ArrayList<TextViewPos> textViewPosArray) {	this.mTextViewPosArray = textViewPosArray;	}
	public void setZOrderTopIndex(int zOrderTopIndex) {	this.mZOrderTopIndex = zOrderTopIndex;	}
	public void setMode(int mode) {	this.mMode = mode;	}
	public void setPenViewFilePath(String penViewfilePath) {	this.mPenViewFilePath = penViewfilePath;	}
	public void setCameraBytes(byte[] cameraBytes) {	this.mCameraBytes = cameraBytes;	}
	public void setCurrentColor(int color) {	this.mCurrentColor = color;	}
	public void setCurrentWidth(int width) {	this.mCurrentWidth = width;	}
	public void setCurrentBrush(int brushStyle) {	this.mCurrentBrush = brushStyle;	}
	public void setFontSize(int size) {	this.mFontSize = size;	}
	public void setFontStyle(int style)	{	this.mFontStyle = style;	}
	
	public void initMessagePool() {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "initMessagePool ȣ��");
		}
		
		mZOrderTopIndex = -1;
		
		mOriginScrapArray = new ArrayList<OriginScrapData>();
		mScrapViewArray = new ArrayList<ScrapView>();
		mCropViewPosArray = new ArrayList<CropViewPos>();
		mTextViewPosArray = new ArrayList<TextViewPos>();
		
		mPenViewFilePath = null;
		mCameraBytes = null;
		mCurrentColor = Color.BLACK;
		mCurrentWidth = PenView.PEN_DEFAULT_WIDTH;
		mCurrentBrush = PenView.PEN_VIEW_STYLE_NORMAL;
		mFontSize = TextEditView.FONT_DEFAULT_SIZE;
		
		mMode = Constants.MENU_MODE_VIEW;
	}

}
