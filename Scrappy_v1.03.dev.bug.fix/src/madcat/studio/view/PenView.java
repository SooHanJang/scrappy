package madcat.studio.view;

import java.io.Serializable;

import madcat.studio.constants.Constants;
import madcat.studio.scrap.R;
import madcat.studio.scrap.ScrapPage;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.EmbossMaskFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

public class PenView extends View implements Serializable {
	
	private static final long serialVersionUID					=	1L;

	private Context mContext;
	private MessagePool mMessagePool;
	
	public static final int VIEW_ID								=	200;
	
	public static final int STATE_PEN							=	0;
	public static final int STATE_ERASER						=	1;
	
	public static final int PEN_DEFAULT_WIDTH					=	5;
	
	public static final int PEN_VIEW_BRUSH_SIZE					=	6;
	
	public static final int PEN_VIEW_STYLE_NORMAL				=	0;
	public static final int PEN_VIEW_STYLE_INNER				=	1;
	public static final int PEN_VIEW_STYLE_OUTTER				=	2;
	public static final int PEN_VIEW_STYLE_SOLID				=	3;
	public static final int PEN_VIEW_STYLE_EMBOSS				=	4;
	public static final int PEN_VIEW_STYLE_RADIAL_GRADIENT		=	5;
	
	public static final float BLUR_FILTER_WIDTH					=	3;
	public static final float BLUR_FILTER_OUTTER_WIDTH			=	10;
	public static final float EMBOSS_FILTER_WIDTH				=	3;
	
	public static final int PEN_VIEW_BRUSH_STATE_OFF			=	0;
	public static final int PEN_VIEW_BRUSH_STATE_ON				=	1;
	
	private int mPenViewState									=	STATE_PEN;
	private int mPenViewStyle									=	PEN_VIEW_STYLE_NORMAL;

	private Path mPath;
	private Paint mDrawPaint;
	private Paint mErasePaint;
	private Paint mEraserOuterPaint;
	
	private Bitmap mPenBitmap;
	private String mPenViewFilePath;
	private Paint mPenBitmapPaint;
	private Canvas mPenBitmapCanvas;
	
	private final float DISTANCE_TOLERANCE						=	5;
	private final int ERASER_CORRECTION_VALUE					=	2;
	
	private float mPreX, mPreY;
	private int mCurrentAction									=	-1;
	private int mCurrentColor									=	Color.BLACK;
	private int mCurrentWidth									=	PEN_DEFAULT_WIDTH;
	
	// 필터 설정
	private BlurMaskFilter mNormalFilter, mInnerFilter, mOutterFilter, mSolidFilter;
	private EmbossMaskFilter mEmbossFilter;
	private RadialGradient mRadialGradientFilter;

	public PenView(Context context) {
		super(context);

		this.mContext = context;
		this.mMessagePool = (MessagePool)context.getApplicationContext();
		this.mCurrentColor = mMessagePool.getCurrentColor();
		this.mCurrentWidth = mMessagePool.getCurrentWidth();
		this.setId(VIEW_ID);
		
		initDrawPenView();
		
	}
	
	private void initDrawPenView() {
		mPath = new Path();
		
    	// 펜을 표시 할 캔버스를 설정
    	mPenViewFilePath = mMessagePool.getPenViewFilePath();
    	
    	if(Constants.DEVELOPE_MODE) {
    		Utils.logPrint(getClass(), "PenView initDraw 중 불러오는 파일 경로 값 : " + mPenViewFilePath);
    	}
    	
		if(mPenViewFilePath != null) {
			mPenBitmap = BitmapFactory.decodeFile(mPenViewFilePath, Utils.getBitmapOptions()).copy(Config.ARGB_4444, true);
			mPenBitmapCanvas = new Canvas(mPenBitmap);
		}
		
		// 사용할 페인트 초기화
		initPaint();
		
		// 사용할 이펙트 초기화
		initFilter();
		
		// Path Effect 적용
		mDrawPaint.setPathEffect(new CornerPathEffect(10));
		mPenBitmapPaint.setPathEffect(new CornerPathEffect(10));
		
	}
	
	private void initPaint() {
		if (mDrawPaint == null) {	mDrawPaint = new Paint(Paint.ANTI_ALIAS_FLAG);	}
		if (mErasePaint == null) {	mErasePaint = new Paint(Paint.ANTI_ALIAS_FLAG);	}
		if (mEraserOuterPaint == null) {	mEraserOuterPaint = new Paint(Paint.ANTI_ALIAS_FLAG);	}
		if (mPenBitmapPaint == null) {	mPenBitmapPaint = new Paint(Paint.ANTI_ALIAS_FLAG);	}
		
		// DrawPaint 설정
		mDrawPaint.setDither(true);
		mDrawPaint.setStyle(Paint.Style.STROKE);
		mDrawPaint.setStrokeJoin(Paint.Join.ROUND);
		mDrawPaint.setStrokeCap(Paint.Cap.ROUND);
		
		mDrawPaint.setColor(mCurrentColor);
	    mDrawPaint.setStrokeWidth(mCurrentWidth);
	    
	    // PenBitmapPaint 설정
    	mPenBitmapPaint.setDither(true);
    	mPenBitmapPaint.setStyle(Paint.Style.STROKE);
    	mPenBitmapPaint.setStrokeJoin(Paint.Join.ROUND);
    	mPenBitmapPaint.setStrokeCap(Paint.Cap.ROUND);
		
    	mPenBitmapPaint.setColor(mCurrentColor);
    	mPenBitmapPaint.setStrokeWidth(mCurrentWidth);
		
	    // EraserPaint 설정
    	mErasePaint.setStyle(Paint.Style.STROKE);
    	mErasePaint.setStrokeJoin(Paint.Join.ROUND);
    	mErasePaint.setStrokeCap(Paint.Cap.ROUND);
		
    	mErasePaint.setColor(Color.BLACK);
    	mErasePaint.setStrokeWidth(mCurrentWidth * ERASER_CORRECTION_VALUE);
		
    	mErasePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
    	
    	// EraserOuterPaint 설정
    	mEraserOuterPaint.setStyle(Style.STROKE);
    	mEraserOuterPaint.setColor(Color.BLACK);
    	mEraserOuterPaint.setStrokeWidth(2);
	}
	
	private void initFilter() {
		if(mNormalFilter == null) {	mNormalFilter = new BlurMaskFilter(BLUR_FILTER_WIDTH, BlurMaskFilter.Blur.NORMAL);	}
		if(mInnerFilter == null) { mInnerFilter = new BlurMaskFilter(BLUR_FILTER_WIDTH, BlurMaskFilter.Blur.INNER);	}
		if(mOutterFilter == null) { mOutterFilter = new BlurMaskFilter(BLUR_FILTER_OUTTER_WIDTH, BlurMaskFilter.Blur.OUTER);	}
		if(mSolidFilter == null) { mSolidFilter = new BlurMaskFilter(BLUR_FILTER_WIDTH, BlurMaskFilter.Blur.SOLID);	}
		if(mEmbossFilter == null) {	mEmbossFilter = new EmbossMaskFilter(new float[] {1, 1, 1}, 0.5f, 8f, 3f);	}
		if(mRadialGradientFilter == null) { 
			mRadialGradientFilter = new RadialGradient(8f, 80f, 90f, mCurrentColor, Color.WHITE, Shader.TileMode.MIRROR);	
		}
	}
	
	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		if(mPenViewFilePath == null) {
			if(Constants.DEVELOPE_MODE) {
				Utils.logPrint(getClass(), "penViewFilePath 가 null 이므로, 파일명을 설정합니다.");
			}
			
			mPenBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_4444);		//	그림을 그려줄 비트맵을 생성한다.
			mPenBitmapCanvas = new Canvas(mPenBitmap);		// Canvas 와 Bitmap 을 연결해준다.

			// 현재 그려지고 있는 비트맵의 파일명을 설정한다.
			mPenViewFilePath = Utils.getFilePathName(mContext, Constants.SAVE_FILE_PATH_PVIEW);
			
			if(mPenViewFilePath != null) {
				mMessagePool.setPenViewFilePath(mPenViewFilePath);
			} else {
				AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
				builder.setTitle(mContext.getString(R.string.dig_check_sd_card_title));
				builder.setMessage(mContext.getString(R.string.dig_check_sd_card_msg));
				builder.setPositiveButton(mContext.getString(R.string.confirm_dig_ok), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(Utils.isSDCardMounted()) {
							mMessagePool.setPenViewFilePath(mPenViewFilePath);
						} else {
							Toast.makeText(mContext, mContext.getString(R.string.dig_check_sd_card_msg), Toast.LENGTH_SHORT).show();
						}
					}
				});
				builder.setNegativeButton(mContext.getString(R.string.confirm_dig_exit), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Utils.killProcessToApp(mContext);
					}
				});
				
				builder.show();
			}
		}
		
		
		super.onLayout(changed, left, top, right, bottom);
	}
	
	public void setPenStyle(int style) {
		mPenViewStyle = style;
		
		if(mDrawPaint == null) {
			initPaint();
		}
		
		switch(style) {
		
			case PEN_VIEW_STYLE_NORMAL:
//				mDrawPaint.setMaskFilter(mNormalFilter);
				mDrawPaint.setMaskFilter(null);
				mDrawPaint.setShader(null);
				break;
				
			case PEN_VIEW_STYLE_INNER:
				mDrawPaint.setMaskFilter(mInnerFilter);
				mDrawPaint.setShader(null);
				break;
				
			case PEN_VIEW_STYLE_OUTTER:
				mDrawPaint.setMaskFilter(mOutterFilter);
				mDrawPaint.setShader(null);
				break;
				
			case PEN_VIEW_STYLE_SOLID:
				mDrawPaint.setMaskFilter(mSolidFilter);
				mDrawPaint.setShader(null);
				break;
				
			case PEN_VIEW_STYLE_EMBOSS:
				mDrawPaint.setMaskFilter(mEmbossFilter);
				mDrawPaint.setShader(null);
				break;
				
			case PEN_VIEW_STYLE_RADIAL_GRADIENT:
				mDrawPaint.setMaskFilter(null);
				
				mRadialGradientFilter = new RadialGradient(8f, 80f, 90f, mCurrentColor, Color.WHITE, Shader.TileMode.MIRROR);
				mDrawPaint.setShader(mRadialGradientFilter);
				break;
		}
	}
	
	@Override
	public void draw(Canvas canvas) {
		super.draw(canvas);
		
		canvas.drawBitmap(mPenBitmap, 0, 0, mPenBitmapPaint);
		
		if (mPenViewState != STATE_ERASER) {
			
			if(mPenViewState == PEN_VIEW_STYLE_RADIAL_GRADIENT) {
				
			}
			
			
			canvas.drawPath(mPath, mDrawPaint);
		} else {
			if(mCurrentAction == MotionEvent.ACTION_MOVE) {
				mErasePaint.setStrokeWidth(mCurrentWidth * ERASER_CORRECTION_VALUE);
				canvas.drawCircle(mPreX, mPreY, mCurrentWidth, mEraserOuterPaint);
			}
		}
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		return super.dispatchTouchEvent(event);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		
		if(mMessagePool.getMode() == Constants.MENU_MODE_PEN) {
			ScrapPage.setStyleMenuVisibility(false);
		}
		
		ScrapPage.setDecoMenuVisibility(false);
		
		float touchX = event.getX();
		float touchY = event.getY();
		
		mCurrentAction = event.getAction();

		if(mMessagePool.getMode() == Constants.MENU_MODE_PEN) {
			switch(event.getAction()) {
			
				case MotionEvent.ACTION_DOWN:
					ScrapPage.setToolMenuVisibility(false);
					
					if(mPenViewState == STATE_PEN || mPenViewState == STATE_ERASER) {
						mPath.moveTo(touchX, touchY);
						
						mPreX = touchX;
						mPreY = touchY;
						
						invalidate();
					}
					
					break;

				case MotionEvent.ACTION_MOVE:

					if(mPenViewState == STATE_PEN) {

						if (Math.abs(touchX - mPreX) >= DISTANCE_TOLERANCE || Math.abs(touchY - mPreY) >= DISTANCE_TOLERANCE) {
							mPath.quadTo(mPreX, mPreY, touchX, touchY);
							
							invalidate();
							
							mPreX = touchX;
							mPreY = touchY;
						}
						
					} else {
						
						if (Math.abs(touchX - mPreX) >= DISTANCE_TOLERANCE || Math.abs(touchY - mPreY) >= DISTANCE_TOLERANCE) {
							mPath.lineTo(touchX, touchY);
							mPenBitmapCanvas.drawPath(mPath, mErasePaint);
							
							invalidate();
							
							mPath.reset();
							mPath.moveTo(touchX, touchY);
							
							mPreX = touchX;
							mPreY = touchY;
							
						}
					}
					
					break;
					
					
				case MotionEvent.ACTION_UP:
					
					if(mPenViewState == STATE_PEN) {

						mPath.lineTo(touchX, touchY);
						mPenBitmapCanvas.drawPath(mPath, mDrawPaint);
						mPath.reset();
						
						invalidate();
						
					} else {
						
						mPath.lineTo(touchX, touchY);
						mPenBitmapCanvas.drawPath(mPath, mErasePaint);
						mPath.reset();
						
						invalidate();
						
					}
					
					break;
			
			
			}
			
			return true;
			
		}
		
		return super.onTouchEvent(event);
	}
	
	public void setPenViewState(int mode) {	this.mPenViewState = mode;	}
	public int getPenViewState() {	return mPenViewState;	}
	
	public Bitmap getPenBitmap() {	return mPenBitmap;	}
	
	public void setCurrentColor(int color) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "setCurrentColor 호출, 현재 펜 스타일 : " + mPenViewStyle);
		}
		
		this.mCurrentColor = color;
		
		if(mDrawPaint == null) {
			initPaint();
		}
		
		mDrawPaint.setColor(mCurrentColor);
		
		if(mPenViewStyle == PenView.PEN_VIEW_STYLE_RADIAL_GRADIENT) {
			mRadialGradientFilter = new RadialGradient(8f, 80f, 90f, mCurrentColor, Color.WHITE, Shader.TileMode.MIRROR);
			mDrawPaint.setShader(mRadialGradientFilter);
		}
	}
	
	public void setCurrentWidth(int width) {	
		this.mCurrentWidth = width;	
		
		if(mDrawPaint == null) {
			initPaint();
		}
		
		mDrawPaint.setStrokeWidth(mCurrentWidth);
	}
	
	public void executeRecycle() {
		if(mPenBitmap != null) { 
			mPenBitmap.recycle();
			mPenBitmap = null;
		}
	}

}
