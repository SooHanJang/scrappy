package madcat.studio.view;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.position.TextViewPos;
import madcat.studio.scrap.ScrapPage;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.content.Context;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

public class TextEditLayout extends RelativeLayout {

	public static final int VIEW_ID										=	300;
	
	public static final int TEXT_MODE_INIT								=	0;
	public static final int TEXT_MODE_EDITING							=	1;
	
	private static int mCurrentTextMode									=	TEXT_MODE_INIT;
	
	private Context mContext;
	private static MessagePool mMessagePool;
	
	private static ArrayList<TextViewPos> mTextViewPosArray;
	
	private static TextEditLayout mTextEditLayout;
	
	public TextEditLayout(Context context) {
		super(context);
		this.mContext = context;
		this.mTextEditLayout = this;
		this.mMessagePool = (MessagePool)mContext.getApplicationContext();
		
		this.setId(VIEW_ID);
		
		RelativeLayout.LayoutParams rp = 
			new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
		
		this.setLayoutParams(rp);
		
		this.mTextViewPosArray = mMessagePool.getTextViewPosArray();
		
		// 현재 설정되어 있는 EditView 를 불러와서 추가한다.
		int textViewPosSize = mTextViewPosArray.size();
		
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "현재 TextEditLayout 에 추가해야 할 텍스트 갯수 : " + textViewPosSize);
		}
		
		for(int i=0; i < textViewPosSize; i++) {
			if(Constants.DEVELOPE_MODE) {
				Utils.logPrint(getClass(), "현재 추가 할 텍스트 값 : " + mTextViewPosArray.get(i).getText());
			}
			
			TextEditView addTextEditView = new TextEditView(context, mTextViewPosArray.get(i), i);
			addTextEditView.setFocusable(false);
			this.addView(addTextEditView);
		}

	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {

		if(mMessagePool.getMode() == Constants.MENU_MODE_TEXT) {
			
			ScrapPage.setStyleMenuVisibility(false);
			ScrapPage.setDecoMenuVisibility(false);

			switch(event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					
					if(Constants.DEVELOPE_MODE) {
						Utils.logPrint(getClass(), "TextEditLayout 에 Action_Down 발생");
					}
					
					// 만약, 메뉴가 화면에 보여지고 있다면 메뉴 레이어를 Gone 한다.
					ScrapPage.setToolMenuVisibility(false);
					
					// 비어있는 EditView가 있다면, 모두 삭제한다.
					removeEmptyEditView();
					
					if(Constants.DEVELOPE_MODE) {
						Utils.logPrint(getClass(), "현재 모드 : " + mCurrentTextMode);
					}
					
					if(mCurrentTextMode == TEXT_MODE_INIT) {
						
						if(Constants.DEVELOPE_MODE) {
							Utils.logPrint(getClass(), "TEXT_MODE_INIT 모드");
						}

						float touchX = event.getX();
						float touchY = event.getY();
						
						// TextEditView 를 추가한다.
						addTextEditView(touchX, touchY);
					} 
					else if(mCurrentTextMode == TEXT_MODE_EDITING) {
						if(Constants.DEVELOPE_MODE) {
							Utils.logPrint(getClass(), "TEXT_MODE_EDITING 모드");
						}
						
						mCurrentTextMode = TEXT_MODE_INIT;
						clearAllFocus();
						
					}
					
					invalidate();
					
					break;
					
			}
			
			return true;
			
		} else if(mMessagePool.getMode() == Constants.MENU_MODE_EDIT) {

			switch(event.getAction()) {
			
				case MotionEvent.ACTION_DOWN:

					clearAllOuterLine();
					
					break;
			
			}
			
			
		}
		
		return super.onTouchEvent(event);
		
	}
	
	public static void clearAllOuterLine() {
		for(int i=0; i < mTextEditLayout.getChildCount(); i++) {
			TextEditView textEditView = (TextEditView)mTextEditLayout.getChildAt(i);
			textEditView.drawBackground(false);
		}
	}

	public static void clearAllFocus() {
		for(int i=0; i < mTextEditLayout.getChildCount(); i++) {
			TextEditView textEditView = (TextEditView)mTextEditLayout.getChildAt(i);
			textEditView.setFocusable(false);
		}
	}
	
	public static void clearExceptFocus(int index) {
		for(int i=0; i < mTextEditLayout.getChildCount(); i++) {
			if(i != index) {
				TextEditView textEditView = (TextEditView)mTextEditLayout.getChildAt(i);
				textEditView.setFocusable(false);
			}
		}
	}
	
	private void addTextEditView(float touchX, float touchY) {
		// 새로운 EditView의 좌표 영역을 추가한다.
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "새로운 EditView 추가");
		}
		
		TextViewPos textViewPos = new TextViewPos(touchX, touchY);
		textViewPos.setColor(mMessagePool.getCurrentColor());
		textViewPos.setFontSize(mMessagePool.getFontSize());

		// MessagePool 에 EditView 좌표 값을 등록
		mMessagePool.getTextViewPosArray().add(textViewPos);
		int lastIndex = mMessagePool.getTextViewPosArray().size() - 1;
		
		// Layout 에서 사용할 실제 EditView 를 TextEditLayout 에 추가
		TextEditView textEditingView = new TextEditView(mContext, textViewPos, lastIndex);
		this.addView(textEditingView);
		textEditingView.requestFocusFromTouch();
		
		mCurrentTextMode = TEXT_MODE_EDITING;
	}
	
	/**
	 * 등록된 EditView 를 삭제하는 함수입니다.
	 * 
	 * @param editView
	 * @param index
	 */
	public static void removeEditView(TextEditView editView, int index) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(TextEditLayout.class, "removeEditView 호출");
		}
		
		mTextEditLayout.removeView(editView);
		mTextViewPosArray.remove(index);
		mMessagePool.setTextViewPosArray(mTextViewPosArray);
		
		for(int i=0; i < mTextEditLayout.getChildCount(); i++) {
			TextEditView childEditView =  (TextEditView)mTextEditLayout.getChildAt(i);
			childEditView.setCurrentId(i);
		}

		ScrapPage.changeTrashIcon(false);
		ScrapPage.setTrashMenuVisibility(false);
		ScrapPage.setLayerUpDownMenuVisibility(false);
		mTextEditLayout.invalidate();
	}
	
	
	/**
	 * 
	 * 등록된 자식 EditView 의 내용이 비어 있다면, 해당 뷰를 삭제합니다.
	 * 
	 */
	public void removeEmptyEditView() {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "removeEmptyEditView 호출");
		}
		
		for(int i=0; i < getChildCount(); i++) {
			TextEditView removeChildView = (TextEditView) getChildAt(i);
			
			if(removeChildView.getText().toString().trim().length() == 0) {
				if(Constants.DEVELOPE_MODE) {
					Utils.logPrint(getClass(), i + " 번째 textView를 삭제합니다.");
				}
				
				removeChildView.setFocusable(false);
				removeViewAt(i);
			}
		}
	}
	
	public static void setCurrentTextMode(int textMode) {	mCurrentTextMode = textMode;	}
	public static int getCurrentTextMode() {	return mCurrentTextMode;	}
}









