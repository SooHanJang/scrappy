package madcat.studio.view;

import madcat.studio.constants.Constants;
import madcat.studio.scrap.R;
import madcat.studio.utils.Utils;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposeShader;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.view.MotionEvent;
import android.view.View;

public class ColorPickerView extends View {
	
	private final int SPLIT_WIDTH									=	24;
	private final int SELECT_RECT_WIDTH								=	5;
	
	private Context mContext;
	
	private int mWidth, mHeight;
	private float mSelectBitmapRadius;
	private PointF mSelectedPoint;
	
	private Paint mBitmapPaint;
	private Bitmap mSelectBitmap, mSelectBitmap2;
	private Bitmap mGradualChangeBitmap;
	
	private OnColorChangedListener mChangedListener;
	
	private Paint mSelectPaint;
	private RectF mSelectRect;
	
	public ColorPickerView(Context context, int width, int height, PointF selectedPoint) {
		super(context);
		this.mContext = context;
		
		this.mWidth = width;
		this.mHeight = height;
		
		this.mSelectedPoint = selectedPoint;
		
		initColorPicker();
	}
	
	private void initColorPicker() {
		mBitmapPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        
		mSelectBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_pen);
		mSelectBitmap2 = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.icon_eraser);
        mSelectBitmapRadius = mSelectBitmap.getWidth() / 2;
        
        mSelectPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mSelectPaint.setStyle(Style.STROKE);
        mSelectPaint.setStrokeWidth(2);
        mSelectPaint.setColor(Color.BLACK);
        
        mSelectRect = new RectF();
        mSelectRect.set(mSelectedPoint.x - SELECT_RECT_WIDTH, mSelectedPoint.y - SELECT_RECT_WIDTH, 
				mSelectedPoint.x + SELECT_RECT_WIDTH, mSelectedPoint.y + SELECT_RECT_WIDTH);
		
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawBitmap(getGradual() , null , new Rect(0, 0, mWidth, mHeight), mBitmapPaint);
		canvas.drawRect(mSelectRect, mSelectPaint);
		
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		
		float x = event.getX();
	    float y = event.getY();
	    
	    switch (event.getAction()) {
		    case MotionEvent.ACTION_DOWN:
		    case MotionEvent.ACTION_MOVE:
		    	
		    	if((x >= 0 && x <= mWidth) && (y >= 0 && y <= mHeight)) {
		    		int selectedColor = getPickerColor(x, y);
		    		
		    		if(Constants.DEVELOPE_MODE) {
		    			Utils.logPrint(getClass(), "선택된 컬러 : " + selectedColor);
		    		}
		    		
		    		mSelectedPoint.x = x;
		    		mSelectedPoint.y = y;
		    		
		    		mSelectRect.set(mSelectedPoint.x - SELECT_RECT_WIDTH, mSelectedPoint.y - SELECT_RECT_WIDTH, 
		    				mSelectedPoint.x + SELECT_RECT_WIDTH, mSelectedPoint.y + SELECT_RECT_WIDTH);
		    		
		    		invalidate();
		    		
		    		if (mChangedListener != null) {
			            mChangedListener.onColorChanged(selectedColor, mSelectedPoint);
			        }
		    	}
		    	
		    	break;
		    case MotionEvent.ACTION_UP:
	    }
		
		return true;
	}
	
	private Bitmap getGradual() {
	    if (mGradualChangeBitmap == null) {
	        Paint leftPaint = new Paint();
	        leftPaint.setStrokeWidth(1);
	        mGradualChangeBitmap = Bitmap.createBitmap(mWidth, mHeight - 2 * SPLIT_WIDTH, Config.RGB_565);
	        Canvas canvas = new Canvas(mGradualChangeBitmap);
	        int bitmapWidth = mGradualChangeBitmap.getWidth();
	        
	        mWidth = bitmapWidth;
	        
	        int bitmapHeight = mGradualChangeBitmap.getHeight();
	        int[] leftColors = new int[] {Color.RED, Color.YELLOW, Color.GREEN, Color.CYAN, Color.BLUE, Color.MAGENTA};
	        Shader leftShader = new LinearGradient(0, bitmapHeight / 2, bitmapWidth, bitmapHeight / 2, leftColors, null, TileMode.REPEAT);
	        LinearGradient shadowShader = new LinearGradient(bitmapWidth / 2, 0, bitmapWidth / 2, bitmapHeight,
	                Color.WHITE, Color.BLACK, Shader.TileMode.CLAMP);
	        ComposeShader shader = new ComposeShader(leftShader, shadowShader, PorterDuff.Mode.SCREEN);
	        leftPaint.setShader(shader);
	        canvas.drawRect(0, 0, bitmapWidth, bitmapHeight, leftPaint);
	    }
	    
	    return mGradualChangeBitmap;
	}
	
	private int getPickerColor(float x, float y) {
	    Bitmap temp = getGradual();
	    
        int intX = (int) x;
        int intY = (int) y;
        if (intX >= temp.getWidth()) {
            intX = temp.getWidth() - 1;
        }
        if (intY >= temp.getHeight()) {
            intY = temp.getHeight() - 1;
        }
        
        return temp.getPixel(intX, intY);
	}
	
	public void setOnColorChangedListenner(OnColorChangedListener listener) {
        mChangedListener = listener;
    }
	
	public interface OnColorChangedListener {
	    void onColorChanged(int color, PointF selectedPoint);
	}

}










