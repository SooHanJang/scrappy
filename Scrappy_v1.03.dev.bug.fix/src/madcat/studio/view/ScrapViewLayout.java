package madcat.studio.view;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.position.CropViewPos;
import madcat.studio.scrap.ScrapPage;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;

public class ScrapViewLayout extends RelativeLayout {
	
	private static Context mContext;
	private static MessagePool mMessagePool;
	
	private static ScrapViewLayout mScrapViewLayout;
	
	private static ArrayList<CropViewPos> mCropViewPosArray;
	private static ArrayList<ScrapView> mScrapPageViewArray;
	
	public ScrapViewLayout(Context context) {
		super(context);
		this.mContext = context;
		this.mMessagePool = (MessagePool)mContext.getApplicationContext();
		this.mScrapViewLayout = this;
		
		setScrapViewToLayout();
	}
	
	public static void setScrapViewToLayout() {
		mScrapViewLayout.removeAllViews();
		
		if(mScrapPageViewArray == null) {
			mScrapPageViewArray = new ArrayList<ScrapView>();
		} else { 
			mScrapPageViewArray.clear();
		}
		
		mCropViewPosArray = mMessagePool.getCropViewPosArray();
		
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(ScrapViewLayout.class, "현재 ScrapViewLayout 에 추가해야 할 Scrap 갯수 : " + mCropViewPosArray.size());
		}
		
        for(int i=0; i < mCropViewPosArray.size(); i++) {
        	mScrapPageViewArray.add(new ScrapView(mContext, mCropViewPosArray.get(i), i));
        	mScrapViewLayout.addView(mScrapPageViewArray.get(i));
        }

        mMessagePool.setScrapViewArray(mScrapPageViewArray);
        
        if(Constants.DEVELOPE_MODE) {
        	Utils.logPrint(ScrapViewLayout.class, "현재 ScrapViewArray 사이즈 : " + mMessagePool.getScrapViewArray().size());
        }
	}
	
	public void setLayerUp(int index) {

		if(index < mCropViewPosArray.size()-1) {
			CropViewPos currentCropViewPos = mCropViewPosArray.get(index);
			CropViewPos nextCropViewPos = mCropViewPosArray.get(index+1);
			
			// 레이어 좌표 순서를 바꿔준다. (한 단계 Up)
			mCropViewPosArray.set(index, nextCropViewPos);
			mCropViewPosArray.set(index+1, currentCropViewPos);

			// ScrapView 가 담긴 Array 의 순서도 바꿔준다.
			ScrapView currentScrapView = mScrapPageViewArray.get(index);
			ScrapView nextScrapView = mScrapPageViewArray.get(index+1);
			
			currentScrapView.setIndex(index+1);
			nextScrapView.setIndex(index);
			
			mScrapPageViewArray.set(index, nextScrapView);
			mScrapPageViewArray.set(index+1, currentScrapView);
			
			// 등록된 View 의 순서도 바꿔준다.
			removeViewAt(index+1);
			removeViewAt(index);
			
			addView(currentScrapView, index);
			addView(nextScrapView, index);
			
			invalidate();
			
		}
		
		// 변경된 값을 저장
		mMessagePool.setCropviewPosArray(mCropViewPosArray);
		mMessagePool.setScrapViewArray(mScrapPageViewArray);
	}
	
	public void setLayerDown(int index) {
		
		if(index > 0) {
			CropViewPos currentCropViewPos = mCropViewPosArray.get(index);
			CropViewPos prevCropViewPos = mCropViewPosArray.get(index-1);
			
			// 레이어 좌표 순서를 바꿔준다. (한 단계 Down)
			mCropViewPosArray.set(index, prevCropViewPos);
			mCropViewPosArray.set(index-1, currentCropViewPos);
			
			// ScrapView 가 담긴 Array 의 순서도 바꿔준다.
			ScrapView currentScrapView = mScrapPageViewArray.get(index);
			ScrapView prevScrapView = mScrapPageViewArray.get(index-1);
			
			currentScrapView.setIndex(index-1);
			prevScrapView.setIndex(index);
			
			mScrapPageViewArray.set(index, prevScrapView);
			mScrapPageViewArray.set(index-1, currentScrapView);
			
			// 등록된 View 의 순서도 바꿔준다.
			removeViewAt(index);
			removeViewAt(index-1);

			addView(prevScrapView, index-1);
			addView(currentScrapView, index-1);
			
			invalidate();
		}
		
		
		// 변경된 값을 저장
		mMessagePool.setCropviewPosArray(mCropViewPosArray);
		mMessagePool.setScrapViewArray(mScrapPageViewArray);
	}
	
	/**
	 * 현재 스크랩된 이미지들의 모드를 전부 INIT_DRAW 모드로 변경한다.
	 * 
	 */
	public static void scrapInitDrawMode() {
		
		for(int i=0; i < mScrapViewLayout.getChildCount(); i++) {
			ScrapView childScrapView = (ScrapView)mScrapViewLayout.getChildAt(i);
			
			childScrapView.setScrapState(ScrapView.SCRAP_STATE_INIT_DRAW);
			childScrapView.setInnerTouchFlag(false);
			childScrapView.invalidate();
		}
	}
	
	public static void removeScrapView(View view, int index) {
    	
    	mCropViewPosArray.remove(index);
    	mMessagePool.setCropviewPosArray(mCropViewPosArray);
    	
    	mScrapViewLayout.removeView(view);
    	mMessagePool.getScrapViewArray().remove(index);
    	
    	// 이미 등록 된, ViewPos 들의 id 를 전부 재설정
    	for(int i=0; i < mMessagePool.getScrapViewArray().size(); i++) {
    		mMessagePool.getScrapViewArray().get(i).setCurrentIndex(i);
    	}

    	ScrapPage.changeTrashIcon(false);
    	ScrapPage.setTrashMenuVisibility(false);
    	ScrapPage.setLayerUpDownMenuVisibility(false);

    	mScrapViewLayout.invalidate();
    }
	
	public int getInnerTouchScrapViewIndex() {

		for(int i=0; i < mScrapViewLayout.getChildCount(); i++) {
			ScrapView childScrapView = (ScrapView)mScrapViewLayout.getChildAt(i);
			
			if(childScrapView != null) {
				if(childScrapView.getInnerTouchFlag()) {
					return childScrapView.getIndex();
				}
			}
		}
		
		return -1;
	}

}










