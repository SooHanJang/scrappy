package madcat.studio.view;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.position.CropViewPos;
import madcat.studio.position.ScrapViewPos;
import madcat.studio.scrap.R;
import madcat.studio.scrap.ScrapCrop;
import madcat.studio.scrap.ScrapPage;
import madcat.studio.utils.ImageUtils;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.FillType;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class CropView extends View {
	
	private final int SAVE_CROP_IMAGE								=	0;
	private final int SAVE_FULL_IMAGE								=	1;
	
	private final float CROP_VIEW_LIMITED_MEMORY					=	1.5f;
	
	private Context mContext;
	private MessagePool mMessagePool;

	private Bitmap mSrcBitmap;
	private Rect mSrcRect;
	private int mChangeWidth, mChangeHeight;
	
	private Paint mSrcPaint, mLinePaint, mScrapPaint, mOutterPaint;
	
	private float mTouchX, mTouchY;
	
	private ArrayList<ScrapViewPos> mArrayScrapPos					=	new ArrayList<ScrapViewPos>();
	private int mCurrentAction;
	private int mScrapTopLeftX, mScrapTopLeftY, mScrapBottomRightX, mScrapBottomRightY;
	
	private boolean mScrapDialogShow							=	false;
	
	private Point mScreenPoint;
	
	public CropView(Context context) {
		super(context);
	}
	
	/**
	 * 갤러리에 저장되어 있는 이미지를 불러올 경우 사용되는 생성자
	 * 
	 * @param context
	 * @param filePath
	 * @param index
	 */
	public CropView(Context context, String filePath, int index) {
		super(context);
		this.mContext = context;
		this.mMessagePool = (MessagePool)context.getApplicationContext();
		
		// 화면의 크기를 미리 가져온다.
		mScreenPoint = Utils.getScreenSize(mContext);
		
		// 사진의 크기를 미리 구해와서, Sampling 을 해준다.
		int srcDegree = Utils.getExifOrientation(filePath);
		
		Bitmap srcBitmap = ImageUtils.readImageWithSampling(filePath, mScreenPoint.x, mScreenPoint.y);
		
		// 경로에 해당하는 이미지를 가져왔을 경우,
		if(srcBitmap != null) {
			// 크롭 시 안내 토스트를 띄워준다.
			Toast.makeText(mContext, mContext.getString(R.string.crop_view_toast_explain), Toast.LENGTH_LONG).show();
			
			mSrcBitmap = Utils.getRotateBitmap(srcBitmap, srcDegree);
			
			initPaint();
			initLayoutParams(index, 0, 0);
		} 
		// 경로에 해당하는 이미지를 가져오지 못했을 경우,
		else {
			Toast.makeText(mContext, mContext.getString(R.string.scrap_crop_image_load_faile), Toast.LENGTH_SHORT).show();
			ScrapCrop.SCRAP_IMAGE_CROP_ACTIVITY.finish();
		}
		
	}
	
	/**
	 * 사진을 찍은 뒤 화면을 불러올 경우 사용되는 생성자
	 * 
	 * @param context
	 * @param srcBitmap
	 * @param index
	 */
	public CropView(Context context, Bitmap srcBitmap, int index, int previewWidth, int previewHeight) {
		super(context);
		this.mContext = context;
		this.mMessagePool = (MessagePool)context.getApplicationContext();
		
		// 크롭 시 안내 토스트를 띄워준다.
		Toast.makeText(mContext, mContext.getString(R.string.crop_view_toast_explain), Toast.LENGTH_LONG).show();
		
		// 화면의 크기를 미리 가져온다.
		mScreenPoint = Utils.getScreenSize(mContext);
		
		switch(Utils.getUsingCameraIndex()) {
			case Camera.CameraInfo.CAMERA_FACING_FRONT:
				mSrcBitmap = Utils.getRotateTopDownReverseBitmap(srcBitmap, 90);
				break;
			case Camera.CameraInfo.CAMERA_FACING_BACK:
				mSrcBitmap = Utils.getRotateBitmap(srcBitmap, 90);
				break;
		}
		
		initPaint();
		initLayoutParams(index, previewWidth, previewHeight);
	}
	
	/**
	 * 페인트 함수 초기화
	 * 
	 */
	private void initPaint() {

		if(mSrcPaint == null) {	mSrcPaint = new Paint(Paint.ANTI_ALIAS_FLAG);	}
		if(mLinePaint == null) { mLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);	}
		if(mScrapPaint == null) { mScrapPaint = new Paint(Paint.ANTI_ALIAS_FLAG);	}
		if(mOutterPaint == null) { mOutterPaint = new Paint(Paint.ANTI_ALIAS_FLAG);	}

		mSrcPaint.setFilterBitmap(true);
		mSrcPaint.setDither(true);
		
		mScrapPaint.setStyle(Style.FILL);
		mScrapPaint.setColor(Color.RED);
		mScrapPaint.setFilterBitmap(true);
		mScrapPaint.setDither(true);
		
		mLinePaint.setStyle(Style.STROKE);
		mLinePaint.setStrokeWidth(3);
		mLinePaint.setColor(Color.BLUE);
		mLinePaint.setFilterBitmap(true);
		mLinePaint.setDither(true);
		
		mOutterPaint.setStyle(Style.FILL);
		mOutterPaint.setColor(Color.BLACK);
		mOutterPaint.setFilterBitmap(true);
		mOutterPaint.setDither(true);
	}
	
	/**
	 * 사용자의 스마트 폰 화면에 맞춰 이미지 크기를 조절
	 * 
	 */
	private void initLayoutParams(int index, int width, int height) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "현재 화면 크기 : " + mScreenPoint.x + ", " + mScreenPoint.y);
		}
		
		switch(index) {
			case ScrapCrop.LOAD_GALLERY:
				if(Constants.DEVELOPE_MODE) {
					Utils.logPrint(getClass(), "갤러리에서 호출, 이미지 사이즈 초기화");
				}
				
				float srcWidth = mSrcBitmap.getWidth();
				float srcHeight = mSrcBitmap.getHeight();
	
				// 이미지의 크기가 화면을 넘어가면, 짧은 쪽을 기준으로 비율을 맞춘다.
				if(srcWidth > mScreenPoint.x || srcHeight > mScreenPoint.y) {
				
					// ver 1.01 비율 공식
//					float ratioValue = 1;
//					
//					if(Constants.DEVELOPE_MODE) {
//						Utils.logPrint(getClass(), "이미지 크기 : " + srcWidth + ", " + srcHeight);
//					}
//					
//					if(srcWidth < srcHeight) {
//						ratioValue = (float)mSrcBitmap.getWidth() / mScreenPoint.x;
//					} else {
//						ratioValue = (float)mSrcBitmap.getHeight() / mScreenPoint.y;
//					}
//					
//					if(ratioValue == 0) {
//						ratioValue = 1;
//					}
//					
//					if(Constants.DEVELOPE_MODE) {
//						Utils.logPrint(getClass(), "변경 비율 값 : " + ratioValue);
//					}
//			
//					mChangeWidth = (int)(mSrcBitmap.getWidth() / ratioValue);
//					mChangeHeight = (int)(mSrcBitmap.getHeight() / ratioValue);
					
					// ver 1.02 비율 공식 수정
					
					// 이미지의 가로 길이가 세로 길이보다 더 길 경우,
					if(srcWidth >= srcHeight) {
						mChangeWidth = mScreenPoint.x;
						mChangeHeight = (int) ((srcHeight * mScreenPoint.x) / srcWidth);
					} 
					// 이미지의 세로 길이가 가로 길이보다 더 길 경우,
					else {
						mChangeWidth = (int) ((srcWidth * mScreenPoint.y) / srcHeight);
						mChangeHeight = mScreenPoint.y;
					}
					
					if(Constants.DEVELOPE_MODE) {
						Utils.logPrint(getClass(), "1차 수정된 이미지 크기 : " + mChangeWidth + ", " + mChangeHeight);
					}
					
					// 만약, 수정된 이미지 비율이 화면 사이즈보다 클 경우, 2차 수정
					if(mChangeWidth > mScreenPoint.x || mChangeHeight > mScreenPoint.y) {
						for(float i=1; i > 0; i -= 0.1) {
							int testWidth = (int) (mChangeWidth * i);
							int testHeight = (int) (mChangeHeight * i);
							
							if(testWidth <= mScreenPoint.x && testHeight <= mScreenPoint.y) {
								mChangeWidth = testWidth;
								mChangeHeight = testHeight;
								break;
							}
						}
					}
					
					if(Constants.DEVELOPE_MODE) {
						Utils.logPrint(getClass(), "2차 수정된 이미지 크기 : " + mChangeWidth + ", " + mChangeHeight);
					}
					
					
				} else {
					mChangeWidth = (int)srcWidth;
					mChangeHeight = (int)srcHeight;
				}
				
				break;
				
			case ScrapCrop.LOAD_CAMERA:
				if(Constants.DEVELOPE_MODE) {
					Utils.logPrint(getClass(), "카메라에서 호출, 이미지 사이즈 초기화");
				}
				
				mChangeWidth = height;
				mChangeHeight = width;
				
				break;
		}
		
		RelativeLayout.LayoutParams rp = new RelativeLayout.LayoutParams(mChangeWidth, mChangeHeight);
		rp.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
		
		this.setLayoutParams(rp);
		
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "변경된 화면 크기 : " + mChangeWidth + ", " + mChangeHeight);
		}
		
		// 변경된 화면 크기에 맞춰 이미지를 표시 해주기 위한 Rectangle 생성
		mSrcRect = new Rect(0, 0, mChangeWidth, mChangeHeight);
	}
	
	private float getReSizeValueForMemory(int width, int height) {
		float memorySize = width * height * 4 / (float)1024000;
		
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "크롭한 이미지가 차지하는 예상 메모리 크기 : " + memorySize);
		}
		
		float samplingValue = 1.0f;
		
		while(true) {
			memorySize = (float)memorySize / samplingValue;
			
			if(memorySize < CROP_VIEW_LIMITED_MEMORY) {
				break;
			} else {
				samplingValue += 0.2f;
			}
		}
		
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "줄이게 된 후의 예상 메모리 : " + memorySize + ", 줄여야 할 값 : " + samplingValue);
		}
		
		return samplingValue;
		
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		mCurrentAction = event.getAction();
		
		switch(mCurrentAction) {
			case MotionEvent.ACTION_DOWN:
				mArrayScrapPos.clear();
				initCancelState();
				break;
				
			case MotionEvent.ACTION_MOVE:
				mTouchX = event.getX();
				mTouchY = event.getY();
				
				if(mTouchX <= 0) {
					mTouchX = 0;
				}
				
				if(mTouchY <= 0) {
					mTouchY = 0;
				}
				
				if(mTouchX >= mChangeWidth) {
					mTouchX = mChangeWidth;
				}
				
				if(mTouchY >= mChangeHeight) {
					mTouchY = mChangeHeight;
				}
				
				ScrapViewPos scrapPos = new ScrapViewPos(mTouchX, mTouchY);
				mArrayScrapPos.add(scrapPos);
				
				invalidate();
				break;
			
			case MotionEvent.ACTION_UP:

				int scrapPosSize = mArrayScrapPos.size();
				
				if(scrapPosSize > 0) {
					invalidate();
				} else {
					if(!mScrapDialogShow) {
						createSaveImageDialog(mScrapDialogShow, SAVE_FULL_IMAGE);
						mScrapDialogShow = true;
					}
				}
				
				break;
		}
		
		return true;
	}
	
	@Override
	public void draw(Canvas canvas) {
		
		canvas.drawBitmap(mSrcBitmap, null, mSrcRect, mSrcPaint);
		
		int scrapPosSize = mArrayScrapPos.size();
		
		switch(mCurrentAction) {
			case MotionEvent.ACTION_MOVE:
				
				for(int i=0; i < scrapPosSize; i++) {
					if(i == 0) {
						int firstX = (int) mArrayScrapPos.get(0).getStartX();
						int firstY = (int) mArrayScrapPos.get(0).getStartY();
						
						canvas.drawPoint(firstX, firstY, mLinePaint);
						
						mScrapTopLeftX = firstX;
						mScrapTopLeftY = firstY;
						mScrapBottomRightX = firstX;
						mScrapBottomRightY = firstY;
						
					} else if(i >= 1) {
						float prevX = mArrayScrapPos.get(i - 1).getStartX();
						float prevY = mArrayScrapPos.get(i - 1).getStartY();
						float currX = mArrayScrapPos.get(i).getStartX();
						float currY = mArrayScrapPos.get(i).getStartY();
						
						canvas.drawLine(prevX, prevY, currX, currY, mLinePaint);
					}
				}
				
				break;
		
			case MotionEvent.ACTION_UP:
				
				if(scrapPosSize > 1) {
				
					Path path = new Path();
					path.reset();
					
					for(int i=0; i < scrapPosSize; i++) {
						int x = (int) mArrayScrapPos.get(i).getStartX();
						int y = (int) mArrayScrapPos.get(i).getStartY();
						
						if(i == 0) {
							path.moveTo(x, y);
							
							mScrapTopLeftX = x;
							mScrapTopLeftY = y;
							mScrapBottomRightX = x;
							mScrapBottomRightY = y;
							
						} else if (i < scrapPosSize - 1) {
							path.lineTo(x, y);
						} else if (i == scrapPosSize - 1) {
							path.lineTo(mArrayScrapPos.get(0).getStartX(), mArrayScrapPos.get(0).getStartY());
						}

						if(x < mScrapTopLeftX) {
							mScrapTopLeftX = x;
						}
						
						if(x >= mScrapBottomRightX) {
							mScrapBottomRightX = x;
						}
						
						if(y < mScrapTopLeftY) {
							mScrapTopLeftY = y;
						}
						
						if(y >= mScrapBottomRightY) {
							mScrapBottomRightY = y;
						}
					}

					path.setFillType(FillType.INVERSE_WINDING);
					mScrapPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
					canvas.drawPath(path, mScrapPaint);
					
					// 잘라낸 부분을 제외한 나머지 부분을 흑백으로 처리
					canvas.drawRect(0, 0, mScreenPoint.x, mScrapTopLeftY, mOutterPaint);
					canvas.drawRect(mScrapBottomRightX, 0, mScreenPoint.x, mScreenPoint.y, mOutterPaint);
					canvas.drawRect(0, mScrapBottomRightY, mScreenPoint.x, mScreenPoint.y, mOutterPaint);
					canvas.drawRect(0, 0, mScrapTopLeftX, mScreenPoint.y, mOutterPaint);
					
					
					if(!mScrapDialogShow) {
						createSaveImageDialog(mScrapDialogShow, SAVE_CROP_IMAGE);
						mScrapDialogShow = true;
					}
					
					
				}
				
				break;
		}
	}
	
	private void createSaveImageDialog(boolean flag, final int saveState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setMessage(mContext.getString(R.string.crop_view_dig_scrap_msg));
		builder.setPositiveButton(mContext.getString(R.string.confirm_dig_ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {

				// 파일을 저장한 뒤, 해당 내용을 업데이트 해준다. 그리고 SavePage 로 인텐트 실행
				CropSaveAsync cropSaveAsync = new CropSaveAsync(dialog, saveState);
				cropSaveAsync.execute();
				
			}
		});
		builder.setNegativeButton(mContext.getString(R.string.confirm_dig_cancel), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				
				initCancelState();
			}
		});

		builder.show();
	}
	
	private void initCancelState() {
		mCurrentAction = -1;
		setDrawingCacheEnabled(false);
		
		mScrapDialogShow = false;
		invalidate();
	}
	
	class CropSaveAsync extends AsyncTask<Void, Void, Void> {
		
		ProgressDialog prgDig;
		DialogInterface dialog;
		int saveState;
		boolean executeFlag;
		
		public CropSaveAsync(DialogInterface dialog, int saveState) {
			this.dialog = dialog;
			this.saveState = saveState;
		}
		
		@Override
		protected void onPreExecute() {
			prgDig = ProgressDialog.show(mContext, "", mContext.getString(R.string.crop_view_dig_saving_img));
		}
		
		/**
		 * 캡쳐한 현재 화면을 비트맵으로 리턴합니다. 
		 * 
		 * @return
		 */
		private Bitmap getCapturedScreenShot() {
			setDrawingCacheEnabled(true);
			buildDrawingCache(true);
			
			return getDrawingCache();
		}
		
		
		@Override
		protected Void doInBackground(Void... params) {
			if(Constants.DEVELOPE_MODE) {
				Utils.logPrint(getClass(), "Crop 한 이미지 저장 중");
			}
			
			// 이미지 파일 저장
			Bitmap currentBitmap = getCapturedScreenShot();
			Bitmap saveBitmap = null;
			
			switch(saveState) {
				// 크롭 한 이미지를 적정 사이즈에 맞춰 Bitmap 으로 변환 처리
				case SAVE_CROP_IMAGE:
					int width = mScrapBottomRightX - mScrapTopLeftX;
					int height = mScrapBottomRightY - mScrapTopLeftY;
					
					float cropResizeValue = getReSizeValueForMemory(width, height);
					
					Bitmap tempBitmap = Bitmap.createBitmap(currentBitmap, mScrapTopLeftX, mScrapTopLeftY, width, height);
					saveBitmap = Bitmap.createScaledBitmap(tempBitmap, (int)(width / cropResizeValue), (int)(height / cropResizeValue), true);

					Utils.recycleBitmap(tempBitmap);
					
					break;
					
				// 기존 이미지 전체를 적정 사이즈에 맞춰 Bitmap 으로 변환 처리
				case SAVE_FULL_IMAGE:
					if(Constants.DEVELOPE_MODE) {
						Utils.logPrint(getClass(), "전체 저장 시 크기 : " + mChangeWidth + ", " + mChangeHeight);
					}

					float fullResizeValue = getReSizeValueForMemory(mChangeWidth, mChangeHeight);
					
					int fWidth = (int)(mChangeWidth / fullResizeValue);
					int fHeight = (int)(mChangeHeight / fullResizeValue);
					
					if(Constants.DEVELOPE_MODE) {
						Utils.logPrint(getClass(), "변경 전 스크랩 이미지 가로, 세로 : " + mChangeWidth + ", " + mChangeHeight);
						Utils.logPrint(getClass(), "변경 후 스크랩 이미지 가로, 세로 : " + fWidth + ", " + fHeight);
					}
					
					saveBitmap = Bitmap.createScaledBitmap(currentBitmap, fWidth, fHeight, true);
					break;
			}

			
			Utils.recycleBitmap(currentBitmap);
			
			// saveBitmap 을 SD-Card 에 저장합니다.
			String filePath = Utils.getFilePathName(mContext, Constants.SAVE_FILE_PATH_SCRAP);
			
			if(filePath != null) {
				Utils.saveBitmapToFileCache(saveBitmap, filePath);
				
				mContext.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
				
				// sd 카드 저장 끝
	
				setDrawingCacheEnabled(false);
				
				// 스크랩한 이미지를 ArrayList에 저장 한 뒤, 종료한다.
				if(Constants.DEVELOPE_MODE) {
					Utils.logPrint(getClass(), "추가 전 스크랩 Array 크기 : " + mMessagePool.getCropViewPosArray().size());
				}
				
				CropViewPos cropViewPos = new CropViewPos(filePath, mScrapTopLeftX, mScrapTopLeftY, 0);
				cropViewPos.setNoDegreeResize(saveBitmap.getWidth(), saveBitmap.getHeight());
				cropViewPos.setDegreeResize(saveBitmap.getWidth(), saveBitmap.getHeight());
				mMessagePool.getCropViewPosArray().add(cropViewPos);
	
				if(Constants.DEVELOPE_MODE) {
					Utils.logPrint(getClass(), "추가 후 스크랩 Array 크기 : " + mMessagePool.getCropViewPosArray().size());
				}
				
				executeFlag = true;
			} else {
				executeFlag = false;
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {		//	파일 저장과 ArrayList의 추가가 완료 되었다면, ScrapPage 를 호출한다.
			if(prgDig.isShowing()) {
				prgDig.dismiss();
			}
			
			if(executeFlag) {
				ScrapViewLayout.setScrapViewToLayout();		//	ScrapViewLayout 재설정
				
				mCurrentAction = -1;
				dialog.dismiss();
				
				mScrapDialogShow = false;
				
				// 스크랩 후에는 메인 모드를 에디트 모드로 재설정
				ScrapPage.setCurrentModeExternalCall(Constants.MENU_MODE_EDIT);
				
				// 비트맵 초기화
				Utils.recycleBitmap(mSrcBitmap);
				
				// 스크랩 Activity 종료
				ScrapCrop.SCRAP_IMAGE_CROP_ACTIVITY.finish();
			} else {
				Toast.makeText(mContext, mContext.getString(R.string.dig_check_sd_card_msg), Toast.LENGTH_SHORT).show();
				
				initCancelState();
				
			}
		}
	}
}










