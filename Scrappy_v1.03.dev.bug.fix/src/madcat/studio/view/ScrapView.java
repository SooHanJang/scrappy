package madcat.studio.view;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.position.CropViewPos;
import madcat.studio.scrap.R;
import madcat.studio.scrap.ScrapPage;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.view.MotionEvent;
import android.view.View;

public class ScrapView extends View {
	
	public static final int VIEW_ID									=	100;
	
	public static final int SCRAP_STATE_INIT							=	0;
	public static final int SCRAP_STATE_INIT_DRAW					=	1;
	public static final int SCRAP_STATE_EDIT_INIT					=	2;
	public static final int SCRAP_STATE_EDIT_MOVE					=	3;
	public static final int SCRAP_STATE_EDIT_RESIZE					=	4;
	public static final int SCRAP_STATE_EDIT_ROTATE					=	5;
	
	private final int ANCHOR_RADIUS									=	20;
	private final int ANCHOR_ROTATE_RADIUS							=	30;
	private final int ANCHOR_ROTATE_HEIGHT_POS						=	50;
	
	private Context mContext;
	private MessagePool mMessagePool;
	private int mIndex;
	private boolean mInnerTouchFlag									=	false;
	
	private int mScrapState											=	SCRAP_STATE_INIT;
	
	private Bitmap mOriginBitmap;
	private Bitmap mOriginDegreeBitmap;
	private Bitmap mSrcNoDegreeBitmap;
	private Bitmap mSrcDegreeBitmap;
	
	private float mDegrees;
	
	private Paint mSrcPaint;
	private Paint mRectPaint;
	private Paint mOvalPaint										=	new Paint(Paint.ANTI_ALIAS_FLAG);
	
	private float mBasePtX, mBasePtY;

	// 앵커 관련 변수
	private RectF mLTAnchor, mRTAnchor, mLBAnchor, mRBAnchor;
	private boolean mLTAnchorPress, mRTAnchorPress, mLBAnchorPress, mRBAnchorPress;
	
	// 회전 관련 변수
	private RectF mRotateAnchor;
	private Bitmap mRotateBitmap;
	private double mPosDegree;

	private Bitmap mTrashBitmap;
	private float mTrashXPos, mTrashYPos;
	
	private CropViewPos mCropViewPos;
	
	private RectF mNoDegreeOutterRect, mDegreeOutterRect;
	
	private Paint mTestPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

	/*
	 * 0.5954 dev.ratio_resize
	 */
	private float mNoDegreeRatioX, mNoDegreeRatioY;
	private float mDegreeRatioX, mDegreeRatioY;
	
	public ScrapView(Context context) {
		super(context);
	}
	
	public ScrapView(Context context, CropViewPos cropViewPos, int index) {
		super(context);
		this.mContext = context;
		this.mMessagePool = (MessagePool)context.getApplicationContext();
		this.mCropViewPos = cropViewPos;
		this.mIndex = index;
		
		// ScrapView 의 View ID 설정
		this.setId(VIEW_ID);

		// 이미지 각도 불러오기
		this.mDegrees = mCropViewPos.getDegree();
		
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "불러온 각도 : " + mDegrees);
		}
		
		// 소스 원본 이미지와 관련된 정보 설정 불러오기
		this.mOriginBitmap = BitmapFactory.decodeFile(cropViewPos.getFilePath(), Utils.getBitmapOptions());
		this.mOriginDegreeBitmap = Utils.getRotateBitmap(mOriginBitmap.copy(Constants.IMAGE_QUALITY_TYPE, false), (int)mDegrees);
		this.mSrcNoDegreeBitmap =
			Utils.getLoadBitmap(mOriginBitmap, mCropViewPos.getNoDegreeWidth(), mCropViewPos.getNoDegreeHeight(), 0);
		this.mSrcDegreeBitmap = 
			Utils.getLoadBitmap(mOriginDegreeBitmap, mCropViewPos.getDegreeWidth(), mCropViewPos.getDegreeHeight(), 0);
		
		
		// 영역 표시에 사용할 RectF 객체 생성
		this.mNoDegreeOutterRect = 
			new RectF(mCropViewPos.getNoDegreeStartX(), mCropViewPos.getNoDegreeStartY(), 
					mCropViewPos.getNoDegreeStartX() + mCropViewPos.getNoDegreeWidth(),
					mCropViewPos.getNoDegreeStartY() + mCropViewPos.getNoDegreeHeight());
		
		this.mDegreeOutterRect = 
			new RectF(mCropViewPos.getStartX(), mCropViewPos.getStartY(),
					mCropViewPos.getStartX() + mCropViewPos.getDegreeWidth(),
					mCropViewPos.getStartY() + mCropViewPos.getDegreeHeight());

		/*
		 * 0.5954 dev.ratio_resize
		 */
		// 수정된 비율을 적용
		if(mNoDegreeOutterRect.width() >= mNoDegreeOutterRect.height()) {
			mNoDegreeRatioX = 1;
			mNoDegreeRatioY = mNoDegreeOutterRect.height() / mNoDegreeOutterRect.width();
		} else {
			mNoDegreeRatioX = mNoDegreeOutterRect.width() / mNoDegreeOutterRect.height();
			mNoDegreeRatioY = 1;
		}
		
		
		if(mDegreeOutterRect.width() >= mDegreeOutterRect.height()) {
			mDegreeRatioX = 1;
			mDegreeRatioY = mDegreeOutterRect.height() / mDegreeOutterRect.width();
		} else {
			mDegreeRatioX = mDegreeOutterRect.width() / mDegreeOutterRect.height();
			mDegreeRatioY = 1;
		}
		
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "NoDegree 비율 : " + mNoDegreeRatioX + ", " + mNoDegreeRatioY);
			Utils.logPrint(getClass(), "Degree 비율 : " + mDegreeRatioX + ", " + mDegreeRatioY);
		}
		
		
		// 삭제 이미지 불러오기
		mTrashBitmap = ((BitmapDrawable)getResources().getDrawable(R.drawable.icon_trash_close)).getBitmap(); 
		
		Point screenPoint = Utils.getScreenSize(mContext);
		mTrashXPos = (screenPoint.x - mTrashBitmap.getWidth()) / 2;
		mTrashYPos = 10;

		// 앵커 객체 초기화
		mLTAnchor = new RectF();
		mRTAnchor = new RectF();
		mLBAnchor = new RectF();
		mRBAnchor = new RectF();
		mRotateAnchor = new RectF();
		
		// 회전 앵커 객체 초기화
		mRotateBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.icon_rotate);
		
		// 페인트 초기화
		initPaint();
		
		// 테스트 코드
		mTestPaint.setColor(Color.RED);
		mTestPaint.setStyle(Style.STROKE);
		mTestPaint.setStrokeWidth(3);
		
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		if(mMessagePool.getMode() != Constants.MENU_MODE_EDIT) {
			mScrapState = SCRAP_STATE_INIT;
		}
		
		canvas.setDrawFilter(new PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG | Paint.DITHER_FLAG));
		
		switch(mScrapState) {

			case SCRAP_STATE_INIT:			//	처음 이미지가 스크랩 페이지에 불려져 왔을 시, (무조건 처음에만 호출 한다.)

				canvas.drawBitmap(mSrcDegreeBitmap, mDegreeOutterRect.left, mDegreeOutterRect.top, mSrcPaint);
				
				break;
				
			case SCRAP_STATE_INIT_DRAW:		//	이미지에 여러가지 가공을 거쳤을 경우, 이에 따른 초기화로 호출 된다. (처음 초기화 이후의 초기화들은 이 모드를 이용한다.)

				canvas.drawBitmap(mSrcDegreeBitmap, mDegreeOutterRect.left, mDegreeOutterRect.top, mSrcPaint);
				
				break;
				
			case SCRAP_STATE_EDIT_INIT:
			case SCRAP_STATE_EDIT_MOVE:
				
				canvas.drawBitmap(mSrcDegreeBitmap, mDegreeOutterRect.left, mDegreeOutterRect.top, mSrcPaint);
				drawOutter(canvas, mDegreeOutterRect);
				
				break;
				
			case SCRAP_STATE_EDIT_RESIZE:
				
				mSrcDegreeBitmap = Utils.getResizedBitmap(mOriginDegreeBitmap, mDegreeOutterRect.width(), mDegreeOutterRect.height());
				mSrcNoDegreeBitmap = Utils.getResizedBitmap(mOriginBitmap, mNoDegreeOutterRect.width(), mNoDegreeOutterRect.height());

				canvas.drawBitmap(mSrcDegreeBitmap, mDegreeOutterRect.left, mDegreeOutterRect.top, mSrcPaint);
				drawOutter(canvas, mDegreeOutterRect);
				break;
				
			case SCRAP_STATE_EDIT_ROTATE:
				canvas.drawBitmap(mSrcDegreeBitmap, mDegreeOutterRect.left, mDegreeOutterRect.top, mSrcPaint);
				break;
				
		}
	}
	
	// 어떤 오브젝트에서 터치 이벤트가 발생되었는 지를 체크한다.
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		
		
		if(mMessagePool.getMode() == Constants.MENU_MODE_EDIT) {
			
			float touchX = event.getX();
			float touchY = event.getY();
			
			if(event.getAction() == MotionEvent.ACTION_DOWN) {
				
				ScrapPage.setToolMenuVisibility(false);
				
				if(touchX >= (mDegreeOutterRect.left - ANCHOR_RADIUS) && touchX <= (mDegreeOutterRect.right + ANCHOR_RADIUS) 
						&& touchY >= (mDegreeOutterRect.top - ANCHOR_RADIUS) && touchY <= (mDegreeOutterRect.bottom + ANCHOR_RADIUS)) {
					
					mMessagePool.setZOrderTopIndex(mIndex);
					
					ArrayList<ScrapView> scrapViewArray = mMessagePool.getScrapViewArray();
					
					for(int i=0; i < scrapViewArray.size(); i++) {
						
						ScrapView scrapView = scrapViewArray.get(i);
						
						if(i != mMessagePool.getZOrderTopIndex()) {		//	현재 비활성화된 영역들
							
							if(Constants.DEVELOPE_MODE) {
								Utils.logPrint(getClass(), i + " 번째 이미지를 비활성화 합니다.");
							}
							
							scrapView.setScrapState(SCRAP_STATE_INIT_DRAW);
							scrapView.setInnerTouchFlag(false);
							
							scrapView.invalidate();
							
							
						} else {		//	현재 활성화된 영역
							if(Constants.DEVELOPE_MODE) {
								Utils.logPrint(getClass(), i + " 번째 이미지를 활성화 합니다.");
							}
							
							scrapView.setScrapState(SCRAP_STATE_EDIT_INIT);
							scrapView.setInnerTouchFlag(true);
							scrapView.invalidate();
							
							mLTAnchorPress = mLTAnchor.contains(touchX, touchY);
							mRTAnchorPress = mRTAnchor.contains(touchX, touchY);
							mLBAnchorPress = mLBAnchor.contains(touchX, touchY);
							mRBAnchorPress = mRBAnchor.contains(touchX, touchY);
							
							if(mLTAnchorPress || mRTAnchorPress || mLBAnchorPress || mRBAnchorPress) {
								
								scrapView.setScrapState(SCRAP_STATE_EDIT_RESIZE);		//	사이즈 수정 모드로 전환
								
							} else {
								
								scrapView.setScrapState(SCRAP_STATE_EDIT_MOVE);			//	이동 모드로 전환
								
								mBasePtX = touchX;
								mBasePtY = touchY;
								
							}
							
							if(mScrapState == SCRAP_STATE_EDIT_INIT || mScrapState == SCRAP_STATE_EDIT_MOVE) {
								ScrapPage.setTrashMenuVisibility(true);
								ScrapPage.setLayerUpDownMenuVisibility(true);
							}
						}
						
						if(i == scrapViewArray.size() - 1) {
							scrapView.invalidate();
						}
						
					}
	
					return true;
					
				} else {
					
					if(mInnerTouchFlag && mRotateAnchor.contains(touchX, touchY)) {
						mScrapState = SCRAP_STATE_EDIT_ROTATE;
						
						mBasePtX = touchX;
						mBasePtY = touchY;
						
						invalidate();
						
						return true;
					} else {
						
						mScrapState = SCRAP_STATE_INIT_DRAW;
						mInnerTouchFlag = false;
						
						ScrapPage.setTrashMenuVisibility(false);
						ScrapPage.setLayerUpDownMenuVisibility(false);
						
						invalidate();
						
						return false;
					}
					
				}
				
			} 
	
			return super.dispatchTouchEvent(event);
		}
		
		return false;
	}

	/**
	 * 특정 오브젝트 내에서 터치 이벤트가 발생하였다면, 해당 오브젝트에 대한 터치 이벤트를 onTouchEvent 에서 처리한다.
	 * 1. 먼저, zOrderTop 의 처리로, 화면에 맨 앞에 존재하는 것이 무엇인지 파악한다.
	 * 2. 현재 맨 앞에 클릭된 뷰를 제외한 나머지 뷰는 모두 초기화 한다.
	 * 
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {

		float touchX = event.getX();
		float touchY = event.getY();
		
		if(event.getAction() == MotionEvent.ACTION_MOVE) {

			switch(mScrapState) {
			
				case SCRAP_STATE_EDIT_MOVE:
					
					float gapX = touchX - mBasePtX;
					float gapY = touchY - mBasePtY;
					
					mNoDegreeOutterRect.left += gapX;
					mNoDegreeOutterRect.right += gapX;
					mNoDegreeOutterRect.top += gapY;
					mNoDegreeOutterRect.bottom += gapY;
					
					mDegreeOutterRect.left += gapX;
					mDegreeOutterRect.right += gapX;
					mDegreeOutterRect.top += gapY;
					mDegreeOutterRect.bottom += gapY;
					
					
					mBasePtX = touchX;
					mBasePtY = touchY;
					
					// 휴지통에 터치 좌표가 들어갔을 경우, 휴지통 이미지를 변경해준다.
					if(touchX >= mTrashXPos && touchX <= (mTrashXPos + mTrashBitmap.getWidth()) &&
							touchY >= mTrashYPos && touchY <= (mTrashYPos + mTrashBitmap.getHeight())) {
						ScrapPage.changeTrashIcon(true);
					} else {
						ScrapPage.changeTrashIcon(false);
					}
	
					invalidate();
					
					break;
				
				case SCRAP_STATE_EDIT_RESIZE:
					
					// 좌측 상단 앵커 수정
					if(mLTAnchorPress) {
						
						float maxDegreeXPos = mDegreeOutterRect.right - ANCHOR_RADIUS;
						float maxDegreeYPos = mDegreeOutterRect.bottom - ANCHOR_RADIUS;
						
						if(touchX > maxDegreeXPos) {
							touchX = maxDegreeXPos;
						}
						
						if(touchY > maxDegreeYPos) {
							touchY = maxDegreeYPos;
						}
						
						
						// 좌표 차이 값 계산
						float ltGapX = mDegreeOutterRect.left - touchX;
						float ltGapY = mDegreeOutterRect.top - touchY;
						
						if(ltGapX <= ltGapY) {
							ltGapY = ltGapX;
						}
						
						else {
							ltGapX = ltGapY;
						}
						
						// 비율에 따른 좌표 차이 재계산
						ltGapX = mDegreeRatioX * ltGapX;
						ltGapY = mDegreeRatioY * ltGapY;
						
						mNoDegreeOutterRect.left -= ltGapX;
						mNoDegreeOutterRect.top -= ltGapY;
						
						mDegreeOutterRect.left -= ltGapX;
						mDegreeOutterRect.top -= ltGapY;
						
						// 최소 로테이션 범위를 넘었을 경우, 좌표 값을 복구
						// Width 계산
						if(mNoDegreeOutterRect.left + (ANCHOR_RADIUS * 2) > mNoDegreeOutterRect.right) {
							mNoDegreeOutterRect.left += ltGapX;
							mNoDegreeOutterRect.top += ltGapY;
							
							mDegreeOutterRect.left += ltGapX;
							mDegreeOutterRect.top += ltGapY;
						}
						
						// Height 계산
						if(mNoDegreeOutterRect.top + (ANCHOR_RADIUS * 2) > mNoDegreeOutterRect.bottom) {
							mNoDegreeOutterRect.left += ltGapX;
							mNoDegreeOutterRect.top += ltGapY;
							
							mDegreeOutterRect.left += ltGapX;
							mDegreeOutterRect.top += ltGapY;
						}
					} 
					
					// 우측 상단 앵커 수정
					else if(mRTAnchorPress) {
						
						float minDegreeXPos = mDegreeOutterRect.left + ANCHOR_RADIUS;
						float maxDegreeYPos = mDegreeOutterRect.bottom - ANCHOR_RADIUS;
						
						if(touchX < minDegreeXPos) {
							touchX = minDegreeXPos;
						}
						
						if(touchY > maxDegreeYPos) {
							touchY = maxDegreeYPos;
						}
						
						// 좌표 차이 값 계산
						float rtGapX = touchX - mDegreeOutterRect.right;	//	> 0
						float rtGapY = mDegreeOutterRect.top - touchY;		//	> 0
						
						if(rtGapX <= rtGapY) {
							rtGapY = rtGapX;
						}
						
						else {
							rtGapX = rtGapY;
						}
						
						// 비율에 따른 좌표 차이 재계산
						rtGapX = mDegreeRatioX * rtGapX;
						rtGapY = mDegreeRatioY * rtGapY;
						
						// 각도가 포함되지 않은 외곽선 표시 값 계산
						mNoDegreeOutterRect.top -= rtGapY;
						mNoDegreeOutterRect.right += rtGapX;
						
						// 각도를 포함한 외곽선 표시 값 계산
						mDegreeOutterRect.top -= rtGapY;
						mDegreeOutterRect.right += rtGapX;
						
						// 최소 로테이션 범위를 넘었을 경우, 좌표 값을 복구
						// Width 계산
						if(mNoDegreeOutterRect.right - (ANCHOR_RADIUS * 2) < mNoDegreeOutterRect.left) {
							mNoDegreeOutterRect.top += rtGapY;
							mNoDegreeOutterRect.right -= rtGapX;
							
							mDegreeOutterRect.top += rtGapY;
							mDegreeOutterRect.right -= rtGapX;
						}
						
						// Height 계산
						if(mNoDegreeOutterRect.bottom - (ANCHOR_RADIUS * 2) < mNoDegreeOutterRect.top) {
							mNoDegreeOutterRect.top += rtGapY;
							mNoDegreeOutterRect.right -= rtGapX;
							
							mDegreeOutterRect.top += rtGapY;
							mDegreeOutterRect.right -= rtGapX;
						}
						
						
						
					} 

					// 좌측 하단 앵커 수정
					else if(mLBAnchorPress) {
						
						float maxDegreeXPos = mDegreeOutterRect.right - ANCHOR_RADIUS;
						float minDegreeYPos = mDegreeOutterRect.top + ANCHOR_RADIUS;
						
						if(touchX > maxDegreeXPos) {
							touchX = maxDegreeXPos;
						}
						
						if(touchY < minDegreeYPos) {
							touchY = minDegreeYPos;
						}
						
						// 좌표 차이 값 계산
						float lbGapX = mDegreeOutterRect.left - touchX;			//	> 0
						float lbGapY = touchY - mDegreeOutterRect.bottom;		//	> 0
						
						if(lbGapX <= lbGapY) {
							lbGapY = lbGapX;
						}
						
						else {
							lbGapX = lbGapY;
						}
						
						// 비율에 따른 좌표 차이 재계산
						lbGapX = mDegreeRatioX * lbGapX;
						lbGapY = mDegreeRatioY * lbGapY;
						
						// 각도가 포함되지 않은 외곽선 표시 값 계산
						mNoDegreeOutterRect.left -= lbGapX;
						mNoDegreeOutterRect.bottom += lbGapY;
						
						// 각도를 포함한 외곽선 표시 값 계산
						mDegreeOutterRect.left -= lbGapX;
						mDegreeOutterRect.bottom += lbGapY;
						
						// 최소 로테이션 범위를 넘었을 경우, 좌표 값을 복구
						// Width 계산
						if(mNoDegreeOutterRect.left + (ANCHOR_RADIUS * 2) > mNoDegreeOutterRect.right) {
							mNoDegreeOutterRect.left += lbGapX;
							mNoDegreeOutterRect.bottom -= lbGapY;
							
							mDegreeOutterRect.left += lbGapX;
							mDegreeOutterRect.bottom -= lbGapY;
						}
						
						// Height 계산
						if(mNoDegreeOutterRect.top + (ANCHOR_RADIUS * 2) > mNoDegreeOutterRect.bottom) {
							mNoDegreeOutterRect.left += lbGapX;
							mNoDegreeOutterRect.bottom -= lbGapY;
							
							mDegreeOutterRect.left += lbGapX;
							mDegreeOutterRect.bottom -= lbGapY;
						}
						
					} 
					
					// 우측 하단 앵커 수정
					else if(mRBAnchorPress) {
						
						float minDegreeXPos = mDegreeOutterRect.left + ANCHOR_RADIUS;
						float minDegreeYPos = mDegreeOutterRect.top + ANCHOR_RADIUS;
						
						if(touchX < minDegreeXPos) {
							touchX = minDegreeXPos;
						}
						
						if(touchY < minDegreeYPos) {
							touchY = minDegreeYPos;
						}
						
						float rbGapX = touchX - mDegreeOutterRect.right;	//	> 0
						float rbGapY = touchY - mDegreeOutterRect.bottom;	//	> 0
						
						if(rbGapX <= rbGapY) {
							rbGapY = rbGapX;
						}
						
						else {
							rbGapX = rbGapY;
						}
						
						// 비율에 따른 좌표 차이 재계산
						rbGapX = mDegreeRatioX * rbGapX;
						rbGapY = mDegreeRatioY * rbGapY;
						
						// 각도가 포함되지 않은 외곽선 표시 값 계산
						mNoDegreeOutterRect.right += rbGapX;
						mNoDegreeOutterRect.bottom += rbGapY;
						
						// 각도를 포함한 외곽선 표시 값 계산
						mDegreeOutterRect.right += rbGapX;
						mDegreeOutterRect.bottom += rbGapY;
						
						// 최소 로테이션 범위를 넘었을 경우, 좌표 값을 복구
						// Width 계산
						if(mNoDegreeOutterRect.right - (ANCHOR_RADIUS * 2) < mNoDegreeOutterRect.left) {
							mNoDegreeOutterRect.right -= rbGapX;
							mNoDegreeOutterRect.bottom -= rbGapY;
							
							mDegreeOutterRect.right -= rbGapX;
							mDegreeOutterRect.bottom -= rbGapY;
						}
						
						// Height 계산
						if(mNoDegreeOutterRect.bottom - (ANCHOR_RADIUS * 2) < mNoDegreeOutterRect.top) {
							mNoDegreeOutterRect.right -= rbGapX;
							mNoDegreeOutterRect.bottom -= rbGapY;
							
							mDegreeOutterRect.right -= rbGapX;
							mDegreeOutterRect.bottom -= rbGapY;
						}
						
					}
					
					invalidate();
					break;
					
				case SCRAP_STATE_EDIT_ROTATE:
					
					if(touchY < mBasePtY) {
						touchY = mBasePtY;
					}

					mPosDegree = getAngle(mBasePtX, mBasePtY, touchX, touchY);
					
					double tempDegree = mDegrees + mPosDegree;
					
					if(Constants.DEVELOPE_MODE) {
						Utils.logPrint(getClass(), "방향각 : " + mPosDegree);
						Utils.logPrint(getClass(), "수정된 각도 : " + tempDegree);
					}
					
					// 회전 좌표 이동
					Matrix matrix = new Matrix();
					Matrix identity = new Matrix();
					matrix.set(identity);
					matrix.setRotate((float) tempDegree, mNoDegreeOutterRect.centerX(), mNoDegreeOutterRect.centerY());
					matrix.mapRect(mDegreeOutterRect, mNoDegreeOutterRect);
					
					mOriginDegreeBitmap = 
						Bitmap.createBitmap(mOriginBitmap, 0, 0, mOriginBitmap.getWidth(), mOriginBitmap.getHeight(), matrix, false); 
					
					mSrcDegreeBitmap = 
						Bitmap.createBitmap(mSrcNoDegreeBitmap, 0, 0, (int)mNoDegreeOutterRect.width(), (int)mNoDegreeOutterRect.height(), matrix, false);
					mSrcDegreeBitmap = Utils.getResizedBitmap(mOriginDegreeBitmap, mDegreeOutterRect.width(), mDegreeOutterRect.height());
					
					/*
					 * 0.5954 dev.ratio_resize
					 */
					// 수정된 비율을 적용
					if(mDegreeOutterRect.width() >= mDegreeOutterRect.height()) {
						mDegreeRatioX = 1;
						mDegreeRatioY = mDegreeOutterRect.height() / mDegreeOutterRect.width();
					} else {
						mDegreeRatioX = mDegreeOutterRect.width() / mDegreeOutterRect.height();
						mDegreeRatioY = 1;
					}

					invalidate();
					break;
			
			}
		
		} else if(event.getAction() == MotionEvent.ACTION_UP) {
			
			switch(mScrapState) {
			
				case SCRAP_STATE_EDIT_MOVE:
					
					if(touchX >= mTrashXPos && touchX <= (mTrashXPos + mTrashBitmap.getWidth()) &&
							touchY >= mTrashYPos && touchY <= (mTrashYPos + mTrashBitmap.getHeight())) {
						
						if(Constants.DEVELOPE_MODE) {
							Utils.logPrint(getClass(), mIndex + "를 쓰레기 통에 버림");
						}
						
						ScrapViewLayout.removeScrapView(this, mIndex);
						
					} else {
						
						saveCropViewPosState();
						
					}
					
					break;
			
				case SCRAP_STATE_EDIT_RESIZE:
					
					saveCropViewPosState();
					
					break;
					
				case SCRAP_STATE_EDIT_ROTATE:
					
					mDegrees = (float) ((mDegrees + mPosDegree) % 360);
					
					saveCropViewPosState();
					
					break;
			
			}
			
			
		}
		
		
		return super.onTouchEvent(event);
	}
	
	public double getAngle(float startX, float startY, float destX, float destY) {  
		
		double dy = destY - startY;  
	    double dx = destX - startX;  

		return Math.atan2(dy, dx) * (360.0 / Math.PI);
	}  
	
	public void setCurrentIndex(int index) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "index(전) -> (후) : " + mIndex + " -> " + index);
		}
		
		this.mIndex = index;
	}
	
	/**
	 * 
	 * 변경된 CropViewPos의 상태를 업데이트 합니다.
	 * 
	 */
	private void saveCropViewPosState() {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "이미지의 변경된 모든 상태를 저장합니다.");
			Utils.logPrint(getClass(), "저장하려는 각도 : " + mDegrees);
		}
		
		// 이미지의 변경된 모든 상태를 저장합니다.
		mCropViewPos.setStartPos(mDegreeOutterRect.left, mDegreeOutterRect.top);
		mCropViewPos.setNoDegreeStartPos(mNoDegreeOutterRect.left, mNoDegreeOutterRect.top);
		mCropViewPos.setNoDegreeResize(mNoDegreeOutterRect.width(), mNoDegreeOutterRect.height());
		mCropViewPos.setDegreeResize(mDegreeOutterRect.width(), mDegreeOutterRect.height());
		mCropViewPos.setDegree(mDegrees);
		
		mMessagePool.getCropViewPosArray().set(mIndex, mCropViewPos);
	}
	
	/**
	 * 지정된 이미지의 외곽 영역을 앵커를 포함하여 표시한다.
	 * 
	 * @param canvas
	 * @param rectF
	 */
	private void drawOutter(Canvas canvas, RectF rectF) {
		canvas.drawRect(rectF, mRectPaint);
		
		// 이미지에 앵커를 그려준다.
		mLTAnchor.set(rectF.left - ANCHOR_RADIUS, rectF.top - ANCHOR_RADIUS, rectF.left + ANCHOR_RADIUS, rectF.top + ANCHOR_RADIUS);
		mRTAnchor.set(rectF.right - ANCHOR_RADIUS, rectF.top - ANCHOR_RADIUS, rectF.right + ANCHOR_RADIUS, rectF.top + ANCHOR_RADIUS);
		mLBAnchor.set(rectF.left - ANCHOR_RADIUS, rectF.bottom - ANCHOR_RADIUS, rectF.left + ANCHOR_RADIUS, rectF.bottom + ANCHOR_RADIUS);
		mRBAnchor.set(rectF.right - ANCHOR_RADIUS, rectF.bottom - ANCHOR_RADIUS, rectF.right + ANCHOR_RADIUS, rectF.bottom + ANCHOR_RADIUS);
		
		// 이미지에 회전 앵커를 그려준다.
		mRotateAnchor.set(rectF.centerX() - ANCHOR_ROTATE_RADIUS, rectF.top - ANCHOR_ROTATE_RADIUS - ANCHOR_ROTATE_HEIGHT_POS, 
				rectF.centerX() + ANCHOR_ROTATE_RADIUS, rectF.top + ANCHOR_ROTATE_RADIUS - ANCHOR_ROTATE_HEIGHT_POS);
		
		
		canvas.drawOval(mLTAnchor, mOvalPaint);
		canvas.drawOval(mRTAnchor, mOvalPaint);
		canvas.drawOval(mLBAnchor, mOvalPaint);
		canvas.drawOval(mRBAnchor, mOvalPaint);
		
		// 회전 앵커 영역을 보여주고 싶다면, 해당 코드를 활성화 한다.
//		canvas.drawOval(mRotateAnchor, mOvalPaint);
		
		canvas.drawBitmap(mRotateBitmap, 
				rectF.centerX() - (mRotateBitmap.getWidth() / 2), 
				rectF.top - ANCHOR_RADIUS - ANCHOR_ROTATE_HEIGHT_POS, 
				mSrcPaint);
		
		
	}
	
	public void setIndex(int index)	{	this.mIndex = index;	}
	public void setScrapState(int scrapState) {	this.mScrapState = scrapState;	}
	public void setInnerTouchFlag(boolean flag)	{	this.mInnerTouchFlag = flag;	}
	public void setDegrees(float degress)	{	this.mDegrees = degress;	}
	
	public int getIndex() {	return mIndex;	}
	public boolean getInnerTouchFlag() {	return mInnerTouchFlag;	}
	public float getDegrees()	{	return mDegrees;	}
	
	/**
	 * Paint 초기화 함수
	 * 
	 */
	private void initPaint() {
		if(mSrcPaint == null) {		mSrcPaint = new Paint(Paint.ANTI_ALIAS_FLAG);	}		//	이미지를 그릴 페인트 초기화
		if(mRectPaint == null) {	mRectPaint = new Paint(Paint.ANTI_ALIAS_FLAG);	}		//	터치 사각형을 그릴 페인트 초기화
		if(mOvalPaint == null) {	mOvalPaint = new Paint(Paint.ANTI_ALIAS_FLAG);	}		//	앵커를 그릴 페인트 초기화
		
		// 이미지 페인트 초기화 설정
		mSrcPaint.setFilterBitmap(true);
		mSrcPaint.setDither(true);
		
		// 터치 사각형 페인트 초기화 설정
		mRectPaint.setStyle(Style.STROKE);
		mRectPaint.setStrokeWidth(1);
		mRectPaint.setColor(getResources().getColor(R.color.light_gray));
		mRectPaint.setFilterBitmap(true);
		mRectPaint.setDither(true);
		
		// 앵커 객체 초기화 및 페인트 초기화 설정
		mOvalPaint.setStyle(Style.FILL);
		mOvalPaint.setColor(getResources().getColor(R.color.light_gray));
		mOvalPaint.setStrokeWidth(2);
		mOvalPaint.setAlpha(90);
		mOvalPaint.setFilterBitmap(true);
		mOvalPaint.setDither(true);
	}
	
	public void executeRecycle() {
		if(mOriginBitmap != null) {	
			mOriginBitmap.recycle();
			mOriginBitmap = null;
		}
		
		if(mOriginDegreeBitmap != null)	{
			mOriginDegreeBitmap.recycle();
			mOriginDegreeBitmap = null;
		}
		
		if(mSrcNoDegreeBitmap != null) {
			mSrcNoDegreeBitmap.recycle();
			mSrcNoDegreeBitmap = null;
		}
		
		if(mSrcDegreeBitmap != null) {
			mSrcDegreeBitmap.recycle();
			mSrcDegreeBitmap = null;
		}
	}
	

}











