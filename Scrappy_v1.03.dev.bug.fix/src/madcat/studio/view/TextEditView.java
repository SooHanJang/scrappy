package madcat.studio.view;

import madcat.studio.constants.Constants;
import madcat.studio.position.TextViewPos;
import madcat.studio.scrap.R;
import madcat.studio.scrap.ScrapPage;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

	
public class TextEditView extends EditText implements OnClickListener, OnFocusChangeListener {
	
	public static final int TEXT_STYLE_SIZE						=	3;
	
	public static final int TEXT_STYLE_NORMAL					=	0;
	public static final int TEXT_STYLE_BOLD						=	1;
	public static final int TEXT_STYLE_ITALIC					=	2;
	public static final int TEXT_STYLE_ALL						=	3;
	
	// 오리지널 변수
	private int mX, mY;
	private int mSize;
	private int mColor;
	private int mStyle											=	TEXT_STYLE_NORMAL;
	
	// 추가 사용 변수
	public static final int VIEW_ID								=	300;
	public static final int CORRECTION_SIZE						=	2;
	
	public static final int FONT_DEFAULT_SIZE					=	25;
	
	private Context mContext;
	private MessagePool mMessagePool;
	private TextViewPos mTextViewPos;
	private int mIndex;
	private boolean mSelectedFlag;
	
	private int mBasePtX, mBasePtY;
	
	private Bitmap mTrashBitmap;
	private float mTrashXPos, mTrashYPos;
	
	public TextEditView(Context context, TextViewPos textViewPos, int index) {
		// TODO Auto-generated constructor stub
		super(context);
		
		this.mContext = context;
		this.mMessagePool = (MessagePool)mContext.getApplicationContext();
		this.mTextViewPos = textViewPos;
		this.mIndex = index;
		this.setId(VIEW_ID + mIndex);
		
		
		setXY((int)mTextViewPos.getStartX(), (int)mTextViewPos.getStartY());
		setPadding(5, 5, 5, 5);
		setBackgroundColor(Color.TRANSPARENT);
		
		setText(mTextViewPos.getText());
		setTextColor(mTextViewPos.getColor());
		setTextSize(mTextViewPos.getFontSize());
		setTextStyle(mTextViewPos.getFontStyle());
		
		setOnClickListener(this);
		setOnFocusChangeListener(this);
		
		// 휴지통 좌표 설정
		mTrashBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.icon_trash_close);
		
		Point screenPoint = Utils.getScreenSize(mContext);
		mTrashXPos = (screenPoint.x - mTrashBitmap.getWidth()) / 2;
		mTrashYPos = 10;
		
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), mIndex + " 에서 onTouchEvent 발생");
		}
		
		if(mMessagePool.getMode() == Constants.MENU_MODE_EDIT
				|| mMessagePool.getMode() == Constants.MENU_MODE_TEXT) {
			
			// 선택된 텍스트뷰를 이동한다.
			
			int touchX = (int)event.getX();
			int touchY = (int)event.getY();
			
			switch(event.getAction()) {
			
				case MotionEvent.ACTION_DOWN:
					
					// 만약, 메뉴가 화면에 보여지고 있다면 메뉴 레이어를 Gone 한다.
					ScrapPage.setToolMenuVisibility(false);
					
					// 화면의 전체 외곽선을 해제합니다.
					TextEditLayout.clearAllOuterLine();
					
					// 현재 클릭된 EditView 의 외곽선을 그린다.
					drawBackground(true);
					
					// 만약, 스크랩한 이미지 모드가 EDIT_INIT 모드라면, INIT_DRAW 모드로 바꿔준다.
					if(ScrapPage.getRootLayout() != null) {
						ScrapViewLayout.scrapInitDrawMode();
					}
					
					mBasePtX = touchX;
					mBasePtY = touchY;
					
					
					break;
				
				case MotionEvent.ACTION_MOVE:
					
					if(mMessagePool.getMode() == Constants.MENU_MODE_TEXT) {
						TextEditLayout.clearAllFocus();
					}
					
					int gapX = mBasePtX - touchX;
					int gapY = mBasePtY - touchY;
					
					mX -= gapX;
					mY -= gapY;
					
					if(mX <= 0) {
						mX = 0;
					}
					
					if(mY <= 0) {
						mY = 0;
					}
					
					// 터치의 절대 좌표 값이 휴지통에 들어와있는 지 판단한 뒤, 들어와 있다면 이미지를 바꿔준다.
					if((touchX + mX) >= mTrashXPos && (touchX + mX) <= (mTrashXPos + mTrashBitmap.getWidth()) &&
							(touchY + mY) >= mTrashYPos && (touchY + mY) <= (mTrashYPos + mTrashBitmap.getHeight())) {
						
						ScrapPage.changeTrashIcon(true);
						
					} else {
						
						ScrapPage.changeTrashIcon(false);
						
					}
					
					setXY(mX, mY);
					invalidate();
					
					break;
					
				case MotionEvent.ACTION_UP:
					
					saveState();
					
					if((touchX + mX) >= mTrashXPos && (touchX + mX) <= (mTrashXPos + mTrashBitmap.getWidth()) &&
							(touchY + mY) >= mTrashYPos && (touchY + mY) <= (mTrashYPos + mTrashBitmap.getHeight())) {
						
						if(Constants.DEVELOPE_MODE) {
							Utils.logPrint(getClass(), mIndex + "를 쓰레기 통에 버림");
						}
						
						TextEditLayout.removeEditView(this, mIndex);
						
					} 
					
					if(mMessagePool.getMode() == Constants.MENU_MODE_TEXT) {
						this.performClick();
						
						return super.onTouchEvent(event);
					}
					
					
					break;
					
			} 
			
			return true;
		} 
		
		return super.onTouchEvent(event);
	}
	
	public void setXY(int x, int y) {
		mX = x;
		mY = y;
		
		setMargin();
		
	}

	private void setMargin() {
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params.setMargins(mX, mY, 0, 0);
		setLayoutParams(params);
	}
	
	@Override
	public void setTextSize(float size) {
		// TODO Auto-generated method stub
		super.setTextSize(size);
		this.mSize = (int)size;

		mTextViewPos.setFontSize(mSize);
	}
	
	@Override
	public void setTextColor(int color) {
		super.setTextColor(color);
		this.mColor = color;

		mTextViewPos.setColor(color);
	}

	public void setTextStyle(int style) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "변경할 텍스트 스타일 : " + style);
		}
		
		this.mStyle = style;
		this.mTextViewPos.setFontStyle(style);
		
		switch(style) {
			case TEXT_STYLE_NORMAL:
				Utils.logPrint(getClass(), "일반 스타일");
				setTypeface(Typeface.DEFAULT);
				break;
				
			
			case TEXT_STYLE_BOLD:
				Utils.logPrint(getClass(), "강조 스타일");
				setTypeface(getTypeface(), Typeface.BOLD);
				
				break;
				
			case TEXT_STYLE_ITALIC:
				Utils.logPrint(getClass(), "기울기 스타일");
				setTypeface(getTypeface(), Typeface.ITALIC);
				
				break;
				
			case TEXT_STYLE_ALL:
				Utils.logPrint(getClass(), "전부 적용 스타일");
				setTypeface(getTypeface(), Typeface.BOLD_ITALIC);
			
		}
		
	}
	
	private void saveState() {
		mTextViewPos.setText(getText().toString().trim());
		mTextViewPos.setStartX(mX);
		mTextViewPos.setStartY(mY);
		mTextViewPos.setColor(mColor);
		mTextViewPos.setFontSize(mSize);

		mMessagePool.getTextViewPosArray().set(mIndex, mTextViewPos);
	}
	
	public float    getX()			{ return mX; }
	public float    getY()			{ return mY; }
	public String getString()		{ return getText().toString(); }
	public float  getTextSize()		{ return mSize; }
	public int    getTextColor()	{ return mColor; }
	public int    getTextStyle()	{ return mStyle; }
	
	public void onClick(View v) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "onClick 발생");
		}
		
		if(mMessagePool.getMode() == Constants.MENU_MODE_TEXT) {
			TextEditLayout.clearExceptFocus(mIndex);
			TextEditLayout.setCurrentTextMode(TextEditLayout.TEXT_MODE_EDITING);
			
			setFocusableInTouchMode(true);
			
			saveState();
		}
	}
	
	public void onFocusChange(View v, boolean hasFocus) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), mIndex + " 에서 onFocusChange 발생(" + hasFocus + ")");
		}
		
		saveState();
		
		InputMethodManager imm = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
		
		if (hasFocus) {
			imm.showSoftInput(this, 0);
			String text = getText().toString();
			
			int textLength = text.length();
			
			if (textLength > 0) {
				setSelection(textLength);
			}
			
			setBackgroundResource(R.drawable.text_view_bg);
			
		} else {
			imm.hideSoftInputFromWindow(getWindowToken(), 0);
			setBackgroundColor(Color.TRANSPARENT);
		}
		
		mSelectedFlag = hasFocus;
		
		if(mMessagePool.getMode() == Constants.MENU_MODE_TEXT) {
			if(hasFocus) {
				ScrapPage.setTextViewColorFontState(mSize, mColor);
			}
		}
	}
	
	private void setUnderLineText(CharSequence text) {
		SpannableString content = new SpannableString(text);
		content.setSpan(new UnderlineSpan(), 0, text.length(), 0);
		setText(content, BufferType.SPANNABLE);
	}
	
	@Override
	public boolean onKeyPreIme(int keyCode, KeyEvent event) {
	// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
			this.setFocusable(false);
			TextEditLayout.setCurrentTextMode(TextEditLayout.TEXT_MODE_INIT);
		}
		
		return super.onKeyPreIme(keyCode, event);
	}
	
	public void setCurrentId(int index) {
		this.mIndex = index;
		this.setId(VIEW_ID + mIndex);
	}
	
	public void drawBackground(boolean flag) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "drawBackground 호출 (" + flag + ")");
		}
		
		if(flag) {
			
			this.setBackgroundColor(Color.TRANSPARENT);					//	투명 설정
			this.setBackgroundResource(R.drawable.text_view_bg);		//	외곽선 설정
			
			if(mMessagePool.getMode() == Constants.MENU_MODE_EDIT) {
				ScrapPage.setTrashMenuVisibility(true);
				ScrapPage.setLayerUpDownMenuVisibility(false);
			}
			
		} else {

			this.setBackgroundColor(Color.TRANSPARENT);
			
			if(mMessagePool.getMode() == Constants.MENU_MODE_EDIT) {
				ScrapPage.setTrashMenuVisibility(false);
				ScrapPage.setLayerUpDownMenuVisibility(false);
			}
			
		}
		
		invalidate();
		
	}
	
	public boolean getSelectedFlag() {
		return mSelectedFlag;
	}
}











