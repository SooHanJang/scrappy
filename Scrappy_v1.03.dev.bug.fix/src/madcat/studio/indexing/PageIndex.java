package madcat.studio.indexing;

public class PageIndex {
	
	private int mSBooksId;
	private int mPageNum;
	private int mPageThemeIndex;
	private String mPageInfo;
	
	public PageIndex(int sBooksId, int pageNum, String pageInfo, int pageThemeIdx) {
		this.mSBooksId = sBooksId;
		this.mPageNum = pageNum;
		this.mPageInfo = pageInfo;
		this.mPageThemeIndex = pageThemeIdx;
	}
	
	public int getSBookId()	{	return mSBooksId;	}
	public void setSBooksId(int sBooksId)	{	this.mSBooksId = sBooksId;	}
	
	public int getPageNum() {	return mPageNum;	}
	public void setPageNum(int pageNum) {	this.mPageNum = pageNum;	}
	
	public String getPageTitle() {	return mPageInfo;	}
	public void setPageInfo(String pageInfo) {	this.mPageInfo = pageInfo;	}
	
	public int getPageThemeIndex()	{	return mPageThemeIndex;	}
	public void setPageThemeIndex(int index)	{	this.mPageThemeIndex = index;	}

}
