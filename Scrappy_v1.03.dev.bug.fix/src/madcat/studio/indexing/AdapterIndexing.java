package madcat.studio.indexing;

import java.util.ArrayList;

import madcat.studio.scrap.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AdapterIndexing extends ArrayAdapter<PageIndex> {
	
	private Context mContext;
	
	private int mResId;
	private ArrayList<PageIndex> mItems;
	private LayoutInflater mInflater;
	
	public AdapterIndexing(Context context, int resId, ArrayList<PageIndex> items) {
		super(context, resId, items);
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null) {
			convertView = mInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			
			holder.pageTitle = (TextView)convertView.findViewById(R.id.scrap_index_page_row_title);
			holder.pageNum = (TextView)convertView.findViewById(R.id.scrap_index_page_row_num);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.pageTitle.setText(mItems.get(position).getPageTitle());
		holder.pageNum.setText((mItems.get(position).getPageNum() + 1) + mContext.getString(R.string.postfix_page));

		return convertView;
	}
	
	class ViewHolder {
		private TextView pageTitle;
		private TextView pageNum;
	}

}
