package madcat.studio.camera;

import java.io.IOException;
import java.util.List;

import madcat.studio.constants.Constants;
import madcat.studio.scrap.R;
import madcat.studio.scrap.ScrapCrop;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.AsyncTask;
import android.os.Build;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.widget.RelativeLayout;

public class CameraPreview extends SurfaceView implements Callback, Camera.PictureCallback {
	
	private SurfaceHolder holder;
	private Camera mCamera;
	
	private Context mContext;
	private MessagePool mMessagePool;
	
	public CameraPreview(Context context) {
		super(context);
		this.mContext = context;
		this.mMessagePool = (MessagePool)mContext.getApplicationContext();

		holder = this.getHolder();
		holder.addCallback(this);
		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

	}

	public CameraPreview(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.mContext = context;
		this.mMessagePool = (MessagePool)mContext.getApplicationContext();

		holder = this.getHolder();
		holder.addCallback(this);
		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

	}
	

	@Override
	public void surfaceCreated(SurfaceHolder holder) {

		if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.FROYO) {
			mCamera = Camera.open();
		} else {
			mCamera = Utils.getUsingCamera();
		}
		
		try {
			mCamera.setDisplayOrientation(90);
			mCamera.setPreviewDisplay(holder);
		} catch (IOException e) {
			mCamera.release();
			mCamera = null;
			e.printStackTrace();
		}
		
		
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,	int height) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "surfaceChanged 호출(width, height) : " + width + ", " + height);
		}
		
		Camera.Parameters parameters = mCamera.getParameters();

		List<Size> arSize = parameters.getSupportedPreviewSizes();
        if (arSize == null) {
        	parameters.setPreviewSize(width, height);
        } else {
             int diff = 10000;
             Size opti = null;
             for (Size s : arSize) {
               if (Math.abs(s.height - height) < diff) {
                    diff = Math.abs(s.height - height);
                    opti = s;
                   
               }
             }
             
             int previewWidth = opti.width;
             int previewHeight = opti.height;
             
             if(Constants.DEVELOPE_MODE) {
            	 Utils.logPrint(getClass(), "preview 사이즈 : " + previewWidth + ", " + previewHeight);
             }
             
             RelativeLayout.LayoutParams rp = new RelativeLayout.LayoutParams(previewHeight, previewWidth);
     		
             rp.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
     		 this.setLayoutParams(rp);
     		 this.invalidate();
     		 
     		 parameters.setPreviewSize(previewWidth, previewHeight);
        }
		

		mCamera.setParameters(parameters);
    	mCamera.startPreview();
	}
	
	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		if( null != mCamera){
			mCamera.setPreviewCallback(null);
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
	}
	
	@Override
	public void onPictureTaken(byte[] data, Camera camera) {
		PictureTakenAsync pictureTakenAysnc = new PictureTakenAsync(data);
		pictureTakenAysnc.execute();
		
	}
	
	public void capture(){
		mCamera.takePicture(null, null, this);
	}
	
	class PictureTakenAsync extends AsyncTask<Void, Void, Void> {
		
		ProgressDialog prgDig;
		byte[] data;
		
		public PictureTakenAsync(byte[] data) {
			this.data = data;
		}
		
		@Override
		protected void onPreExecute() {
			prgDig = ProgressDialog.show(mContext, "", mContext.getString(R.string.camera_picture_dig_msg));
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(prgDig.isShowing()) {
				prgDig.dismiss();
			}
			
			mMessagePool.setCameraBytes(data);
			
			Intent cropIntent = new Intent(mContext, ScrapCrop.class);
			cropIntent.putExtra(ScrapCrop.PUT_SCRAP_CROP_SELECT, ScrapCrop.LOAD_CAMERA);
			cropIntent.putExtra(ScrapCrop.PUT_SCRAP_CROP_CAMERA_WIDTH, mCamera.getParameters().getPreviewSize().width);
			cropIntent.putExtra(ScrapCrop.PUT_SCRAP_CROP_CAMERA_HEIGHT, mCamera.getParameters().getPreviewSize().height);
			mContext.startActivity(cropIntent);
			
			CameraPicture.CAMERA_PICUTRE_ACTIVITY.finish();
			
		}
	}
}











