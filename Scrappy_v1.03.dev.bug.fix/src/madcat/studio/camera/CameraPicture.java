package madcat.studio.camera;

import madcat.studio.scrap.R;
import madcat.studio.utils.MessagePool;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class CameraPicture extends Activity implements OnClickListener {
	
	public static Activity CAMERA_PICUTRE_ACTIVITY;

	private Context mContext;
	private MessagePool mMessagePool;

	private CameraPreview mPicturePreview;
	private ImageButton mBtnCapture;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scrap_camera_preview);
		
		this.mContext = this;
        this.mMessagePool = (MessagePool)getApplicationContext();
        this.CAMERA_PICUTRE_ACTIVITY = this;
        
        mPicturePreview = (CameraPreview)findViewById(R.id.scrap_camera_preview_picture_preview);
        mBtnCapture = (ImageButton)findViewById(R.id.scrap_camera_preview_btn_capture);
        
        mBtnCapture.setOnClickListener(this);
        
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.scrap_camera_preview_btn_capture:						//	���� ��� ��ư
				mPicturePreview.capture();
				mBtnCapture.setEnabled(false);
				mBtnCapture.setClickable(false);
				
				break;
		}
	}
}