package madcat.studio.database;


public class SBooks {
	
	private int mIndex;
	private String mInfo;
	private byte[] mScrapViewBytes, mTextViewBytes, mPenViewBytes;
	
	public SBooks(int index, String info, byte[] scrapViewBytes, byte[] textViewBytes, byte[] penViewBytes) {
		this.mIndex = index;
		this.mInfo = info;
		this.mScrapViewBytes = scrapViewBytes;
		this.mTextViewBytes = textViewBytes;
		this.mPenViewBytes = penViewBytes;
	}
	
	public int getIndex() {	return mIndex;	}
	public String getInfo() {	return mInfo;	}
	public byte[] getScrpViewBytes() {	return mScrapViewBytes;	}
	public byte[] getTextViewBytes() {	return mTextViewBytes;	}
	public byte[] getPenViewBytes() {	return mPenViewBytes;	}

}
