package madcat.studio.database;

import madcat.studio.constants.Constants;
import madcat.studio.utils.DateUtils;
import madcat.studio.utils.Utils;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SBooksDBHelper extends SQLiteOpenHelper {

	private final String SAMPLE_TABLE					=	"SAMPLE";

	public SBooksDBHelper(Context context) {
		super(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(
				"CREATE TABLE " + SAMPLE_TABLE + " (" +
				Constants.SCRAP_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
				Constants.SCRAP_COLUMN_INDEX + " INTEGER NOT NULL, " + 
				Constants.SCRAP_COLUMN_INFO + " TEXT, " + 
				Constants.SCRAP_COLUMN_SCRAP_VIEW_ARRAY + " BLOB, " +
				Constants.SCRAP_COLUMN_TEXT_VIEW_ARRAY + " BLOB, " +
				Constants.SCRAP_COLUMN_PEN_VIEW_FILE_PATH + " TEXT);"); 
		
		db.execSQL(
				"CREATE TABLE " + Constants.TABLE_HISTORY + " (" +
				Constants.HISTORY_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
				Constants.HISTORY_COLUMN_TITLE + " TEXT NOT NULL, " +
				Constants.HISTORY_COLUMN_PAGE + " INTEGER NOT NULL DEFAULT 0, " +
				Constants.HISTORY_COLUMN_DATE + " TEXT NOT NULL, " +
				Constants.HISTORY_COLUMN_COVER_THEME_IDX + " INTEGER NOT NULL DEFAULT 0, " +
				Constants.HISTORY_COLUMN_PAGE_THEME_IDX + " INTEGER NOT NULL DEFAULT 0);");

		ContentValues sampleValues = new ContentValues();
		sampleValues.put(Constants.HISTORY_COLUMN_TITLE, SAMPLE_TABLE);
		sampleValues.put(Constants.HISTORY_COLUMN_DATE, DateUtils.getCurrentDate(DateUtils.FORMAT_BOOK_DATE));
		
		db.insert(Constants.TABLE_HISTORY, null, sampleValues);

		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "Version mismatch : " + oldVersion + " to " + newVersion);
		}
		
		// 현재 데이터베이스에 담긴 정보를 다른 곳에 백업한 뒤, 데이터베이스 수정 후 복구한다.
	}
	
	

}
