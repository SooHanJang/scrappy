package madcat.studio.scrapdata;

public class SaveScrapData {
	
	private int mIndex;
	private String mInfo, mPViewFilePath;
	private byte[] mSViewByteArray;
	private byte[] mTViewByteArray;
	
	public SaveScrapData(int index, String info, byte[] sViewByteArray, byte[] tViewByteArray, String pViewFilePath) {
		this.mIndex = index;
		this.mInfo = info;
		this.mSViewByteArray = sViewByteArray;
		this.mTViewByteArray = tViewByteArray;
		this.mPViewFilePath = pViewFilePath;
	}
	
	public int getIndex() {	return mIndex;	}
	public void setIndex(int index) {	this.mIndex = index;	}
	
	public String getInfo() {	return mInfo;	}
	public void setInfo(String info) {	this.mInfo = info;	}
	
	public byte[] getSViewByteArray() {	return mSViewByteArray;	}
	public void setScrapView(byte[] scrapView) {	this.mSViewByteArray = scrapView;	}
	
	public byte[] getTViewByteArray() {	return mTViewByteArray;	}
	public void setTextView(byte[] textView) {	this.mTViewByteArray = textView;	}
	
	public String getPViewFilePath() {	return mPViewFilePath;	}
	public void setPViewFilePath(String pViewFilePath) {	this.mPViewFilePath = pViewFilePath;	}

}
