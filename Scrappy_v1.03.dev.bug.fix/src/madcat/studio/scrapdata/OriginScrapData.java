package madcat.studio.scrapdata;

import java.util.ArrayList;

import madcat.studio.position.CropViewPos;
import madcat.studio.position.TextViewPos;

public class OriginScrapData {
	
	private int mIndex;
	private String mInfo;
	private ArrayList<CropViewPos> mCropViewPosArray;
	private ArrayList<TextViewPos> mTextViewPosArray;
	private String mPViewFilePath;
	
	public OriginScrapData(int index, String info, ArrayList<CropViewPos> cropViewPosArray, 
			ArrayList<TextViewPos> textViewPosArray, String pViewFilePath) {
		this.mIndex = index;
		this.mInfo = info;
		this.mCropViewPosArray = cropViewPosArray;
		this.mTextViewPosArray = textViewPosArray;
		this.mPViewFilePath = pViewFilePath;
	}
	
	public int getIndex() {	return mIndex;	}
	public void setIndex(int index) {	this.mIndex = index;	}
	
	public String getInfo() {	return mInfo;	}
	public void setInfo(String info) {	this.mInfo = info;	}
	
	public ArrayList<CropViewPos> getCropViewPosArray() {	return mCropViewPosArray;	}
	public void setCropViewPosArray(ArrayList<CropViewPos> cropViewPosArray) {	this.mCropViewPosArray = cropViewPosArray;	}
	
	public ArrayList<TextViewPos> getTextViewPosArray() {	return mTextViewPosArray;	}
	public void setTextViewPosArray(ArrayList<TextViewPos> textViewPosArray) {	this.mTextViewPosArray = textViewPosArray;	}
	
	public String getPViewFilePath() {	return mPViewFilePath;	}
	public void setPViewFilePath(String pViewFilePath) {	this.mPViewFilePath = pViewFilePath;	}

}
