package madcat.studio.share;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import madcat.studio.scrap.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.net.Uri;
import android.provider.MediaStore.Images;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;

public class ShareImageAdapter extends ArrayAdapter<Uri> {
	
	private Context mContext;
	private int mResId;
	private ArrayList<Uri> mItems;
	
	private LayoutInflater mInflater;
	
	private int mRowWidth, mRowHeight;
	
	private boolean[] isCheckedConfirm;
	
	public ShareImageAdapter(Context context, int resId, ArrayList<Uri> items, int rowWidth, int rowHeight) {
		super(context, resId, items);
		
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		
		this.mRowWidth = rowWidth;
		this.mRowHeight = rowHeight;
		
		this.mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		this.isCheckedConfirm = new boolean[mItems.size()];

	}
	
	public void setAllCheckedItems(boolean isChecked) {
		int tempSize = isCheckedConfirm.length;
		
		for(int i=0; i < tempSize; i++) {
			isCheckedConfirm[i] = isChecked;
		}
		
		notifyDataSetInvalidated();
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		
		if(convertView == null) {
			convertView = mInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			
			holder.mIvImage = (ImageView)convertView.findViewById(R.id.share_image_row_img);
			holder.mCbCheck = (CheckBox)convertView.findViewById(R.id.share_image_check_box);

			convertView.setTag(holder);
		
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		try {
			Bitmap bm = Images.Media.getBitmap(mContext.getContentResolver(), mItems.get(position)).copy(Config.RGB_565, true);
			holder.mIvImage.setImageBitmap(bm);
			
			ViewGroup.LayoutParams viewParams = holder.mIvImage.getLayoutParams();
			
			viewParams.width = mRowWidth;
			viewParams.height = mRowHeight;
			
			holder.mIvImage.setLayoutParams(viewParams);
			
			holder.mIvImage.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					holder.mCbCheck.setChecked(!holder.mCbCheck.isChecked());
				}
			});
			
			holder.mCbCheck.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if(isChecked) {
						isCheckedConfirm[position] = true;
					} else {
						isCheckedConfirm[position] = false;
					}
				}
			});
			
			if(isCheckedConfirm[position]) {
				holder.mCbCheck.setChecked(true);
			} else {
				holder.mCbCheck.setChecked(false);
			}
			

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return convertView;
	}
	
	public ArrayList<Uri> getCheckedList() {
		ArrayList<Uri> results = new ArrayList<Uri>();
		
		for(int i=0; i < isCheckedConfirm.length; i++) {
			if(isCheckedConfirm[i]) {
				results.add(mItems.get(i));
			}
		}
		
		return results;
	}
	
	class ViewHolder {
		private ImageView mIvImage;
		private CheckBox mCbCheck;
	}

}













