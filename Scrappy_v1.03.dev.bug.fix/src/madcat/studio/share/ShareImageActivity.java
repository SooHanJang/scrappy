package madcat.studio.share;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.scrap.R;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Toast;

public class ShareImageActivity extends Activity implements OnClickListener {
	
	public static final String PUT_SHARE_SBOOK_ID				=	"PUT_SHARE_SBOOK_ID";

	private Context mContext;
	
	private GridView mGridView;
	private ImageButton mBtnAllCheck, mBtnShare;
	
	private ShareImageAdapter mAdapter;
	private ArrayList<Uri> mItems;
	
	private int mSbookId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.share_image_layout);
		
		this.mContext = this;
		this.getIntenter();
		
		mGridView = (GridView)findViewById(R.id.share_image_grid_view);
		mBtnAllCheck = (ImageButton)findViewById(R.id.share_image_btn_select);
		mBtnShare = (ImageButton)findViewById(R.id.share_image_btn_share);
		
		mBtnAllCheck.setOnClickListener(this);
		mBtnShare.setOnClickListener(this);
		
		mItems = Utils.getSBookSnapShot(mContext, mSbookId);
		
		// 공유할 페이지가 존재할 경우,
		if(!mItems.isEmpty()) {
		
			mGridView.post(new Runnable() {
				@Override
				public void run() {
					if(Constants.DEVELOPE_MODE) {
						Utils.logPrint(getClass(), "gridView 크기 : " + mGridView.getWidth() + ", " + mGridView.getHeight());
					}
					
					int rowWidth = (int) ((float)mGridView.getWidth() / 2);
					int rowHeight = (int) ((float)mGridView.getHeight() / 2);
					
					mAdapter = new ShareImageAdapter(mContext, R.layout.share_image_rows, mItems, rowWidth, rowHeight);
					
					mGridView.setAdapter(mAdapter);
				}
			});
		} 
		// 공유할 페이지가 존재하지 않을 경우,
		else {
			
			Toast.makeText(mContext, getString(R.string.share_scrap_no_item), Toast.LENGTH_SHORT).show();
			finish();
			
		}
	}
	
	private void getIntenter() {
		Intent intent = getIntent();
		
		if(intent != null) {
			mSbookId = intent.getExtras().getInt(PUT_SHARE_SBOOK_ID);
		}
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.share_image_btn_select:
				
				mAdapter.setAllCheckedItems(true);
					
				break;
				
			case R.id.share_image_btn_share:
				
				if(!mAdapter.getCheckedList().isEmpty()) {
					Intent shareIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
					
					shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, mAdapter.getCheckedList());
					shareIntent.setType("image/png");
					
					startActivity(Intent.createChooser(shareIntent, getString(R.string.share_scrap_title)));
				} else {
					Toast.makeText(mContext, getString(R.string.share_scrap_page), Toast.LENGTH_SHORT).show();
				}
				
				break;
		}
	}
}








