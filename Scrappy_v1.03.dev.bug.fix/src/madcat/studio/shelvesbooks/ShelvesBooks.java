package madcat.studio.shelvesbooks;

import android.os.Parcel;
import android.os.Parcelable;

public class ShelvesBooks implements Parcelable {
	
	public static final float RATIO_WIDTH						=	9;
	public static final float RATIO_HEIGHT						=	11;		
	
	private String mTitle, mDate;
	private int mIndex;
	private int mCoverThemeIndex, mPageThemeIndex;
	
	public ShelvesBooks() {}
	
	public ShelvesBooks(Parcel in) {
		readFromParcel(in);
	}
	
	public ShelvesBooks(String title, int index, String date, int coverIdx, int pageIdx) {
		this.mTitle = title;
		this.mIndex = index;
		this.mDate = date; 
		this.mCoverThemeIndex = coverIdx;
		this.mPageThemeIndex = pageIdx;
	}

	public String getTitle() {	return mTitle;	}
	public void setTitle(String title) {	this.mTitle = title;	}

	public String getDate() {	return mDate;	}
	public void setDate(String date) {	this.mDate = date;	}
	
	public int getTotlaPageIndex() {	return mIndex;	}
	public void setTotalPageIndex(int index) {	this.mIndex = index;	}
	
	public int getCoverThemeIndex() {	return mCoverThemeIndex;	}
	public void setCoverThemeIndex(int idx) {	this.mCoverThemeIndex = idx;	}

	public int getPageThemeIndex() {	return mPageThemeIndex;	}
	public void setPageThemeIndex(int idx) {	this.mPageThemeIndex = idx;	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mTitle);
		dest.writeInt(mIndex);
		dest.writeString(mDate);
		dest.writeInt(mCoverThemeIndex);
		dest.writeInt(mPageThemeIndex);
	}
	
	private void readFromParcel(Parcel in) {
		mTitle = in.readString();
		mIndex = in.readInt();
		mDate = in.readString();
		mCoverThemeIndex = in.readInt();
		mPageThemeIndex = in.readInt();
	}

	public static final Parcelable.Creator<ShelvesBooks> CREATOR = new Parcelable.Creator<ShelvesBooks>() {
		
		@Override
		public ShelvesBooks createFromParcel(Parcel source) {
			ShelvesBooks books = new ShelvesBooks();
			books.mTitle = source.readString();
			books.mIndex = source.readInt();
			books.mDate = source.readString();
			books.mCoverThemeIndex = source.readInt();
			books.mPageThemeIndex = source.readInt();
			
			return books;
		}

		@Override
		public ShelvesBooks[] newArray(int size) {
			return new ShelvesBooks[size];
		}
	};

}














