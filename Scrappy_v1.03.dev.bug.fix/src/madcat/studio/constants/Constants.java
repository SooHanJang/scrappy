package madcat.studio.constants;

import android.graphics.Bitmap;

public class Constants {
	
	public static final boolean DEVELOPE_MODE						=	true;
	
	// 환경 설정 (SharedPreference) 관련 변수
	public static final String CONFIG_PREF_NAME						=	"SBOOKS_CONFIG_PREF";
	public static final String CONFIG_APP_FIRST						=	"SBOOKS_CONFIG_APP_FIRST";
	public static final String CONFIG_PEN_STYLE						=	"SBOOKS_CONFIG_PEN_STYLE";
	public static final String CONFIG_TEXT_STYLE					=	"SBOOKS_CONFIG_TEXT_STYLE";
	
	// Menu 모드 관련 변수
	public static final int MENU_MODE_VIEW							=	0;
	public static final int MENU_MODE_PEN							=	1;
	public static final int MENU_MODE_TEXT							=	2;
	public static final int MENU_MODE_EDIT							=	3;
	
	// 파일 저장 관련 변수
	public static final String SCRAP_DIR_ROOT						=	"/MadCatStudio/SBooks";
	
	public static final String SEPERATOR_PREVIEW_RATIO				=	":";
	
	public static final int SAVE_FILE_PATH_SCRAP					=	0;
	public static final int SAVE_FILE_PATH_PVIEW					=	1;
	public static final int SAVE_FILE_PATH_SNAP_VIEW				=	2;

	public static final String SAVE_IMAGE_PATH						=	"/MadCatStudio/SBooks/Images";
	public static final String SAVE_IMAGE_PVIEW_PATH				=	"/MadCatStudio/SBooks/Images/PViews";
	public static final String SAVE_IMAGE_SNAP_VIEW_PATH			=	"/MadCatStudio/SBooks/Images/SNapViews";
	
	public static final String SAVE_IMAGE_PREFIX					=	"SBooks_";
	public static final String SAVE_IMAGE_PVIEW_PREFIX				=	"PViews_";
	public static final String SAVE_IMAGE_SNAP_VIEW_PREFIX			=	"SNAP_Views_";
	public static final String SAVE_IMAGE_POSTFIX					=	".png";
	
	// 데이터 베이스 관련 변수
	public static final String DATABASE_NAME						=	"SBooks.db";
	public static final int DATABASE_VERSION						=	1;
	public static final String DATABASE_ROOT_DIR					=	"/data/data/madcat.studio.scrap/databases/";
	
	// 데이터 베이스 테이블 관련 변수
	public static final String SCRAP_COLUMN_ID						=	"_id";
	public static final String SCRAP_COLUMN_INDEX					=	"_index";
	public static final String SCRAP_COLUMN_INFO					=	"_info";
	public static final String SCRAP_COLUMN_SCRAP_VIEW_ARRAY		=	"_scrap_view_array";
	public static final String SCRAP_COLUMN_TEXT_VIEW_ARRAY			=	"_text_view_array";
	public static final String SCRAP_COLUMN_PEN_VIEW_FILE_PATH		=	"_pen_view_file_path";
	public static final String SCRAP_COLUMN_SNAP_VIEW_FILE_PATH		=	"_snap_view_file_path";
	
	public static final String TABLE_HISTORY						=	"HISTORY";
	
	public static final String HISTORY_COLUMN_ID					=	"_id";
	public static final String HISTORY_COLUMN_TITLE					=	"_title";
	public static final String HISTORY_COLUMN_PAGE					=	"_page";
	public static final String HISTORY_COLUMN_DATE					=	"_date";
	public static final String HISTORY_COLUMN_COVER_THEME_IDX		=	"_cover_idx";
	public static final String HISTORY_COLUMN_PAGE_THEME_IDX		=	"_page_idx";
	
	// 스크랩 페이지 관련 변수
	public static final String PAGE_POSTFIX							=	" Page";
	
	// 이미지 관련 변수
	public static final Bitmap.Config IMAGE_QUALITY_TYPE			=	Bitmap.Config.ARGB_8888;
	
	// 테마 설정 관련 변수
	public static final String THEME_COVER_PREFIX					=	"cover_theme_";
	public static final String THEME_PAGE_PREFIX					=	"theme_pattern_";
	
	// 데코레이션 관련 변수
	public static final int DECO_PAPER_ITEM_SIZE					=	19;
	public static final int DECO_STICKER_ITEM_SIZE					=	92;
	public static final int DECO_TAPE_ITEM_SIZE						=	16;
	
}












