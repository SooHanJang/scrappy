package madcat.studio.position;

import java.io.Serializable;

public abstract class ViewPos implements PositionInterface, Serializable {
	
	private static final long serialVersionUID					=	1L;
	
	private float mStartX, mStartY;

	public ViewPos(float startX, float startY) {
		this.mStartX = startX;
		this.mStartY = startY;
	}

	@Override
	public float getStartX() {	return mStartX;	}
	public float getStartY() {	return mStartY;	}

	@Override
	public void setStartX(float startX) {	this.mStartX = startX;	}
	public void setStartY(float startY) {	this.mStartY = startY;	}
	public void setStartPos(float startX, float startY) {
		this.mStartX = startX;
		this.mStartY = startY;
	}
	

}
