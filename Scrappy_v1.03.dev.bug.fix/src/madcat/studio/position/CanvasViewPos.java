package madcat.studio.position;


public abstract class CanvasViewPos extends ViewPos {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int mColor;
	
	public CanvasViewPos(float startX, float startY) {
		super(startX, startY);
	}
	
	public int getColor() {	return mColor;	}
	public void setColor(int color) {	this.mColor = color;	}

}
