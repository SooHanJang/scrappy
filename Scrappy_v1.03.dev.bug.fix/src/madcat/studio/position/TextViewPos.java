package madcat.studio.position;

import madcat.studio.view.TextEditView;

public class TextViewPos extends CanvasViewPos {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mText;
	private int mFontSize											=	40;
	private int mFontStyle											=	TextEditView.TEXT_STYLE_NORMAL;
	
	public TextViewPos(float startX, float startY) {
		super(startX, startY);
	}
	
	public void setText(String text) {	this.mText = text;	}
	public String getText() {	return mText;	}
	
	public int getFontSize() {	return mFontSize;	}
	public void setFontSize(int fontSize) {	this.mFontSize = fontSize;	}
	
	public int getFontStyle() {	return mFontStyle;	}
	public void setFontStyle(int fontStyle)	{	this.mFontStyle = fontStyle;	}
	

}
