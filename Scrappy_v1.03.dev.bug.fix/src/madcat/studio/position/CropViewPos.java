package madcat.studio.position;



public class CropViewPos extends ViewPos {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String mFilePath;
	
	private float mNoDegreeWidth, mNoDegreeHeight;
	private float mNoDegreeStartX, mNoDegreeStartY;
	
	private float mDegreeWidth, mDegreeHeight;
	private float mDegree;


	public CropViewPos(String filePath, float startX, float startY, float degree) {
		super(startX, startY);
		this.mNoDegreeStartX = startX;
		this.mNoDegreeStartY = startY;
		this.mFilePath = filePath;
		this.mDegree = degree;
	}
	
	
	public void setFilePath(String filePath) {	this.mFilePath = filePath;	}
	public void setNoDegreeStartPos(float startX, float startY) {
		this.mNoDegreeStartX = startX;
		this.mNoDegreeStartY = startY;
	}
	
	public void setNoDegreeResize(float width, float height)	{	
		this.mNoDegreeWidth = width;
		this.mNoDegreeHeight = height;
	}
	public void setDegreeResize(float width, float height)	{
		this.mDegreeWidth = width;
		this.mDegreeHeight = height;
	}
	
	public void setDegree(float degree) {	this.mDegree = degree;	}
	
	public String getFilePath() {	return mFilePath;	}
	public float getNoDegreeWidth()	{	return mNoDegreeWidth;	}
	public float getNoDegreeHeight()	{	return mNoDegreeHeight;	}
	public float getDegreeWidth()	{	return mDegreeWidth;	}
	public float getDegreeHeight()	{	return mDegreeHeight;	}
	public float getNoDegreeStartX()	{	return mNoDegreeStartX;	}
	public float getNoDegreeStartY()	{	return mNoDegreeStartY;	}
	public float getDegree() {	return mDegree;	}

	

}
