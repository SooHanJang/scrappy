package madcat.studio.position;

import android.graphics.Color;

public class PenViewPos extends CanvasViewPos {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private float mDestX, mDestY;
	
	private int mColor;
	
	public PenViewPos(float startX, float startY, float destX, float destY) {
		super(startX, startY);
		
		this.mDestX = destX;
		this.mDestY = destY;
		
		this.mColor = Color.BLACK;
	}
	
	public float getDestX() {	return mDestX;	}
	public float getDestY() {	return mDestY;	}
	public int getColor() {	return mColor;	}
	
	public void setDestX(float destX) {	this.mDestX = destX;	}
	public void setDestY(float destY) {	this.mDestY = destY;	}
	public void setColor(int color)	{ this.mColor = color;	} 
	
	public void setDestPos(float destX, float destY) {
		this.mDestX = destX;
		this.mDestY = destY;
	}
	
	

}
