package madcat.studio.position;

interface PositionInterface {
	
	public float getStartX();
	public float getStartY();
	
	public void setStartX(float startX);
	public void setStartY(float startY);
	public void setStartPos(float startX, float startY);
	
	

}
