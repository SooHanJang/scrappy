package madcat.studio.dialog;

import madcat.studio.constants.Constants;
import madcat.studio.scrap.R;
import madcat.studio.scrap.ScrapPage;
import madcat.studio.shelvesbooks.ShelvesBooks;
import madcat.studio.utils.DateUtils;
import madcat.studio.utils.Utils;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DialogAddSBooks extends Dialog implements android.view.View.OnClickListener {
	
	public static final int MODE_ADD						=	0;
	public static final int MODE_MODIFY						=	1;
	
	public static final int CHANGE_TITLE					=	0;
	public static final int CHANGE_THEME					=	1;
	public static final int CHANGE_NO						=	2;
	
	public static final int COVER_THEME_SIZE				=	10;
	
	private Context mContext;
	private RelativeLayout mRlCoverMain, mRlCoverLeft, mRlCoverRight;
	private ImageButton mBtnSelectLeft, mBtnSelectRight, mBtnOk, mBtnCancel;
	private EditText mEditTitle;
	private TextView mTvDate, mTvPage;
	
	private int mMode										=	MODE_ADD;
	private int mCoverIndex;
	
	// 수정 모드에서 사용되는 전역 변수
	private ShelvesBooks mItem;
	
	public DialogAddSBooks(Context context, int mode) {
		super(context);
		this.mContext = context;
		this.mMode = mode;
	}
	
	public DialogAddSBooks(Context context, int mode, ShelvesBooks item) {
		super(context);
		this.mContext = context;
		this.mMode = mode;
		this.mItem = item;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		setContentView(R.layout.dig_add_sbooks);
		
		// Resourece 초기화
		mRlCoverMain = (RelativeLayout)findViewById(R.id.dig_add_sbooks_cover_main);
		mRlCoverLeft = (RelativeLayout)findViewById(R.id.dig_add_sbooks_cover_left);
		mRlCoverRight = (RelativeLayout)findViewById(R.id.dig_add_sbooks_cover_right);
		
		mEditTitle = (EditText)findViewById(R.id.dig_add_sbooks_cover_title);
		mTvDate = (TextView)findViewById(R.id.dig_add_sbooks_cover_date);
		mTvPage = (TextView)findViewById(R.id.dig_add_sbooks_cover_page);
		
		mBtnSelectLeft = (ImageButton)findViewById(R.id.dig_add_sbooks_cover_select_left);
		mBtnSelectRight = (ImageButton)findViewById(R.id.dig_add_sbooks_cover_select_right);
		
		mBtnOk = (ImageButton)findViewById(R.id.dig_add_sbooks_cover_ok);
		mBtnCancel = (ImageButton)findViewById(R.id.dig_add_sbooks_cover_cancel);
		
		// Event Listener 설정
		mRlCoverLeft.setOnClickListener(this);
		mRlCoverRight.setOnClickListener(this);
		
		mBtnSelectLeft.setOnClickListener(this);
		mBtnSelectRight.setOnClickListener(this);
		
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		
		// 스크랩 북 수정 모드일 경우,
		if(mMode == DialogAddSBooks.MODE_MODIFY) {
			if(mItem != null) {
				mCoverIndex = mItem.getCoverThemeIndex();
				mEditTitle.setText(mItem.getTitle());
				mTvDate.setText(mItem.getDate());
				mTvPage.setText("- " + mItem.getTotlaPageIndex() + " -");
				
				setCoverThemeView(mCoverIndex);
			}
		} 
		// 스크랩 북 생성 모드일 경우,
		else {
			mTvPage.setText("- 0 -");
			mTvDate.setText(DateUtils.getCurrentDate(DateUtils.FORMAT_BOOK_DATE));
		}
		
		// 사용법 토스트 출력
		Toast.makeText(mContext, mContext.getString(R.string.dig_create_sbooks_explain_toast), Toast.LENGTH_SHORT).show();
	}
	
	private void setCoverThemeView(int currIdx) {
		
		int prevIdx = currIdx - 1;
		int nextIdx = currIdx + 1;
		
		if(currIdx == 0) {
			prevIdx = DialogAddSBooks.COVER_THEME_SIZE-1;
		} else if(currIdx == (DialogAddSBooks.COVER_THEME_SIZE-1)) {
			nextIdx = 0;
		}
		
		int prevCoverId = mContext.getResources().getIdentifier(Constants.THEME_COVER_PREFIX + prevIdx, "drawable", mContext.getPackageName());
		int mainCoverId = mContext.getResources().getIdentifier(Constants.THEME_COVER_PREFIX + currIdx, "drawable", mContext.getPackageName());
		int nextCoverId = mContext.getResources().getIdentifier(Constants.THEME_COVER_PREFIX + nextIdx, "drawable", mContext.getPackageName());
		
		mRlCoverLeft.setBackgroundResource(prevCoverId);
		mRlCoverMain.setBackgroundResource(mainCoverId);
		mRlCoverRight.setBackgroundResource(nextCoverId);
		
	}
	
	// 왼쪽 커버를 메인으로 변경 (Index--)
	private void setLeftCover() {
		mCoverIndex--;
		
		if(mCoverIndex == -1) {
			mCoverIndex = DialogAddSBooks.COVER_THEME_SIZE-1;
		}
		
		setCoverThemeView(mCoverIndex);
	}
	
	// 오른쪽 커버를 메인으로 변경 (Index++)
	private void setRightCover() {
		mCoverIndex++;
		
		if(mCoverIndex == DialogAddSBooks.COVER_THEME_SIZE) {
			mCoverIndex = 0;
		}
	  
		setCoverThemeView(mCoverIndex);
	}
	
	@Override
	public void onClick(View v) {
		int id = v.getId();
		
		// 왼쪽 선택 버튼 클릭,
		if(id == mBtnSelectLeft.getId()) {
			setLeftCover();
		}
		// 오른쪽 선택 버튼 클릭,
		else if(id == mBtnSelectRight.getId()) {
			setRightCover();
		}
		// 왼쪽 책 커버 클릭,
		else if(id == mRlCoverLeft.getId()) {
			setLeftCover();
		}
		// 오른쪽 책 커버 클릭,
		else if(id == mRlCoverRight.getId()) {
			setRightCover();
		}
		
		// 확인 버튼 클릭,
		else if(id == mBtnOk.getId()) {
			
			String changeTitle = mEditTitle.getText().toString().trim();
			
			int changeMode = CHANGE_TITLE;
			
			// 스크랩 북 커버를 수정 중,
			if(mMode == MODE_MODIFY) {
				// 타이틀 제목이 바뀜
				if(!changeTitle.equals(mItem.getTitle())) {
					changeMode = CHANGE_TITLE;
				} 
				// 타이틀 제목은 그대로, 커버나 페이지 테마만 변경
				else if(changeTitle.equals(mItem.getTitle()) && mCoverIndex != mItem.getCoverThemeIndex()) {
					changeMode = CHANGE_THEME;
				} 
				// 아무것도 변하지 않음
				else {
					changeMode = CHANGE_NO;
				}
				
				switch(changeMode) {
					case CHANGE_TITLE:
					case CHANGE_THEME:
						
						if(Utils.isTitleCheckInvalid(changeTitle)) {
							SBooksEditAysnc shelvesBooksModifyAsync = new SBooksEditAysnc(
									mContext, mMode, changeMode, mItem.getTitle(), changeTitle, mCoverIndex);
							shelvesBooksModifyAsync.execute();
						} else {
							Toast.makeText(mContext, mContext.getString(R.string.dig_create_sbooks_invalid), Toast.LENGTH_SHORT).show();
						}
						
						break;
					case CHANGE_NO:
						dismiss();
						break;
				}
				
			} 
			// 스크랩 북 커버를 새로 추가 중,
			else {
				
				if(Utils.isTitleCheckInvalid(changeTitle)) {
					SBooksEditAysnc shelvesBooksAddAsync = 
						new SBooksEditAysnc(mContext, mMode, changeTitle, mCoverIndex);
					shelvesBooksAddAsync.execute();
				} else {
					Toast.makeText(mContext, mContext.getString(R.string.dig_create_sbooks_invalid), Toast.LENGTH_SHORT).show();
				}
			}
			
			
		} 
		// 취소 버튼 클릭,
		else if(id == mBtnCancel.getId()) {
			this.dismiss();
		}
	}
	
	class SBooksEditAysnc extends AsyncTask<Void, Void, Void> {
		
		Context context;
		ProgressDialog proDig;
		
		int bookId;
		int mode;
		int changeMode;
		int coverIdx;
		String preTitle, changeTitle;
		int executeFlag;
		
		public SBooksEditAysnc(Context context, int mode, String changeTitle, int coverIdx) {
			this.context = context;
			this.mode = mode;
			this.changeTitle = changeTitle;
			this.coverIdx = coverIdx;
		}
		
		public SBooksEditAysnc(Context context, int mode, int changeMode, String preTitle, String changeTitle, int coverIdx) {
			this.context = context;
			this.mode = mode;
			this.changeMode = changeMode;
			this.preTitle = preTitle;
			this.changeTitle = changeTitle;
			this.coverIdx = coverIdx;
		}
		
		@Override
		protected void onPreExecute() {
			proDig = ProgressDialog.show(mContext, "", mContext.getString(R.string.dig_create_sbooks_msg));
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			switch(mode) {
				case MODE_ADD:
					executeFlag = Utils.createSBooks(context, changeTitle, mTvDate.getText().toString(), coverIdx, 0);
					
					break;
					
				case MODE_MODIFY:
					executeFlag = Utils.updateSBooks(context, changeMode, preTitle, changeTitle, coverIdx, 0);
					break;
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(proDig.isShowing()) {
				proDig.dismiss();
			}
			
			if(executeFlag == -1) {
				Toast.makeText(mContext, mContext.getString(R.string.dig_create_sbooks_failed), Toast.LENGTH_SHORT).show();
			} else {
				if(mMode == MODE_ADD) {
					bookId = executeFlag;
					
					Intent scrapPageIntent = new Intent(mContext, ScrapPage.class);
					scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_ID, bookId);
					scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_TITLE, changeTitle);
					scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_PAGE_INDEX, 0);
					scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_PAGE_THEME_INDEX, 0);
					mContext.startActivity(scrapPageIntent);
				}
			}
			
			dismiss();
		}
	}

}













