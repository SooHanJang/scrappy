package madcat.studio.dialog;

import madcat.studio.scrap.R;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class DialogTutorial extends Dialog implements OnClickListener {
	
	public static final int TUTORIAL_PAGE_FIRST			=	0;
	public static final int TUTORIAL_PAGE_SECOND		=	1;
	public static final int TUTORIAL_PAGE_THIRD			=	2;
	
	public static final int MAX_PAGE_SIZE				=	3;
	
	public static final int DIALOG_IMAGE_RATIO_WIDTH	=	2;
	public static final int DIALOG_IMAGE_RATIO_HEIGHT	=	3;

	private Context mContext;
	private int mShowPage								=	TUTORIAL_PAGE_FIRST;
	
	private ImageView mIvPage;
	private ImageButton mBtnPrev, mBtnNext;
	
	public DialogTutorial(Context context, int showPage) {
		super(context);
		this.mContext = context;
		this.mShowPage = showPage;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		
		setContentView(R.layout.scrap_tutorial);

		// Resource Id 초기화
		mIvPage = (ImageView)findViewById(R.id.scrap_tutorial_page);
		mBtnPrev = (ImageButton)findViewById(R.id.scrap_tutorial_prev);
		mBtnNext = (ImageButton)findViewById(R.id.scrap_tutorial_next);

		mIvPage.setOnClickListener(this);
		mBtnPrev.setOnClickListener(this);
		mBtnNext.setOnClickListener(this);
		
		// 초기 페이지 설정
		setTutorialPage(mShowPage);
		
		// 도움말 관련 안내 토스트
		Toast.makeText(mContext, mContext.getString(R.string.tutorial_dig_explain), Toast.LENGTH_LONG).show();
		
	}
	
	private void setTutorialPage(int pageIdx) {
		int bgId = mContext.getResources().getIdentifier("tutorial_page_" + pageIdx, "drawable", mContext.getPackageName());
		
		if(mIvPage != null) {
			mIvPage.setBackgroundResource(bgId);
		}
	}
	
	@Override
	public void onClick(View v) {
		
		int id = v.getId();
		
		// 도움말 화면을 클릭 했을 경우,
		if(id == mIvPage.getId()) {
			
			mShowPage++;
			
			if(mShowPage >= MAX_PAGE_SIZE) {
				mShowPage = 0;
			}
			
			setTutorialPage(mShowPage);
			
		}
		// 이전 버튼을 클릭 했을 경우,
		else if(id == mBtnPrev.getId()) {
			
			mShowPage--;
			
			if(mShowPage < 0) {
				mShowPage = MAX_PAGE_SIZE - 1;
			}
			
			setTutorialPage(mShowPage);
			
			
		}
		// 다음 버튼을 클릭 했을 경우,
		else if(id == mBtnNext.getId()) {
			
			mShowPage++;
			
			if(mShowPage >= MAX_PAGE_SIZE) {
				mShowPage = 0;
			}
			
			setTutorialPage(mShowPage);
			
		}
	}

}

















