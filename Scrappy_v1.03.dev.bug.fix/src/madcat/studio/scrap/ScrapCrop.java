package madcat.studio.scrap;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import madcat.studio.constants.Constants;
import madcat.studio.utils.ImageUtils;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import madcat.studio.view.CropView;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class ScrapCrop extends Activity {

	public static final String PUT_SCRAP_CROP_SELECT					=	"PUT_SCRAP_CROP_SELECT";
	public static final String PUT_SCRAP_CROP_CAMERA_WIDTH				=	"PUT_SCRAP_CROP_CAMERA_WIDTH";
	public static final String PUT_SCRAP_CROP_CAMERA_HEIGHT				=	"PUT_SCRAP_CROP_CAMERA_HEIGHT";
	
	public static final int LOAD_GALLERY								=	0;
	public static final int LOAD_CAMERA									=	1;
	
	private final int REQUEST_CODE_GALLERY								=	100;
	private final int REQUEST_CODE_CAMERA								=	200;
	
	public static Activity SCRAP_IMAGE_CROP_ACTIVITY;
	
	private Context mContext;
	private MessagePool mMessagePool;
	private RelativeLayout mRlCropRootLayout;
	
	private int mLoadIndex;
	
	private int mCameraWidth, mCameraHeight;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scrap_image_crop);
		
		this.mContext = this;
		this.mMessagePool = (MessagePool)getApplicationContext();
		this.SCRAP_IMAGE_CROP_ACTIVITY = this;
		
		// 호출한 곳에서 넘겨준 값을 로드
		getIntenter();
		
		mRlCropRootLayout = (RelativeLayout)findViewById(R.id.scrap_image_crop_root);
		
		switch(mLoadIndex) {
			case LOAD_GALLERY:
					
				doSelectImage();
					
				break;
				
			case LOAD_CAMERA:

				byte[] captureData = mMessagePool.getCameraBytes();
				
				if(captureData != null) {
					Bitmap srcCameraBitmap = BitmapFactory.decodeByteArray(captureData, 0, captureData.length, ImageUtils.getBitmapOptions());
					CropView cameraCropView = new CropView(mContext, srcCameraBitmap, LOAD_CAMERA, mCameraWidth, mCameraHeight);
					
					mRlCropRootLayout.addView(cameraCropView);
					mRlCropRootLayout.invalidate();
					
				}
				
				break;
		}
	}
	
	@Override
	protected void onStop() {
		Utils.recurisveRecycle(mRlCropRootLayout);
		
		super.onStop();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		
		if(resultCode == RESULT_OK && requestCode == REQUEST_CODE_GALLERY) {

			Uri uri = intent.getData();
			String path = getAbsolutePath(mContext, uri);
			
			if(Constants.DEVELOPE_MODE) {
				Utils.logPrint(getClass(), "불러온 이미지 경로 : " + path);
			}

			if(path != null) {
				
				String regexUrl = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
				
				Pattern patternUrl = Pattern.compile(regexUrl);
				Matcher matcher = patternUrl.matcher(path);
				boolean isUrlMatch = matcher.matches();
				
				if(!isUrlMatch) {
					CropView galleryCropView = new CropView(mContext, path, LOAD_GALLERY);
		            mRlCropRootLayout.addView(galleryCropView);
		            mRlCropRootLayout.invalidate();
				} else {
					Toast.makeText(mContext, getString(R.string.scrap_crop_image_load_faile), Toast.LENGTH_SHORT).show();
					finish();
				}
			} else {
				Toast.makeText(mContext, getString(R.string.scrap_crop_image_load_faile), Toast.LENGTH_SHORT).show();
				finish();
			}
				
		} else {
			finish();
		}
		
		
	}
	
	private void doSelectImage() {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "doSelectImage 호출");
		}
		
	    Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
	    galleryIntent.setType("image/*");
	    galleryIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    
	    try {
	        startActivityForResult(galleryIntent, REQUEST_CODE_GALLERY);
	    } catch (android.content.ActivityNotFoundException e) {
	        e.printStackTrace();
	    }
	}
	
	private void getIntenter() {
		Intent intent = this.getIntent();
		
		if(intent != null) {
			mLoadIndex = intent.getExtras().getInt(PUT_SCRAP_CROP_SELECT);
			
			if(mLoadIndex == LOAD_CAMERA) {
				mCameraWidth = intent.getExtras().getInt(PUT_SCRAP_CROP_CAMERA_WIDTH);
				mCameraHeight = intent.getExtras().getInt(PUT_SCRAP_CROP_CAMERA_HEIGHT);
				
				if(Constants.DEVELOPE_MODE) {
					Utils.logPrint(getClass(), "전달 받은 카메라 프리뷰 가로, 세로 : " + mCameraWidth + ", " + mCameraHeight);
				}
			}
		}
	}
	
	// 실제 경로 찾기
	public static String getAbsolutePath(Context context, Uri uri) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(ScrapCrop.class, "getAbsolutePath 호출 (Uri : " + uri + ")");
		}
		
		String[] projection = { MediaStore.Images.Media.DATA };
	    Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
	    int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
	    cursor.moveToFirst();
	    
	    String path = cursor.getString(column_index);
		
	    return path;
	}

}












