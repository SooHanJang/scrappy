package madcat.studio.scrap;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.indexing.AdapterIndexing;
import madcat.studio.indexing.PageIndex;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

public class ScrapIndexPage extends Activity implements OnItemClickListener {
	
	public static final String PUT_SCRAP_PAGE_INDEX_TITLE				=	"PUT_SCRAP_PAGE_INDEX_TITLE";
	
	public static ScrapIndexPage ACTIVITY;

	private Context mContext;
	
	private ListView mListIndex;
	private LinearLayout mLinearRoot;
	
	private AdapterIndexing mAdapter;
	private ArrayList<PageIndex> mItems;
	
	private String mSBooksTitle;
	
	// 제스쳐를 감지할 변수
	private GestureDetector mGestureDetector;
	private IndexPageGestureListener mIndexPageGestureListener;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scrap_index_page);
		
		this.ACTIVITY = this;
		this.mContext = this;
		this.getIntenter();
		
		// Resource Id 설정
		mListIndex = (ListView)findViewById(android.R.id.list);
		mLinearRoot = (LinearLayout)findViewById(R.id.scrap_index_page_root_layout);
		
		// Event Listener 설정
		mListIndex.setOnItemClickListener(this);
		
		// 제스쳐 설정
		mIndexPageGestureListener = new IndexPageGestureListener();
		mGestureDetector = new GestureDetector(mContext, mIndexPageGestureListener);
	}
	
	@Override
	protected void onStart() {
		setListAdapter();
		
		super.onStart();
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		mGestureDetector.onTouchEvent(ev);
		return super.dispatchTouchEvent(ev);
	}
	
	@Override
	public void onBackPressed() {
		finish();
	}
	
	private void setListAdapter() {
		if(mAdapter != null) {
			mAdapter.clear();
		}
		
		// 해당 스크랩 북의 인덱스 정보를 가져온다.
		mItems = Utils.getPageIndex(mContext, mSBooksTitle);
		
		// 인덱스 정보가 존재하지 않는다면, 바로 스크랩 페이지를 호출한다.
		if(mItems.isEmpty()) {
			if(Constants.DEVELOPE_MODE) {
				Utils.logPrint(getClass(), "인덱스 정보가 존재하지 않습니다.");
			}
			
			mLinearRoot.setVisibility(View.INVISIBLE);
			
			int[] pageInfo = Utils.getPageThemeIndex(mContext, mSBooksTitle);		//	[0] = 스크랩 북 ID, [1] = 스크랩 북 페이지 테마
			
			Intent scrapPageIntent = new Intent(mContext, ScrapPage.class);
			scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_ID, pageInfo[0]);
			scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_TITLE, mSBooksTitle);
			scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_PAGE_INDEX, 0);			
			scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_PAGE_THEME_INDEX, pageInfo[1]);
			startActivity(scrapPageIntent);
		} 
		// 인덱스 정보가 존재한다면, 인덱스 List 로 구성한다.
		else {
			mLinearRoot.setVisibility(View.VISIBLE);
			
			mAdapter = new AdapterIndexing(mContext, R.layout.scrap_index_page_row, mItems);
			mListIndex.setAdapter(mAdapter);
		}
	}
	
	

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "클릭 한 페이지 인덱스의 번호 : " + (mItems.get(position).getPageNum()));
		}
		
		Intent scrapPageIntent = new Intent(mContext, ScrapPage.class);
		scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_ID, mItems.get(position).getSBookId());
		scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_TITLE, mSBooksTitle);
		scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_PAGE_INDEX, mItems.get(position).getPageNum());
		scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_PAGE_THEME_INDEX, mItems.get(position).getPageThemeIndex());
		startActivity(scrapPageIntent);
		
	}
	
	private void getIntenter() {
		Intent intent = getIntent();
		
		if(intent != null) {
			mSBooksTitle = intent.getExtras().getString(PUT_SCRAP_PAGE_INDEX_TITLE);
		}
	}
	
	/**
	 * 페이지의 제스쳐(Fling)를 감지하기 위한 리스너
	 * 
	 * @author Acsha
	 */
	class IndexPageGestureListener implements OnGestureListener {

		private final int SWIPE_MIN_DISTANCE = 120;
	    private final int SWIPE_MAX_OFF_PATH = 250;
	    private final int SWIPE_THRESHOLD_VELOCITY = 200;
	    
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,	float velocityY) {
			try {
	            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH) {
	                return false;
	            }

	            // 다음 페이지를 불러온다.
	            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	            	
	            	Intent scrapPageIntent = new Intent(mContext, ScrapPage.class);
	            	scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_ID, mItems.get(0).getSBookId());
	    			scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_TITLE, mSBooksTitle);
	    			scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_PAGE_INDEX, 0);
	    			scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_PAGE_THEME_INDEX, mItems.get(0).getPageThemeIndex());
	    			startActivity(scrapPageIntent);
	    			
	            }
	            // left to right swipe (이전 페이지로)
	            else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	            	finish();
	            }
	        } catch (Exception e) {
	            
	        }
			
			return true;
		}
		
		@Override
		public void onLongPress(MotionEvent e) {}
		public void onShowPress(MotionEvent e) {}
		public boolean onSingleTapUp(MotionEvent e) {	return false;	}
		public boolean onDown(MotionEvent e) {	return false;	}
		public boolean onScroll(MotionEvent e1, MotionEvent e2,	float distanceX, float distanceY) {	return false;	}
		
	}

}
