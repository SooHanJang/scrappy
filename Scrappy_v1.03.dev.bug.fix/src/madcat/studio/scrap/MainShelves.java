package madcat.studio.scrap;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.database.SBooksDBHelper;
import madcat.studio.dialog.DialogAddSBooks;
import madcat.studio.dialog.DialogTutorial;
import madcat.studio.scrapdata.SaveScrapData;
import madcat.studio.shelves.ShelvesLayout;
import madcat.studio.shelvesbooks.ShelvesBooks;
import madcat.studio.shelvesbooks.aShelvesBooksAdapter;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

public class MainShelves extends Activity implements OnClickListener {

	private final int FIRST_PAGE_INDEX								=	0;
	
	private final int MODE_VIEW										=	0;
	private final int MODE_REMOVE									=	1;
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	private MessagePool mMessagePool;
	
	private int mMode												=	MODE_VIEW;
	
	// 책장 레이아웃 관련 변수
	private ShelvesLayout mTableShelves;
	
	// 책 관련 변수
	private ShelvesBooksViewAdapter mAdapter;
	private ShelvesBooksViewDelAdapter mDelAdapter;
	private ArrayList<ShelvesBooks> mItems;
	
	// 책장 번호 관련 변수
	private int mCurrentPage										=	FIRST_PAGE_INDEX;
	private int mTotalPage;
	
	private GestureDetector mGestureDetector;
	private PageGesutreListener mPageGestureListener;
	
	// 스크랩 추가, 삭제, 보기 관련 변수
	private ImageButton mBtnAddScrap, mBtnRemoveScrap;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_shelves);
		
		this.mContext = this;
		this.mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		
		// App 을 최초 실행하는 지에 대한 유무 확인
        if(mConfigPref.getBoolean(Constants.CONFIG_APP_FIRST, true)) {
        	if(Constants.DEVELOPE_MODE) {
        		Utils.logPrint(getClass(), "앱을 처음 실행합니다.");
        	}
        	
        	// 앱에 사용할 폴더 전체 초기화
        	Utils.initFilePath();
        	
        	// DB 생성
        	SBooksDBHelper sBooksDBHelper = new SBooksDBHelper(mContext);
        	
        	SharedPreferences.Editor editor = mConfigPref.edit();
        	editor.putBoolean(Constants.CONFIG_APP_FIRST, false);
        	editor.commit();
        	
        	// 도움말 활성화
        	Point screenPoint = Utils.getScreenSize(mContext);
        	
        	DialogTutorial digTutorial = new DialogTutorial(mContext, DialogTutorial.TUTORIAL_PAGE_FIRST);
			
			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.copyFrom(digTutorial.getWindow().getAttributes());
			
			int width = (int) (screenPoint.x * 0.9);
			int height = (int) ((width / DialogTutorial.DIALOG_IMAGE_RATIO_WIDTH) * DialogTutorial.DIALOG_IMAGE_RATIO_HEIGHT);
			
			lp.width = width;
			lp.height = height;
			
			digTutorial.show();
			
			Window window = digTutorial.getWindow();
			window.setAttributes(lp);
        	
        }
		
        
        // 책장 설정
        mTableShelves = (ShelvesLayout)findViewById(R.id.main_shelves_layout_item_table);
        
        // 버튼 설정
        mBtnAddScrap = (ImageButton)findViewById(R.id.main_shelves_add_scrap);
        mBtnRemoveScrap = (ImageButton)findViewById(R.id.main_shelves_remove_scrap);
        
        // Click Listener 설정
        mBtnAddScrap.setOnClickListener(this);
        mBtnRemoveScrap.setOnClickListener(this);
        
        setGestureListener(mContext);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		switch(mMode) {
			case MODE_VIEW:
				initAdapter();
				break;
				
			case MODE_REMOVE:
				initDelAdapter();
				break;
		
		}
	}
	
	
	@Override
    public boolean onTouchEvent(MotionEvent event) {
    	return mGestureDetector.onTouchEvent(event);
    }
	
	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.main_shelves_add_scrap:
				
				if(mMode == MODE_VIEW) {
					DialogAddSBooks digAddSbooks = new DialogAddSBooks(mContext, DialogAddSBooks.MODE_ADD);
					digAddSbooks.show();
				} else {
					Toast.makeText(mContext, getString(R.string.main_shelves_please_cancel_remove), Toast.LENGTH_SHORT).show();
				}
				
				break;
			case R.id.main_shelves_remove_scrap:
				if(mItems.isEmpty()) {
					Toast.makeText(mContext, getString(R.string.main_shelves_remove_is_empty_toast), Toast.LENGTH_SHORT).show();
				} else {
					if(mMode != MODE_REMOVE) {
						initDelAdapter();
					} else {
						initAdapter();
					}
				}
				
				break;
		}
	}
	
	private void initAdapter() {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "initAdapter 호출");
		}
		
		mMode = MODE_VIEW;
		
    	// 책 아이템 설정
		if(mItems != null) {
			mItems.clear();
		}
    	
    	// ShelvesBooks 전부 읽어오기
    	mItems = Utils.getShelvesBooksFromDB(mContext);
    	
        // 총 페이지 설정
        setTotalPage(mItems.size());
        
        // 어댑터 설정
        if(mAdapter != null) {	mAdapter.clear();	}
        if(mDelAdapter != null) {	mDelAdapter.clear();	}
        
        // 스크랩 북이 가득 찼을 경우, 추가 되었을 시에 자동으로 다음 책장으로 넘어가게 끔 구현할 것.
    	mAdapter = new ShelvesBooksViewAdapter(mContext, R.layout.main_shelves_item, mItems);
    	mTableShelves.setSideNAdapter(mAdapter, mCurrentPage);
    }
	
	private void initDelAdapter() {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "initDelAdapter");
		}
		
		mMode = MODE_REMOVE;
		
		if(mItems != null) {
			mItems.clear();
		}
		
		// ShelvesBooks 전부 읽어오기
    	mItems = Utils.getShelvesBooksFromDB(mContext);
    	
        // 총 페이지 설정
        setTotalPage(mItems.size());
        
        // 어댑터 설정
        if(mAdapter != null) {	mAdapter.clear();	}
        if(mDelAdapter != null) {	mDelAdapter.clear();	}

        if(mCurrentPage != 0 && (mItems.size() % (ShelvesLayout.MAX_BOOKS_COUNT * ShelvesLayout.MAX_ROW_COUNT)) == 0) {
        	mCurrentPage -= 1;
        }
        
        // 모든 스크랩 북이 삭제 되었을 경우, 일반 어댑터로 전환
        if(mItems.isEmpty()) {
        	mMode = MODE_VIEW;
        	
        	mAdapter = new ShelvesBooksViewAdapter(mContext, R.layout.main_shelves_item, mItems);
        	mTableShelves.setSideNAdapter(mAdapter, mCurrentPage);
        } 
        // 삭제할 수 있는 스크랩 북이 1개 이상 존재할 경우, 삭제 어댑터 사용
        else {
        	mDelAdapter = new ShelvesBooksViewDelAdapter(mContext, R.layout.main_shelves_del_item, mItems);
        	mTableShelves.setSideNAdapter(mDelAdapter, mCurrentPage);
        }
    	

    	
	}
	
	private void setTotalPage(int itemSize) {
		if(itemSize <= (ShelvesLayout.MAX_BOOKS_COUNT * ShelvesLayout.MAX_ROW_COUNT)) {
			mTotalPage = 1;
		} else {
			if(itemSize % (ShelvesLayout.MAX_BOOKS_COUNT * ShelvesLayout.MAX_ROW_COUNT) == 0) {
				mTotalPage = itemSize / (ShelvesLayout.MAX_BOOKS_COUNT * ShelvesLayout.MAX_ROW_COUNT);
			} else {
				mTotalPage = itemSize / (ShelvesLayout.MAX_BOOKS_COUNT * ShelvesLayout.MAX_ROW_COUNT) + 1;
			}
		}
    	
    }
	    
    private void setGestureListener(Context context) {
		mPageGestureListener = new PageGesutreListener();
		mGestureDetector = new GestureDetector(context, mPageGestureListener);
	}
    
    @Override
    public void onBackPressed() {
    	switch(mMode) {
	    	case MODE_VIEW:
	    		finish();
	    		break;
	    		
	    	case MODE_REMOVE:
	    		initAdapter();
	    		break;
    	}
    	
    }
    
	class PageGesutreListener implements OnGestureListener {
		
		private final int SWIPE_MIN_DISTANCE = 120;
	    private final int SWIPE_MAX_OFF_PATH = 250;
	    private final int SWIPE_THRESHOLD_VELOCITY = 200;

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
			try {
	            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH) {
	                return false;
	            }

	            // 다음 페이지를 불러온다.
	            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	            	if(mCurrentPage == mTotalPage -1) {
	            		Toast.makeText(mContext, getString(R.string.main_shelves_last_page), Toast.LENGTH_SHORT).show();
	            	} else {
	            		mCurrentPage += 1;
	            		
	            		mTableShelves.setCurrentPage(mCurrentPage);
	            		mTableShelves.updateLayout();
	            	}
	            }
	            // left to right swipe (이전 페이지로)
	            else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	            	if(mCurrentPage == FIRST_PAGE_INDEX) {
	            		Toast.makeText(mContext, getString(R.string.main_shelves_first_page), Toast.LENGTH_SHORT).show();
	            		
	            	} else {
	            		mCurrentPage -= 1;
	            		
	            		mTableShelves.setCurrentPage(mCurrentPage);
	            		mTableShelves.updateLayout();
	            	}
	            }
	            
	        } catch (Exception e) {
	            
	        }
	        
	        return true;
		}

		@Override
		public boolean onDown(MotionEvent e) {	return false;	}
		public void onLongPress(MotionEvent e) {}
		public boolean onScroll(MotionEvent e1, MotionEvent e2,	float distanceX, float distanceY) {	return false;	}
		public void onShowPress(MotionEvent e) {}
		public boolean onSingleTapUp(MotionEvent e) {	return false;	}
		
	}

	/**
	 * 책장을 화면에 표시해주기 위해 쓰이는 View Adapter
	 * 
	 * @author Acsha
	 *
	 */
	private class ShelvesBooksViewAdapter extends aShelvesBooksAdapter{
		
		private Context mContext;
		private int mResId;
		private ArrayList<ShelvesBooks> mItems;
		
		private LayoutInflater mInflater;
		
		private LinearLayout.LayoutParams mLiRootParmas;
		private RelativeLayout.LayoutParams mRlRootParams;
		
		private MessagePool mMessagePool;
		
		private int mWidth, mHeight, mSideMargin, mBottomMargin;
		
		public ShelvesBooksViewAdapter(Context context, int resId, ArrayList<ShelvesBooks> items) {
			super(context, resId, items);
			this.mContext = context;
			this.mResId = resId;
			this.mItems = items;
			
			this.mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			this.mLiRootParmas = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			this.mRlRootParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
			this.mMessagePool = (MessagePool) mContext.getApplicationContext();
		}
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;
			
			if(convertView == null) {
				convertView = mInflater.inflate(mResId, null);
				
				holder = new ViewHolder();
				holder.mRlRoot = (RelativeLayout)convertView.findViewById(R.id.main_shelves_item_root_layout);
				holder.mRlBook = (RelativeLayout)convertView.findViewById(R.id.main_shelves_item_book_layout);
				holder.mIvShadow = (ImageView)convertView.findViewById(R.id.main_shelves_item_shadow_bottom);
				
				holder.mTvTitle = (TextView)convertView.findViewById(R.id.main_shelves_item_title);
				holder.mTvDate = (TextView)convertView.findViewById(R.id.main_shelves_item_date);
				holder.mTvPageTotal = (TextView)convertView.findViewById(R.id.main_shelves_item_total_page);
				
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder)convertView.getTag();
			}
			
			// LayoutParam 설정
			mLiRootParmas.width = mWidth;
			mLiRootParmas.height = mHeight;
			mLiRootParmas.leftMargin = mSideMargin;
			mLiRootParmas.bottomMargin = mBottomMargin;
			
			holder.mRlRoot.setLayoutParams(mLiRootParmas);
			
			holder.mIvShadow.post(new Runnable() {
				@Override
				public void run() {
					int sWidth = holder.mIvShadow.getWidth();
					int sHeight = holder.mIvShadow.getHeight();
					
					if(Constants.DEVELOPE_MODE) {
						Utils.logPrint(getClass(), "바닥 그림자 가로, 세로 : " + sWidth + ", " + sHeight);
					}
					
					mRlRootParams.bottomMargin = (int) (sHeight / 2.2);
					mRlRootParams.leftMargin = (int) (sWidth * 0.05);
					mRlRootParams.rightMargin = (int) (sWidth * 0.05);
					
					holder.mRlBook.setLayoutParams(mRlRootParams);
					
					holder.mRlBook.setBackgroundResource(Utils.getId(mContext, "drawable", Constants.THEME_COVER_PREFIX, mItems.get(position).getCoverThemeIndex()));
					
					holder.mRlBook.post(new Runnable() {
						@Override
						public void run() {
							int bWidth = holder.mRlBook.getWidth();
							int bHeight = holder.mRlBook.getHeight();
							
							if(Constants.DEVELOPE_MODE) {
								Utils.logPrint(getClass(), "책의 가로, 세로 : " + bWidth + ", " + bHeight);
							}
							
							// 책의 제목 마진 설정
							RelativeLayout.LayoutParams rpTitle =  (LayoutParams) holder.mTvTitle.getLayoutParams();
							rpTitle.topMargin = (int) (bHeight * 0.23);
							holder.mTvTitle.setLayoutParams(rpTitle);
							
							// 책의 제목 Max Width 설정
							holder.mTvTitle.setMaxWidth((int) (bWidth * 0.7));
							
							// 책 작성 날짜 마진 설정
							RelativeLayout.LayoutParams rpDate = (LayoutParams) holder.mTvDate.getLayoutParams();
							rpDate.bottomMargin = (int) (bHeight * 0.10);
							holder.mTvDate.setLayoutParams(rpDate);
							
							// 책 정보 설정
							holder.mTvTitle.setText("" + mItems.get(position).getTitle());
							holder.mTvDate.setText("" + mItems.get(position).getDate());
							holder.mTvPageTotal.setText("- " + mItems.get(position).getTotlaPageIndex() + " -");
							
						}
					});
				}
			});
			
			holder.mRlRoot.setOnTouchListener(new OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					return mGestureDetector.onTouchEvent(event);
				}
			});
			
			// 책장의 스크랩 북을 클릭 했을 경우,
			holder.mRlRoot.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					PreReadPageAsync preReadPageAsync = new PreReadPageAsync(mItems.get(position).getTitle());
					preReadPageAsync.execute();
				}
			});
			
			// 책장의 스크랩 북을 롱 클릭 했을 경우, (스크랩 북 설정 변경 시)
			holder.mRlRoot.setOnLongClickListener(new OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					
					final DialogAddSBooks digAddSbooks = new DialogAddSBooks(mContext, DialogAddSBooks.MODE_MODIFY, mItems.get(position));
					digAddSbooks.show();
					
					digAddSbooks.setOnDismissListener(new OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
							initAdapter();
						}
					});
					
					return true;
				}
			});
			
			return convertView;
		}
		
		public void setBooksSize(int width, int height, int sMargin, int bMargin) {
			this.mWidth = width;
			this.mHeight = height;
			this.mSideMargin = sMargin;
			this.mBottomMargin = bMargin;
		}
		
		class ViewHolder {
			private RelativeLayout mRlRoot, mRlBook;
			private ImageView mIvShadow;
			private TextView mTvTitle;
			private TextView mTvDate;
			private TextView mTvPageTotal;
		}
		
		/**
		 * 페이지 정보를 미리 읽어 들여 MessagePool 에 설정한 뒤, 인덱스 페이지를 실행하는 AsyncTask
		 *
		 * @author Acsha
		 */
		class PreReadPageAsync extends AsyncTask<Void, Void, Void> {

			ProgressDialog proDig;
			String title;
			
			public PreReadPageAsync(String title) {
				this.title = title;
			}
			
			@Override
			protected void onPreExecute() {
				proDig = ProgressDialog.show(mContext, "", mContext.getString(R.string.scrap_page_dig_load_msg));
			}
			
			@Override
			protected Void doInBackground(Void... params) {
				ArrayList<SaveScrapData> saveScrapArray = Utils.getReadAllScrapPage(mContext, title);
				mMessagePool.setOriginScrapArray(Utils.convertArraySaveToOrigin(saveScrapArray));

				return null;
			}
			
			@Override
			protected void onPostExecute(Void result) {
				if(proDig.isShowing()) {
					proDig.dismiss();
				}
				
				// 인덱스 페이지 개발하고자 할시에는 반드시 이 코드를 활성화 한다.
//				Intent scrapIndexIntent = new Intent(mContext, ScrapIndexPage.class);
//				scrapIndexIntent.putExtra(ScrapIndexPage.PUT_SCRAP_PAGE_INDEX_TITLE, title);
//				mContext.startActivity(scrapIndexIntent);
				
				// 스크랩 페이지를 호출한다.
				int[] pageInfo = Utils.getPageThemeIndex(mContext, title);		//	[0] = 스크랩 북 ID, [1] = 스크랩 북 페이지 테마
				
				Intent scrapPageIntent = new Intent(mContext, ScrapPage.class);
				scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_ID, pageInfo[0]);
				scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_TITLE, title);
				scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_PAGE_INDEX, 0);			
				scrapPageIntent.putExtra(ScrapPage.PUT_SCRAP_BOOK_PAGE_THEME_INDEX, pageInfo[1]);
				startActivity(scrapPageIntent);
			}
		}

	}

	/**
	 * 책장 삭제에 쓰이는 삭제 어댑터
	 * 
	 * @author Acsha
	 *
	 */
	private class ShelvesBooksViewDelAdapter extends aShelvesBooksAdapter{
		
		private Context mContext;
		private int mResId;
		private ArrayList<ShelvesBooks> mItems;
		
		private LayoutInflater mInflater;
		private LinearLayout.LayoutParams mLiRootParmas;
		private RelativeLayout.LayoutParams mRlRootParams;
		
		private int mWidth, mHeight, mSideMargin, mBottomMargin;
		
		private AlertDialog.Builder mBuilder;
		
		public ShelvesBooksViewDelAdapter(Context context, int resId, ArrayList<ShelvesBooks> items) {
			super(context, resId, items);
			this.mContext = context;
			this.mResId = resId;
			this.mItems = items;
			
			this.mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			this.mLiRootParmas = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			this.mRlRootParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
			
			mBuilder = new AlertDialog.Builder(mContext);
			
		}
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;
			
			if(convertView == null) {
				convertView = mInflater.inflate(mResId, null);
				
				holder = new ViewHolder();
				holder.mRlRoot = (RelativeLayout)convertView.findViewById(R.id.main_shelves_del_item_root_layout);
				holder.mRlBook = (RelativeLayout)convertView.findViewById(R.id.main_shelves_del_item_book_layout);
				holder.mIvShadow = (ImageView)convertView.findViewById(R.id.main_shelves_del_item_shadow_bottom);
				holder.mIvDel = (ImageButton)convertView.findViewById(R.id.main_shelves_del_item_remove);
				
				holder.mTvTitle = (TextView)convertView.findViewById(R.id.main_shelves_del_item_title);
				holder.mTvDate = (TextView)convertView.findViewById(R.id.main_shelves_del_item_date);
				holder.mTvPageTotal = (TextView)convertView.findViewById(R.id.main_shelves_del_item_total_page);
				
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder)convertView.getTag();
			}
			
			// LayoutParam 설정
			mLiRootParmas.width = mWidth;
			mLiRootParmas.height = mHeight;
			mLiRootParmas.leftMargin = mSideMargin;
			mLiRootParmas.bottomMargin = mBottomMargin;
			
			holder.mRlRoot.setLayoutParams(mLiRootParmas);
			
			holder.mIvShadow.post(new Runnable() {
				@Override
				public void run() {
					int sWidth = holder.mIvShadow.getWidth();
					int sHeight = holder.mIvShadow.getHeight();
					
					if(Constants.DEVELOPE_MODE) {
						Utils.logPrint(getClass(), "바닥 그림자 가로, 세로 : " + sWidth + ", " + sHeight);
					}
					
					mRlRootParams.bottomMargin = (int) (sHeight / 2.2);
					mRlRootParams.leftMargin = (int) (sWidth * 0.05);
					mRlRootParams.rightMargin = (int) (sWidth * 0.05);
					
					holder.mRlBook.setLayoutParams(mRlRootParams);
					
					holder.mRlBook.setBackgroundResource(Utils.getId(mContext, "drawable", Constants.THEME_COVER_PREFIX, mItems.get(position).getCoverThemeIndex()));
					
					holder.mRlBook.post(new Runnable() {
						@Override
						public void run() {
							int bWidth = holder.mRlBook.getWidth();
							int bHeight = holder.mRlBook.getHeight();
							
							if(Constants.DEVELOPE_MODE) {
								Utils.logPrint(getClass(), "책의 가로, 세로 : " + bWidth + ", " + bHeight);
							}
							
							// 책의 제목 마진 설정
							RelativeLayout.LayoutParams rpTitle =  (LayoutParams) holder.mTvTitle.getLayoutParams();
							rpTitle.topMargin = (int) (bHeight * 0.23);
							holder.mTvTitle.setLayoutParams(rpTitle);
							
							// 책의 제목 Max Width 설정
							holder.mTvTitle.setMaxWidth((int) (bWidth * 0.7));
							
							// 책 작성 날짜 마진 설정
							RelativeLayout.LayoutParams rpDate = (LayoutParams) holder.mTvDate.getLayoutParams();
							rpDate.bottomMargin = (int) (bHeight * 0.10);
							holder.mTvDate.setLayoutParams(rpDate);
							
							// 책 정보 설정
							holder.mTvTitle.setText("" + mItems.get(position).getTitle());
							holder.mTvDate.setText("" + mItems.get(position).getDate());
							holder.mTvPageTotal.setText("- " + mItems.get(position).getTotlaPageIndex() + " -");
							
							// 삭제 버튼 설정
							RelativeLayout.LayoutParams rpRemove = (LayoutParams) holder.mIvDel.getLayoutParams();
							rpRemove.rightMargin = (int) (bWidth * 0.08);
							rpRemove.topMargin = (int) (bHeight * 0.05);
							holder.mIvDel.setLayoutParams(rpRemove);
							holder.mIvDel.setBackgroundResource(R.drawable.icon_sbooks_remove);
						}
					});
				}
			});
			
			holder.mIvDel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if(Constants.DEVELOPE_MODE) {
						Utils.logPrint(getClass(), position + " 번째 삭제 버튼을 클릭");
					}
					
					AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
					builder.setTitle(getString(R.string.main_shelves_dig_del_title));
					builder.setMessage(getString(R.string.main_shelves_dig_del_msg));
					builder.setPositiveButton(getString(R.string.confirm_dig_ok), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Utils.deleteSBooks(mContext, mItems.get(position).getTitle());
							
							initDelAdapter();
							dialog.dismiss();
						}
					});
					builder.setNegativeButton(getString(R.string.confirm_dig_cancel), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
					
					builder.show();
				}
			});
			
			return convertView;
		}
		
		public void setBooksSize(int width, int height, int sMargin, int bMargin) {
			this.mWidth = width;
			this.mHeight = height;
			this.mSideMargin = sMargin;
			this.mBottomMargin = bMargin;
		}
		
		class ViewHolder {
			private RelativeLayout mRlRoot, mRlBook;
			private ImageView mIvShadow;
			private ImageButton mIvDel;
			private TextView mTvTitle;
			private TextView mTvDate;
			private TextView mTvPageTotal;
		}

	}

}










