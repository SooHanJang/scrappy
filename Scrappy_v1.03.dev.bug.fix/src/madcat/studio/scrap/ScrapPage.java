package madcat.studio.scrap;

import java.io.File;
import java.util.ArrayList;

import madcat.studio.camera.CameraPicture;
import madcat.studio.constants.Constants;
import madcat.studio.dialog.DialogTutorial;
import madcat.studio.position.CropViewPos;
import madcat.studio.position.TextViewPos;
import madcat.studio.scrapdata.OriginScrapData;
import madcat.studio.scrapdata.SaveScrapData;
import madcat.studio.share.ShareImageActivity;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import madcat.studio.view.ColorPickerView;
import madcat.studio.view.ColorPickerView.OnColorChangedListener;
import madcat.studio.view.PenView;
import madcat.studio.view.ScrapView;
import madcat.studio.view.ScrapViewLayout;
import madcat.studio.view.TextEditLayout;
import madcat.studio.view.TextEditView;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class ScrapPage extends Activity implements OnClickListener {
	
	public static final String PUT_SCRAP_BOOK_ID						=	"PUT_SCRAP_BOOK_ID";
	public static final String PUT_SCRAP_BOOK_TITLE						=	"PUT_SCRAP_BOOK_TITLE";
	public static final String PUT_SCRAP_BOOK_PAGE_INDEX				=	"PUT_SCRAP_BOOK_PAGE_INDEX";
	public static final String PUT_SCRAP_BOOK_PAGE_STATE				=	"PUT_SCRAP_BOOK_PAGE_STATE";
	public static final String PUT_SCRAP_BOOK_PAGE_THEME_INDEX			=	"PUT_SCRAP_BOOK_PAGE_THEME";

	public static final int PAGE_PREV									=	0;
	public static final int PAGE_NEXT									=	1;
	
	public static final int LOAD_NEW_PAGE								=	0;					//	새로운 페이지를 작성
	public static final int LOAD_MODIFY_PAGE							=	1;					//	현재 페이지를 수정
	private int mPageState												=	LOAD_NEW_PAGE;
	
	// 페이지 저장 후의 이벤트에 따른 전역 변수
	public static final int SAVE_MODE_ONLY								=	0;					//	단순히 저장만 한다.
	public static final int SAVE_MODE_ADD								=	1;					//	저장 뒤 다음 페이지 불러오기 혹은 새로운 페이지 생성
	public static final int SAVE_MODE_SHARE								=	2;					//	공유를 위해, 페이지를 단순 저장합니다.
	public static final int SAVE_MODE_EXIT								=	3;					//	저장 뒤 프로그램 종료
	
	// 모드를 설정하는 타입
	public static final int MODE_CALLER_TYPE_ID							=	0;
	public static final int MODE_CALLER_TYPE_INDEX						=	1;
	
	// 컬러 색상을 설정하는 타입
	private static final int SELECT_FIXED_COLOR							=	0;
	private static final int SELECT_PICKER_COLOR						=	1;
	private static int mColorStyleType									=	SELECT_FIXED_COLOR;
	
	private static Context mContext;
	private static MessagePool mMessagePool;
	
	// 현재 페이지 관련 변수
	private int mSbookId;
	private String mSbookTitle;
	private static int mCurrentPageIndex								=	0;
	private int mCurrentPageThemeIndex									=	0;
	private static String mCurrentPageInfo;
	private static boolean mCurrentPageModify							=	false;
	
	// ScrapPage Layout 관련 변수
	private RelativeLayout mRelativeRoot, mRlCenterToolLayout;
	private static RelativeLayout mRelativeScrpLayout;
	private static TextEditLayout mTextEditLayout;
	private ScrapViewLayout mScrapViewLayout;
	
	private static ImageButton mBtnLayerUp, mBtnLayerDown;
	private static ImageView mImgTrash;
	
	private ImageButton mBtnMenu;
	private static ImageButton mBtnPenEraser;

	// Display 모드 관련 변수
	private static ImageButton mBtnDisplayMode;
	private int mPrevMode												=	Constants.MENU_MODE_EDIT;
	private int mCurrMode												=	-1;
	
	// 퀵 바 메뉴 관련 변수
	private static View mViewQuickBarMenu, mViewQuickDecoLayout;
	private static ImageButton mBtnQuickScrap, mBtnQuickDecoration, mBtnQuickStyle;
	private static LinearLayout mLiQuickStyleDetail;
	
	// 서브메뉴 버튼 관련 변수
	private static View mViewToolMenu;
	private static TextView mTvSubDisplayPageInfo, mTvPageIndex;
	private ImageButton mBtnToolMenuView, mBtnToolMenuScrap, mBtnToolMenuPen, mBtnToolMenuText, 
				   mBtnToolMenuEdit, mBtnToolMenuInfo, mBtnToolMenuPageAdd, mBtnToolMenuPageDel,
				   mBtnToolMenuShare;
	
	// 펜 뷰 관련 변수
	private static PenView mPenView;

	// 스타일 관련 변수
	private static View mPenBrushView, mTextStyleView;
	private static ImageButton[] mBtnPenBrushStyle;
	private static CheckBox[] mCheckTextViewStyle;
	
	// 데코레이션 관련 변수
	private View mViewDecoDetailPaper, mViewDecoDetailTape, mViewDecoDetailSticker;
	private ImageButton mBtnDecoPaper, mBtnDecoTape, mBtnDecoSticker;
	private ImageButton[] mBtnDecoPaperItem, mBtnDecoTapeItem, mBtnDecoStickerItem;
	
	// 색상 버튼 관련 변수
	private String[] mColorName											=	{"white", "yellow", "orange", "red", "blue", "green",
																			 "navy_blue", "purple", "pink", "brown", "black"};
	
	private static int[] mColorInteger;
	
	private static ImageView mBtnColorDetail;
	private static LinearLayout mLiColorPickerLayout;
	private static RelativeLayout[] mBtnPickColor;
	private static RelativeLayout mBtnPickerColorLayout;
	
	private static SeekBar mSeekWidth;
	private int mPickedColor											=	Color.WHITE;
	private PointF mSelectedPoint;
	
	// 레이아웃 크기를 계산하는 가로, 세로 값
	private static int mScreenWidth, mScreenHeight;
	
	// 전체 페이지를 미리 담고 있을 ArrayList
	private ArrayList<OriginScrapData> mOriginScrapArray;
	
	// 제스쳐를 감지할 변수
	private GestureDetector mGestureDetector;
	private PageGestureListener mPageGestureListener;
	
	// 메뉴가 보여지고 있는지를 저장할 Static 변수
	private static boolean mStyleMenuShowFlag, mDecoMenuShowFlag;
	
	// 서브 메뉴 에니메이션 변수
	private static Animation mAnimToolMenuOpen, mAnimToolMenuClose;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	if(Constants.DEVELOPE_MODE) {
    		Utils.logPrint(getClass(), "onCreate 호출");
    	}
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scrap_page);

        // 기본 설정
        this.mContext = this;
        this.mMessagePool = (MessagePool)getApplicationContext();
        this.getIntenter();

        // Resource Id 및 이벤트 초기화
        mRelativeRoot = (RelativeLayout)findViewById(R.id.scrap_page_root);
        mRlCenterToolLayout = (RelativeLayout)findViewById(R.id.scrap_page_center_tool_layout);
        mRelativeScrpLayout = (RelativeLayout)findViewById(R.id.scrap_page_layout);
        mImgTrash = (ImageView)findViewById(R.id.scrap_page_trash);
        mBtnMenu = (ImageButton)findViewById(R.id.scrap_page_menu_btn);
        
        mBtnLayerUp = (ImageButton)findViewById(R.id.scrap_page_layer_up);
        mBtnLayerDown = (ImageButton)findViewById(R.id.scrap_page_layer_down);
        mBtnDisplayMode = (ImageButton)findViewById(R.id.scrap_page_display_mode); 
        mTvPageIndex = (TextView)findViewById(R.id.scrap_page_num);
        mViewToolMenu = (View)findViewById(R.id.scrap_page_menu_layout);
        mViewQuickBarMenu = (View)findViewById(R.id.scrap_page_quick_bar_layout);
        mBtnPenEraser = (ImageButton)findViewById(R.id.scrap_page_pen_eraser);
        
        mPenBrushView = (View)findViewById(R.id.scrap_tool_pen_style);
        mTextStyleView = (View)findViewById(R.id.scrap_tool_text_style);
        
        mPenBrushView.setVisibility(View.GONE);
        mTextStyleView.setVisibility(View.GONE);
        
        // SeekBar 버튼 초기화 및 Listener 설정
        mSeekWidth = (SeekBar)findViewById(R.id.scrap_tool_size_seek_bar);
        mSeekWidth.setOnSeekBarChangeListener(new SeekBarListener());
        
        // 페이지 바탕화면 설정
        mRelativeRoot.setBackgroundResource(Utils.getId(mContext, "drawable", Constants.THEME_PAGE_PREFIX, mCurrentPageThemeIndex));
        
        // 뷰를 화면의 맨 앞으로 위치
        mRlCenterToolLayout.bringToFront();
        mViewQuickBarMenu.bringToFront();
        mBtnDisplayMode.bringToFront();
        mBtnMenu.bringToFront();
        mTvPageIndex.bringToFront();
        
        mViewToolMenu.setVisibility(View.GONE);
        mViewToolMenu.bringToFront();
        
        // 제스쳐 설정
        mPageGestureListener = new PageGestureListener();
        mGestureDetector = new GestureDetector(mContext, mPageGestureListener);
        
        // Event Listener 설정
        mBtnLayerUp.setOnClickListener(this);
        mBtnLayerDown.setOnClickListener(this);
        mBtnPenEraser.setOnClickListener(this);
        mBtnMenu.setOnClickListener(this);
        mBtnDisplayMode.setOnClickListener(this);
        
        // 메뉴 초기화
        initToolMenu();
        initDecorationMenu();
        initStyleMenu();
        initQuickBarMenu();
        
        // 스크랩 페이지에서 사용할 OriginScrapArray 설정
        mOriginScrapArray = mMessagePool.getOriginScrapArray();

        if(Constants.DEVELOPE_MODE) {
        	Utils.logPrint(getClass(), "Original Scarp Array Size : " + mOriginScrapArray.size());
        }
        
        if(mOriginScrapArray.isEmpty()) {
			mPageState = LOAD_NEW_PAGE;
		} else {
			mPageState = LOAD_MODIFY_PAGE;
			applyOriginDataToLayout(mOriginScrapArray.get(mCurrentPageIndex));	//	첫 페이지를 불러와서 값을 설정
		}
        
        // 스크랩 페이지 레이아웃 설정
        setScrapViewLayout();
        
        // ToolMenu 에 사용할 애니메이션 설정
        initToolMenuAnimation();
        
    }
    
    @Override
    protected void onDestroy() {
    	if(Constants.DEVELOPE_MODE) {
    		Utils.logPrint(getClass(), "onDestroy 호출");
    	}
    	
    	initAndFinish();
    	
    	super.onDestroy();
    }
    
    /**
     * 툴 메뉴에 사용하는 버튼들의 초기화 및 리스너를 설정합니다.
     * 
     */
    private void initToolMenu() {
    	if(Constants.DEVELOPE_MODE) {
    		Utils.logPrint(getClass(), "initToolMenu 호출");
    	}
    	
        mTvSubDisplayPageInfo = (TextView)findViewById(R.id.scrap_tool_menu_display_page_info);
        mBtnToolMenuView = (ImageButton)findViewById(R.id.scrap_tool_menu_view);
        mBtnToolMenuScrap = (ImageButton)findViewById(R.id.scrap_tool_menu_scrap);
        mBtnToolMenuPen = (ImageButton)findViewById(R.id.scrap_tool_menu_pen);
        mBtnToolMenuText = (ImageButton)findViewById(R.id.scrap_tool_menu_text);
        mBtnToolMenuEdit = (ImageButton)findViewById(R.id.scrap_tool_menu_edit);
        mBtnToolMenuPageAdd = (ImageButton)findViewById(R.id.scrap_tool_menu_page_add);
        mBtnToolMenuPageDel = (ImageButton)findViewById(R.id.scrap_tool_menu_page_delete);
        mBtnToolMenuInfo = (ImageButton)findViewById(R.id.scrap_tool_menu_info);
        mBtnToolMenuShare = (ImageButton)findViewById(R.id.scrap_tool_menu_share);

        // SubMenu Listener 설정
        ToolMenuClickListener toolMenuClickListener = new ToolMenuClickListener();
        
        mTvSubDisplayPageInfo.setOnClickListener(toolMenuClickListener);
        mBtnToolMenuView.setOnClickListener(toolMenuClickListener);
        mBtnToolMenuScrap.setOnClickListener(toolMenuClickListener);
        mBtnToolMenuPen.setOnClickListener(toolMenuClickListener);
        mBtnToolMenuText.setOnClickListener(toolMenuClickListener);
        mBtnToolMenuEdit.setOnClickListener(toolMenuClickListener);
        mBtnToolMenuPageAdd.setOnClickListener(toolMenuClickListener);
        mBtnToolMenuPageDel.setOnClickListener(toolMenuClickListener);
        mBtnToolMenuInfo.setOnClickListener(toolMenuClickListener);
        mBtnToolMenuShare.setOnClickListener(toolMenuClickListener);
    }
    
    /**
     * QuickBarMenu 에 사용하는 버튼들의 초기화 및 리스너를 설정합니다.
     * 
     */
    private void initQuickBarMenu() {
    	if(Constants.DEVELOPE_MODE) {
    		Utils.logPrint(getClass(), "initQuickBarMenu 호출");
    	}
    	
    	mBtnQuickScrap = (ImageButton)findViewById(R.id.scrap_tool_quick_bar_scrap);
    	mBtnQuickDecoration = (ImageButton)findViewById(R.id.scrap_tool_quick_bar_decoration);
    	mBtnQuickStyle = (ImageButton)findViewById(R.id.scrap_tool_quick_bar_style);
    	
    	mLiQuickStyleDetail = (LinearLayout)findViewById(R.id.scrap_tool_quick_bar_color_detail_layout);
    	mLiQuickStyleDetail.setVisibility(View.GONE);
    	
    	mViewQuickDecoLayout = (View)findViewById(R.id.scrap_tool_quick_bar_decoration_layout);
    	mViewQuickDecoLayout.setVisibility(View.GONE);
    	
    	QuickBarMenuItemClickListener quickBarClickListener = new QuickBarMenuItemClickListener();

    	mBtnQuickScrap.setOnClickListener(quickBarClickListener);
    	mBtnQuickDecoration.setOnClickListener(quickBarClickListener);
    	mBtnQuickStyle.setOnClickListener(quickBarClickListener);
    }
    
    /**
     * 데코레이션 메뉴에 사용하는 버튼들의 초기화 및 리스너를 설정합니다.
     * 
     */
    private void initDecorationMenu() {
    	if(Constants.DEVELOPE_MODE) {
    		Utils.logPrint(getClass(), "initDecorationMenu 호출");
    	}
    	
    	mViewDecoDetailPaper = (View)findViewById(R.id.scrap_tool_deco_menu_detail_paper);
    	mViewDecoDetailTape = (View)findViewById(R.id.scrap_tool_deco_menu_detail_tape);
    	mViewDecoDetailSticker = (View)findViewById(R.id.scrap_tool_deco_menu_detail_sticker);
    	
    	mBtnDecoPaper = (ImageButton)findViewById(R.id.scrap_tool_deco_menu_paper);
    	mBtnDecoTape = (ImageButton)findViewById(R.id.scrap_tool_deco_menu_tape);
    	mBtnDecoSticker = (ImageButton)findViewById(R.id.scrap_tool_deco_menu_sticker);
    	
    	DecoMenuClickListener decoClickListener = new DecoMenuClickListener();
    	
    	mBtnDecoPaper.setOnClickListener(decoClickListener);
    	mBtnDecoTape.setOnClickListener(decoClickListener);
    	mBtnDecoSticker.setOnClickListener(decoClickListener);
    	
    	// 데코레이션 메뉴 아이템 클릭 설정
    	DecoMenuItemClickListener decoItemClickListener = new DecoMenuItemClickListener();
    	
    	mBtnDecoPaperItem = new ImageButton[Constants.DECO_PAPER_ITEM_SIZE];
    	mBtnDecoTapeItem = new ImageButton[Constants.DECO_TAPE_ITEM_SIZE];
    	mBtnDecoStickerItem = new ImageButton[Constants.DECO_STICKER_ITEM_SIZE];
    	
    	for(int i=0; i < Constants.DECO_PAPER_ITEM_SIZE; i++) {
    		int resId = getResources().getIdentifier("deco_paper_" + i, "id", getPackageName());
    		mBtnDecoPaperItem[i] = (ImageButton) findViewById(resId);
    		mBtnDecoPaperItem[i].setOnClickListener(decoItemClickListener);
    	}
    	
    	for(int i=0; i < Constants.DECO_TAPE_ITEM_SIZE; i++) {
    		int resId = getResources().getIdentifier("deco_tape_" + i, "id", getPackageName());
    		mBtnDecoTapeItem[i] = (ImageButton)findViewById(resId);
    		mBtnDecoTapeItem[i].setOnClickListener(decoItemClickListener);
    	}
    	
    	for(int i=0; i < Constants.DECO_STICKER_ITEM_SIZE; i++) {
    		int resId = getResources().getIdentifier("deco_sticker_" + i, "id", getPackageName());
    		mBtnDecoStickerItem[i] = (ImageButton) findViewById(resId);
    		mBtnDecoStickerItem[i].setOnClickListener(decoItemClickListener);
    	}
    	
    	
    }

    /**
     * 스타일메뉴에 사용하는 버튼들의 초기화 및 리스너를 설정합니다.
     * 
     */
    private void initStyleMenu() {
    	if(Constants.DEVELOPE_MODE) {
    		Utils.logPrint(getClass(), "initStyleMenu 호출");
    	}
    	
    	StyleMenuClickListener styleMenuClickListener = new StyleMenuClickListener();
    	
    	// Color 변수 초기화
    	int[] color = {getResources().getColor(R.color.white), getResources().getColor(R.color.yellow), getResources().getColor(R.color.orange),
    					 getResources().getColor(R.color.red), getResources().getColor(R.color.blue), getResources().getColor(R.color.green),
    					 getResources().getColor(R.color.navy_blue), getResources().getColor(R.color.purple), getResources().getColor(R.color.pink),
    					 getResources().getColor(R.color.brown), getResources().getColor(R.color.black)};
    	
    	mColorInteger = color.clone();
    	
    	// Color Style 버튼 초기화
        mBtnPickColor = new RelativeLayout[mColorName.length];
        
        for(int i=0; i < mColorName.length; i++) {
        	
        	int resId = getResources().getIdentifier("scrap_tool_color_" + mColorName[i], "id", getPackageName());
        	
        	mBtnPickColor[i] = (RelativeLayout)findViewById(resId);
        	mBtnPickColor[i].setOnClickListener(styleMenuClickListener);
        	mBtnPickColor[i].bringToFront();
        	
        }
        
        // Color Picker 초기화
        mBtnPickerColorLayout = (RelativeLayout)findViewById(R.id.scrap_tool_color_detail_edit_layout);
        mBtnPickerColorLayout.setOnClickListener(styleMenuClickListener);
        mBtnPickerColorLayout.bringToFront();
        
        mBtnColorDetail = (ImageView)findViewById(R.id.scrap_tool_color_detail_edit);
        
        mLiColorPickerLayout = (LinearLayout)findViewById(R.id.scrap_tool_color_picker_layout);
        mSelectedPoint = new PointF();
        
        // Pen Brush 버튼 초기화
        mBtnPenBrushStyle = new ImageButton[PenView.PEN_VIEW_BRUSH_SIZE];
        for(int i=0; i < mBtnPenBrushStyle.length; i++) {
        	int resId = getResources().getIdentifier("pen_brush_" + i, "id", getPackageName());
        	
        	mBtnPenBrushStyle[i] = (ImageButton)findViewById(resId);
        	mBtnPenBrushStyle[i].setOnClickListener(styleMenuClickListener);
        	mBtnPenBrushStyle[i].bringToFront();
        }
        
        // Text Style 버튼 초기화

        TextStyleCheckBoxListener textStyleListener = new TextStyleCheckBoxListener();
        
        mCheckTextViewStyle = new CheckBox[TextEditView.TEXT_STYLE_SIZE];
        for(int i=0; i < mCheckTextViewStyle.length; i++) {
        	int resId = getResources().getIdentifier("text_style_" + i, "id", getPackageName());
        	
        	mCheckTextViewStyle[i] = (CheckBox)findViewById(resId);
        	mCheckTextViewStyle[i].setOnClickListener(textStyleListener);
        	mCheckTextViewStyle[i].bringToFront();
        }
    }
    
    /**
     * SubMenu 에 사용할 Open, Close 애니메이션을 설정합니다.
     * 
     */
    private void initToolMenuAnimation() {
    	
        mAnimToolMenuOpen = AnimationUtils.loadAnimation(mContext, R.anim.toolmenu_open);
        mAnimToolMenuClose = AnimationUtils.loadAnimation(mContext, R.anim.toolmenu_close);
    }
    
    /**
     * 스크랩 페이지에 사용된 전역 데이터들을 초기화 한 뒤 스크랩 북을 종료하는 함수
     * 
     */
    private void initAndFinish() {
    	if(Constants.DEVELOPE_MODE) {
    		Utils.logPrint(getClass(), "initAndFinish 호출");
    	}
    	
    	mCurrentPageIndex = 0;
		mCurrentPageInfo = null;
		mCurrentPageModify = false;
		
		initMessagePool();
		
		// 모두 리사이클
    	if(mPenView != null) {
    		mPenView.executeRecycle();
    	}
		
    	if(mScrapViewLayout != null) {
			for(int i=0; i < mScrapViewLayout.getChildCount(); i++) {
				ScrapView childScrapView = (ScrapView) mScrapViewLayout.getChildAt(i);
				
				if(childScrapView != null) {
					childScrapView.executeRecycle();
				}
			}
    	}
		
    	if(mRelativeScrpLayout != null) {
    		Utils.recurisveRecycle(mRelativeScrpLayout);
    	}
		
		System.gc();
		
		if(ScrapIndexPage.ACTIVITY != null) {
			ScrapIndexPage.ACTIVITY.finish();
		}
		
		finish();
    }
    
    /**
     * Style - Color 에 체크 되어 있는 체크를 전부 해제하는 함수
     * 
     */
    private static void initColorCheck() {
    	
    	// Check 표시 전부 해제
		for(int i=0; i < mBtnPickColor.length; i++) {
			mBtnPickColor[i].setBackgroundResource(0);
		}
		
		if(mBtnPickerColorLayout != null) {
			mBtnPickerColorLayout.setBackgroundResource(0);
		}
    }
    
    
    /**
     * Style - Brush 에 체크 되어 있는 체크를 전부 해제하는 함수
     * 
     */
    private static void initPenBrushCheck() {
    	for(int i=0; i < mBtnPenBrushStyle.length; i++) {
    		int brushId = getBrushBgId(mContext, i, PenView.PEN_VIEW_BRUSH_STATE_OFF);
    		mBtnPenBrushStyle[i].setBackgroundResource(brushId);
    	}
    }
    
    /**
     * 공통으로 사용되는 MessagePool 의 값을 초기화 한뒤, 페이지에 사용되는 다양한 옵션 값들을 초기화 한다.
     *  init seekbar
     * 
     */
    private void initMessagePool() {
    	if(mMessagePool != null) {
    		mMessagePool.initMessagePool();
    	}
    	
    	mSeekWidth.setProgress(0);
    	
    }
    
    @Override
	public boolean onTouchEvent(MotionEvent event) {
		setToolMenuVisibility(false);
		setDecoMenuVisibility(false);
		
		if(getCurrentMode() == Constants.MENU_MODE_VIEW) {
			return mGestureDetector.onTouchEvent(event);
		}
		
		return super.onTouchEvent(event);
	}

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
    	super.onWindowFocusChanged(hasFocus);
    	
    	mScreenWidth = mRelativeScrpLayout.getWidth();
    	mScreenHeight = mRelativeScrpLayout.getHeight();
    }
    
    /**
     * 실제 페이지 데이터가 담겨 있는 AraryList<OriginScrapData> 에서 해당 페이지의 데이터를 복구하여 설정한다.
     * 
     * @param originData
     */
    private void applyOriginDataToLayout(OriginScrapData originData) {
    	if(Constants.DEVELOPE_MODE) {
    		Utils.logPrint(getClass(), "applyOriginDataToLayout 호출");
    	}
    	
    	initMessagePool();
		
		mMessagePool.setOriginScrapArray(mOriginScrapArray);

		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "------------apply 에서 origin 상태-------------");
		}
		
		for(int i=0; i < mOriginScrapArray.size(); i++) {
			Utils.logPrint(getClass(), i + " 번째 textArray 크기 : " + mOriginScrapArray.get(i).getTextViewPosArray().size());
		}
		
		mCurrentPageIndex = originData.getIndex();
		mCurrentPageInfo = originData.getInfo();
		mCurrentPageModify = false;

		ArrayList<CropViewPos> cropViewPosArray = originData.getCropViewPosArray();
		if(cropViewPosArray != null) {
			mMessagePool.setCropviewPosArray(cropViewPosArray);
		}
		
		ArrayList<TextViewPos> textViewPosArray = originData.getTextViewPosArray();
		if(textViewPosArray != null) {
			mMessagePool.setTextViewPosArray(textViewPosArray);
		}
		
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "불러오는 펜뷰 파일 경로 : " + originData.getPViewFilePath());
		}
		
		mMessagePool.setPenViewFilePath(originData.getPViewFilePath());
	}
	
    
    /**
     * 현재 레이아웃에 그려진 모든 View 들을 제거한 뒤, 재설정한다.
     * 
     */
    private void setScrapViewLayout() {
    	if(Constants.DEVELOPE_MODE) {
    		Utils.logPrint(getClass(), "setScrapViewLayout 호출");
    		Utils.logPrint(getClass(), "설정된 현재 페이지 : " + mCurrentPageIndex);
    	}
    	
    	// 초기화 및 Recycle 실행
		if(mScrapViewLayout != null) {
			Utils.recurisveRecycle(mScrapViewLayout);
			
			mScrapViewLayout.removeAllViews();
			mRelativeScrpLayout.removeView(mScrapViewLayout);
		}
		
		if(mTextEditLayout != null) {
			Utils.recurisveRecycle(mTextEditLayout);
			
			mTextEditLayout.removeAllViews();
			mRelativeScrpLayout.removeView(mTextEditLayout);
		}
		
		if(mPenView != null) {
			mPenView.executeRecycle();
			Utils.recurisveRecycle(mPenView);
			
			mRelativeScrpLayout.removeView(mPenView);
		}
		
		// 페이지 재설정
		// RootLayout 에 ScrapViewLayout 을 추가
        mScrapViewLayout = new ScrapViewLayout(mContext);
        mRelativeScrpLayout.addView(mScrapViewLayout);

        // RootLayout 에 TextEditLayout 를 추가
        mTextEditLayout = new TextEditLayout(mContext);
        mRelativeScrpLayout.addView(mTextEditLayout);
        
        // RootLayout 에 PenView 를 추가 (반드시 펜 뷰를 마지막에 추가할 것)
        mPenView = new PenView(mContext);
        mRelativeScrpLayout.addView(mPenView);
        
        //////////////////////////////////////////////////////////////////
        //
        /*
         * Dev ver 0.588
         * 컬러 설정
         */
        
        if(mColorStyleType == SELECT_PICKER_COLOR) {
        	setColorStyle(mColorStyleType, mPickedColor);
        }
        //
        //////////////////////////////////////////////////////////////////
        
        
        // 설정 값에 따른 모드 표시 내용 변경
        setCurrentMode(getCurrentMode());
        
        mRelativeScrpLayout.invalidate();
	}

    /**
     * 설정된 모드에 따라 모드 이벤트들을 실행하는 함수
     * 
     * @param mode
     */
    public void setCurrentMode(int mode) {
    	if(Constants.DEVELOPE_MODE) {
    		Utils.logPrint(getClass(), "setCurrentMode 호출");
    	}
    	
    	int tempMode = getCurrentMode();
    	mPrevMode = tempMode;
    	mCurrMode = mode;
    	
    	if(mode != Constants.MENU_MODE_EDIT && mPrevMode != Constants.MENU_MODE_EDIT) {
    		mPrevMode = Constants.MENU_MODE_EDIT;
    	}
    	
    	if(mMessagePool != null) {
    		mMessagePool.setMode(mode);
    	}
    	
    	initModeState();
    	
    	setToolMenuVisibility(false);
    	
    	switch(mode) {
			case Constants.MENU_MODE_VIEW:
				
				mBtnDisplayMode.setBackgroundResource(R.drawable.sub_menu_icon_view_off);
				mBtnQuickStyle.setEnabled(false);
				
				setPenMenuVisibility(false);
				setColorButtonVisibility(false);
				
				if(mCurrentPageModify) {
					if(Constants.DEVELOPE_MODE) {
						Utils.logPrint(getClass(), "페이지가 수정 되었으므로, 정보를 다시 저장합니다.");
					}
					
					savePage(mPageState, SAVE_MODE_ONLY, null);
					
					// mCurrentPageModify = false;
				}
				
				break;
				
			case Constants.MENU_MODE_PEN:
				mBtnDisplayMode.setBackgroundResource(R.drawable.sub_menu_icon_pen_off);
				mBtnQuickStyle.setEnabled(true);
				
				setPenMenuVisibility(true);
				
				mCurrentPageModify = true;
				break;
				
			case Constants.MENU_MODE_TEXT:
				mBtnDisplayMode.setBackgroundResource(R.drawable.sub_menu_icon_text_off);
				mBtnQuickStyle.setEnabled(true);
				
				setPenMenuVisibility(false);
				setColorButtonVisibility(true);
				
				mCurrentPageModify = true;
				
				break;
				
			case Constants.MENU_MODE_EDIT:
				mBtnDisplayMode.setBackgroundResource(R.drawable.sub_menu_icon_edit_off);
				mBtnQuickStyle.setEnabled(false);
				
				setPenMenuVisibility(false);
				setColorButtonVisibility(false);
				
				mCurrentPageModify = true;
				break;
    	}
    }
    
    /**
     * 외부에서 스크랩 모드를 변경시키기 위해 호출하는 함수
     * 
     * @param mode
     */
    public static void setCurrentModeExternalCall(int mode) {
    	
    	if(mMessagePool != null) {
    		mMessagePool.setMode(mode);
    	}
    	
    	switch(mode) {
			case Constants.MENU_MODE_VIEW:
				mBtnDisplayMode.setBackgroundResource(R.drawable.sub_menu_icon_view_off);
				mBtnQuickStyle.setEnabled(false);
				
				break;
				
			case Constants.MENU_MODE_PEN:
				mBtnDisplayMode.setBackgroundResource(R.drawable.sub_menu_icon_pen_off);
				mBtnQuickStyle.setEnabled(true);
				
				setPenMenuVisibility(true);
				
				mCurrentPageModify = true;
				break;
				
			case Constants.MENU_MODE_TEXT:
				mBtnDisplayMode.setBackgroundResource(R.drawable.sub_menu_icon_text_off);
				mBtnQuickStyle.setEnabled(true);
				
				setPenMenuVisibility(false);
				setColorButtonVisibility(true);
				
				mCurrentPageModify = true;
				
				break;
				
			case Constants.MENU_MODE_EDIT:
				mBtnDisplayMode.setBackgroundResource(R.drawable.sub_menu_icon_edit_off);
				mBtnQuickStyle.setEnabled(false);
				
				setPenMenuVisibility(false);
				setColorButtonVisibility(false);
				
				mCurrentPageModify = true;
				
				break;
    	}
    }

    private static int getCurrentMode() {

    	if(mMessagePool != null) {
    		return mMessagePool.getMode();
    	}
    	
    	return -1;
    }
    
    /**
     * 선택된 레이어들을 초기화 하는 함수
     */
    private void initSelectedLayer() {
    	// ScrapView 의 선택 영역을 초기화
		ScrapViewLayout.scrapInitDrawMode();
		
		// TextView 의 선택 영역을 초기화
		if(mTextEditLayout != null) {
			if(Constants.DEVELOPE_MODE) {
				Utils.logPrint(getClass(), "텍스트 선택 영역 초기화");
			}
			
			mTextEditLayout.clearAllFocus();
			mTextEditLayout.invalidate();
		}
    }
    
    
    /**
     * 현재의 모드 상태를 전부 초기화 시킨다.
     * 
     */
    private void initModeState() {
    	if(Constants.DEVELOPE_MODE) {
    		Utils.logPrint(getClass(), "initModeState 호출(현재 모드 : " + getCurrentMode() + ")");
    	}
    	
    	switch(getCurrentMode()) {
    	
    		case Constants.MENU_MODE_VIEW:
	    	case Constants.MENU_MODE_EDIT:
	    		
	    		initSelectedLayer();
	    		
	    		break;
	    	
	    	case Constants.MENU_MODE_TEXT:
	    		
	    		mTextEditLayout.removeEmptyEditView();
				setColorButtonVisibility(true);
				
	    		break;
	    		
	    	case Constants.MENU_MODE_PEN:
	    		
	    		if(mPenView.getPenViewState() == PenView.STATE_PEN) {
					setPenMenuVisibility(true);
				}
	    		
	    		break;
    	
    	}
		
		setTrashMenuVisibility(false);
		setLayerUpDownMenuVisibility(false);
		setStyleMenuVisibility(false);
		setDecoMenuVisibility(false);
    }
    
	@Override
	public void onClick(View v) {
		
		int id = v.getId();
		
		switch(id) {
		
			// 메뉴 버튼 클릭 시 이벤트 처리
			case R.id.scrap_page_menu_btn:
				
				initModeState();
				
				// 메뉴 Visible -> Gone, Gone -> Visible
				if(mViewToolMenu.getVisibility() == View.VISIBLE) {
					setToolMenuVisibility(false);
				} else {
					setToolMenuVisibility(true);
				}
				
				break;
			
			// Display Mode 버튼 클릭 시 이벤트 처리
			case R.id.scrap_page_display_mode:
				
				setCurrentMode(mPrevMode);

				break;
				
			// 지우개 버튼 클릭 시 이벤트 처리
			case R.id.scrap_page_pen_eraser:
				setToolMenuVisibility(false);
				setColorButtonVisibility(true);
				
				setPenViewState(mPenView.getPenViewState());
				
				mCurrentPageModify = true;
				
				break;
				
			// 레이어 업 클릭 시 이벤트 처리
			case R.id.scrap_page_layer_up:

				if(getCurrentMode() == Constants.MENU_MODE_EDIT) {
				
					for(int i=0; i < mScrapViewLayout.getChildCount(); i++) {
						ScrapView childView = (ScrapView)mScrapViewLayout.getChildAt(i);

						if(childView.getInnerTouchFlag()) {
							mScrapViewLayout.setLayerUp(i);
//							break;				//	break 문을 걸면, Layer 를 하나씩 올립니다.
						}
					}
					
					mCurrentPageModify = true;
					
				}
				
				break;
				
			// 레이어 다운 클릭 시 이벤트 처리
			case R.id.scrap_page_layer_down:
				
				if(getCurrentMode() == Constants.MENU_MODE_EDIT) {
					
					for(int i=0; i < mScrapViewLayout.getChildCount(); i++) {
						ScrapView childView = (ScrapView)mScrapViewLayout.getChildAt(i);

						if(childView.getInnerTouchFlag()) {
							mScrapViewLayout.setLayerDown(i);
							break;
						}
					}
					
					mCurrentPageModify = true;
					
				}
				
				break;
		}
	}
	
	private void setPenViewState(int penMode) {
		switch(penMode) {
			case PenView.STATE_PEN:		//	지우개 모드 설정,
				
				mPenView.setPenViewState(PenView.STATE_ERASER);			
				setPenViewIcon(PenView.STATE_ERASER);				
				
				//mBtnDisplayMode.setText("지우개 모드");
				mBtnDisplayMode.setBackgroundResource(R.drawable.sub_menu_icon_pen_off);
				
				break;
			
			case PenView.STATE_ERASER:	//	펜 모드 설정,
				
				mPenView.setPenViewState(PenView.STATE_PEN);
				setPenViewIcon(PenView.STATE_PEN);
				
				mBtnDisplayMode.setBackgroundResource(R.drawable.sub_menu_icon_pen_off);
				
				
				break;
		}
	}
	
	/**
	 * 정해져 있는 칼라를 선택 했을 시에 따른 이벤트를 처리하는 함수
	 *   - SELECT_FIXED_COLOR 일 경우, 두 번째 파라메터는 index
	 *   - SELECT_PICKER_COLOR 일 경우, 두 번째 파라메터는 int 형 color 값
	 * 
	 * @param color
	 */

	private static void setColorStyle(int type, int index) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(ScrapPage.class, "Type(" + type + "), index(" + index + ")");
		}
		
		// Check 표시 전부 해제
		initColorCheck();
		mColorStyleType = type;
		
		int color = 0;
		
		switch(type) {
		
			case SELECT_FIXED_COLOR:
				color = mColorInteger[index];
				mBtnPickColor[index].setBackgroundResource(R.drawable.style_color_select_icon);
				break;
			
			case SELECT_PICKER_COLOR:
				color = index;
				mBtnPickerColorLayout.setBackgroundResource(R.drawable.style_color_select_icon);
				break;
		}
		
		mMessagePool.setCurrentColor(color);			//	공유 풀에 현재 설정된 색상 지정
		mPenView.setCurrentColor(color);				//	펜 뷰의 색상을 변경
		
		// 텍스트 모드 일 경우, 컬러 설정 창을 닫지 않고 설정 값을 바로 적용해준다.
		if(getCurrentMode() == Constants.MENU_MODE_TEXT) {
			TextEditView selectedTextEditView = getSelectedTextEditView();
			
			// 현재 선택된 텍스트뷰가 존재한다면, 색상 값을 변경한다.
			if(selectedTextEditView != null) {
				selectedTextEditView.setTextColor(color);
			} else {
				
				if(type != SELECT_PICKER_COLOR) {
//					setStyleMenuVisibility(false);
				}
			}
		} 
		// 텍스트 모드가 아닐 경우, 컬러 설정 창을 닫는다.
		else {
			if(type != SELECT_PICKER_COLOR) {
//				setStyleMenuVisibility(false);
			}
		}

		
	}
	
	/**
	 * 텍스트 스타일을 선택 했을 시에 따른 이벤트를 처리하는 함수
	 * 
	 * @param style
	 */
	private static void setTextStyle(int style) {
		if(Constants.DEVELOPE_MODE)	{
			Utils.logPrint(ScrapPage.class, "setTextStyle(" + style + ") 호출");
		}
		
		mMessagePool.setFontStyle(style);
		
		switch(style) {
			case TextEditView.TEXT_STYLE_NORMAL:
				mCheckTextViewStyle[0].setChecked(true);
				mCheckTextViewStyle[1].setChecked(false);
				mCheckTextViewStyle[2].setChecked(false);
				break;
			case TextEditView.TEXT_STYLE_BOLD:
				mCheckTextViewStyle[0].setChecked(false);
				mCheckTextViewStyle[1].setChecked(true);
				mCheckTextViewStyle[2].setChecked(false);
				break;
			case TextEditView.TEXT_STYLE_ITALIC:
				mCheckTextViewStyle[0].setChecked(false);
				mCheckTextViewStyle[1].setChecked(false);
				mCheckTextViewStyle[2].setChecked(true);
				break;
			case TextEditView.TEXT_STYLE_ALL:
				mCheckTextViewStyle[0].setChecked(false);
				mCheckTextViewStyle[1].setChecked(true);
				mCheckTextViewStyle[2].setChecked(true);
				break;
			}
		
	}
	
	
	/**
	 * 펜 스타일을 선택 했을 시에 따른 이벤트를 처리하는 함수
	 * 
	 * @param state
	 */
	private void setPenBrush(int brushStyle) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "setPenStyle 호출");
		}
		
		// Check 표시 전부 해제
		initPenBrushCheck();
		
		// MessagePool 에 현재 선택된 브러쉬 스타일 설정
		if(mMessagePool != null) {
			mMessagePool.setCurrentBrush(brushStyle);
		}
		
		// PenView 에 현재 선택된 브러쉬 스타일을 적용
		if(mPenView != null) {
			mPenView.setPenStyle(brushStyle);
		}
		
		// 현재 선택된 스타일에 맞춰 체크 설정
		int selectedBrushId = getBrushBgId(mContext, brushStyle, PenView.PEN_VIEW_BRUSH_STATE_ON);
		mBtnPenBrushStyle[brushStyle].setBackgroundResource(selectedBrushId);
		
//		setStyleMenuVisibility(false);
	}
	
	/**
	 * 브러쉬 배경에 해당하는 Resource ID 를 리턴하는 함수
	 * 
	 * @param context
	 * @param style
	 * @param state
	 * @return
	 */
	private static int getBrushBgId(Context context, int style, int brushState) {
		
		String state = "on";
		
		if(brushState == PenView.PEN_VIEW_BRUSH_STATE_OFF) {
			state = "off";
		}
		
		int bgId = context.getResources().getIdentifier("style_brush_" + style + "_" + state, "drawable", context.getPackageName());
		
		return bgId;
	}
	
	public static boolean isColorMenuShow() {
		return mStyleMenuShowFlag;
	}
	
	// 서브 메뉴인 툴 메뉴의 나타냄을 설정하는 함수
    public static void setToolMenuVisibility(boolean flag) {
    	if(mViewToolMenu != null) {
    		// 스크랩 메뉴, 페이지 정보, 페이지 번호를 화면에 표시한다.
    		if(flag) {
    			mViewToolMenu.startAnimation(mAnimToolMenuOpen);
    			
    			mViewToolMenu.setVisibility(View.VISIBLE);
    			
    			// 현재 페이지에 대한 정보 표시 여부를 설정한다.
    			if(mCurrentPageInfo != null) {
    				mTvSubDisplayPageInfo.setText(mCurrentPageInfo);
    				mTvSubDisplayPageInfo.setVisibility(View.VISIBLE);
    			} else {
    				mTvSubDisplayPageInfo.setVisibility(View.GONE);
    			}
    			
    			// 현재 페이지 인덱스에 대한 표시 여부를 설정한다.
    			mTvPageIndex.setText((mCurrentPageIndex + 1) + Constants.PAGE_POSTFIX);
    			mTvPageIndex.setVisibility(View.VISIBLE);
    		} 
    		// 스크랩 메뉴와 페이지 번호를 화면에서 제거한다.
    		else {
    			
    			if(mViewToolMenu.getVisibility() == View.VISIBLE) {
    				mViewToolMenu.startAnimation(mAnimToolMenuClose);
    			}
    			
    			mViewToolMenu.setVisibility(View.GONE);
    			mTvPageIndex.setVisibility(View.GONE);
    		}
    	}
    }
    
    /**
     * 쓰레기통 아이콘을 flag 상태에 따라 변경한다.
     * 
     * @param flag
     */
    public static void changeTrashIcon(boolean flag) {
    	if(flag) {
    		mImgTrash.setBackgroundResource(R.drawable.icon_trash_open);
    	} else {
    		mImgTrash.setBackgroundResource(R.drawable.icon_trash_close);
    	}
    }
    
    /**
     * 화면에 쓰레기통 메뉴 표시 여부를 결정한다.
     * 
     * @param flag
     */
    public static void setTrashMenuVisibility(boolean flag) {
    	if(mImgTrash != null) {

    		if(flag) {
    			mImgTrash.setVisibility(View.VISIBLE);
    		} else {
    			mImgTrash.setVisibility(View.INVISIBLE);
    		}
    	}
    }
	
    /**
     * 화면에 레이어 업, 다운 메뉴 표시 여부를 결정한다.
     * 
     * @param flag
     */
	public static void setLayerUpDownMenuVisibility(boolean flag) {
		if(flag) {
			mBtnLayerUp.setVisibility(View.VISIBLE);
			mBtnLayerDown.setVisibility(View.VISIBLE);
		} else {
			mBtnLayerUp.setVisibility(View.GONE);
			mBtnLayerDown.setVisibility(View.GONE);
		}
		
	}
	
	/**
	 * 설정된 flag 에 따라 펜이나 지우개 아이콘을 화면에 표시한다.
	 * 
	 * true - 펜이나 지우개 아이콘을 화면에 표시한다.
	 *        펜 뷰의 기본 모드를 펜 모드로 변경한다.
	 *        펜 모드의 아이콘을 펜 아이콘으로 바꿔준다.
	 * 
	 * false - 펜이나 지우개 아이콘을 화면에서 삭제한다.
	 *         펜 뷰의 기본 모드를 펜 모드로 변경한다.
	 *         펜 모드의 아이콘을 펜 아이콘으로 바꿔준다.
	 *         
	 * @param flag
	 */
	public static void setPenMenuVisibility(boolean flag) {
		if(flag) {
			mBtnPenEraser.setVisibility(View.VISIBLE);
			mPenView.setPenViewState(PenView.STATE_PEN);
			setPenViewIcon(PenView.STATE_PEN);
		} else {
			mBtnPenEraser.setVisibility(View.GONE);
			mPenView.setPenViewState(PenView.STATE_PEN);
			setPenViewIcon(PenView.STATE_PEN);
		}
		
		setColorButtonVisibility(flag);
		
	}
	
	/**
	 * 컬러 설정 버튼을 화면에 표시할지 여부를 결정한다.
	 * 
	 * @param flag
	 */
	private static void setColorButtonVisibility(boolean flag) {
		if(Constants.DEVELOPE_MODE)	{
			Utils.logPrint(ScrapPage.class, "setColorButtonVisibility 호출 (flag : " + flag + ")");
		}
		
//		if(flag) {
//			mBtnColor.setVisibility(View.VISIBLE);
//		} else {
//			mBtnColor.setVisibility(View.GONE);
//		}
	}

	/**
	 * isVisibility에 따라, 스타일  메뉴를 화면에 표시할지 여부를 결정한다.
	 * 
	 * @param isVisibility
	 */
	public static void setStyleMenuVisibility(boolean isVisibility) {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(ScrapPage.class, "setStyleMenuVisibility 호출");
		}
		
		if(isVisibility) {
			
			mLiQuickStyleDetail.setVisibility(View.VISIBLE);
			mBtnQuickStyle.setBackgroundResource(R.drawable.quick_menu_icon_color_on);
			
			// 어떤 스타일의 레이아웃을 표시할지 여부를 설정
			if(mMessagePool.getMode() == Constants.MENU_MODE_PEN) {
				mPenBrushView.setVisibility(View.VISIBLE);
				mTextStyleView.setVisibility(View.GONE);
			} else if(mMessagePool.getMode() == Constants.MENU_MODE_TEXT) {
				mPenBrushView.setVisibility(View.VISIBLE);
				mTextStyleView.setVisibility(View.GONE);
			}
			
			// 지정된 브러쉬 스타일을 로드
			// 먼저 체크된 브러쉬 스타일을 초기화를 한 뒤,
			initPenBrushCheck();
			
			if(mMessagePool != null) {
				int brushStyle = mMessagePool.getCurrentBrush();
				int resId = getBrushBgId(mContext, brushStyle, PenView.PEN_VIEW_BRUSH_STATE_ON);
				mBtnPenBrushStyle[brushStyle].setBackgroundResource(resId);
			}
			
			
			// 지정된 색상을 로드
			// 먼저 체크 색상을 초기화를 한 뒤,
			initColorCheck();
			
			// 설정된 색상을 로드 한다.
			switch(mColorStyleType) {
			
				case SELECT_FIXED_COLOR:
					
					for(int i=0; i < mColorInteger.length; i++) {
						if(mMessagePool.getCurrentColor() == mColorInteger[i]) {
							mBtnPickColor[i].setBackgroundResource(R.drawable.style_color_select_icon);
						} else {
							mBtnPickColor[i].setBackgroundResource(0);
						}
					}
					
					break;
					
				case SELECT_PICKER_COLOR:
					mBtnPickerColorLayout.setBackgroundResource(R.drawable.style_color_select_icon);
					
					break;
			
			}

			// 텍스트 스타일을 선택하여 로드
			if(mMessagePool.getMode() == Constants.MENU_MODE_TEXT) {
				TextEditView selectedTextEditView = getSelectedTextEditView();
				
				if(selectedTextEditView != null) {
					setTextStyle(selectedTextEditView.getTextStyle());
				}
			}
			
			
		} else {
			mLiQuickStyleDetail.setVisibility(View.GONE);
			mLiColorPickerLayout.setVisibility(View.GONE);
			
			if(mMessagePool.getMode() == Constants.MENU_MODE_PEN || mMessagePool.getMode() == Constants.MENU_MODE_TEXT) {
				mBtnQuickStyle.setBackgroundResource(R.drawable.quick_menu_icon_color_off);
			} else {
				mBtnQuickStyle.setBackgroundResource(R.drawable.quick_menu_icon_color_disable);
			}
		}
	}
	
	/**
	 * isVisibility에 따라, 데코레이션 메뉴를 화면에 표시할지 여부를 결정한다.
	 * 
	 * @param isVisibility
	 */
	public static void setDecoMenuVisibility(boolean isVisibility) {
		
		// 화면에 데코레이션 메뉴를 표시한다.
		if(isVisibility) {
			mViewQuickDecoLayout.setVisibility(View.VISIBLE);
			mBtnQuickDecoration.setBackgroundResource(R.drawable.quick_menu_icon_deco_on);
		}
		// 화면에 데코레이션 메뉴를 표시하지 않는다.
		else {
			mViewQuickDecoLayout.setVisibility(View.GONE);
			mBtnQuickDecoration.setBackgroundResource(R.drawable.quick_menu_icon_deco_off);
		}
		
	}
	
	/**
	 * 설정된 mode 에 따라 펜 혹은 지우개 아이콘으로 변경한다.
	 * PenView.MODE_PEN - 펜 아이콘으로 변경한다.
	 * PenView.MODE_ERASER - 지우개 아이콘으로 변경한다.
	 * 
	 * @param mode
	 */
	public static void setPenViewIcon(int mode) {
		switch(mode) {
			case PenView.STATE_PEN:
				mBtnPenEraser.setBackgroundResource(R.drawable.icon_pen);
				break;
			case PenView.STATE_ERASER:
				mBtnPenEraser.setBackgroundResource(R.drawable.icon_eraser);
				break;
		}
	}
	
	public static int getLayoutWidth() {	return mScreenWidth;	}
	public static int getLayoutHeight() {	return mScreenHeight;	}
	public static RelativeLayout getRootLayout() {	return mRelativeScrpLayout;	}
	
	private void getIntenter() {
		Intent intent = getIntent();
		
		if(intent != null) {
			mSbookId = intent.getExtras().getInt(PUT_SCRAP_BOOK_ID);
			mSbookTitle = intent.getExtras().getString(PUT_SCRAP_BOOK_TITLE);
			mCurrentPageIndex = intent.getExtras().getInt(PUT_SCRAP_BOOK_PAGE_INDEX);
			mCurrentPageThemeIndex = intent.getExtras().getInt(PUT_SCRAP_BOOK_PAGE_THEME_INDEX);
			
			if(Constants.DEVELOPE_MODE)	{
				Utils.logPrint(getClass(), "스크랩 아이디 : " + mSbookId + 
					", 스크랩 북 제목 : " + mSbookTitle + ", 페이지 테마 인덱스 : " + mCurrentPageThemeIndex);
			}
			
		}
		
	}
	
	
	@Override
	public void onBackPressed() {
		
		if(mViewToolMenu.getVisibility() == View.VISIBLE) {
			setToolMenuVisibility(false);
		} else {
			SavePageAsync savePageAsync = new SavePageAsync(mPageState, SAVE_MODE_EXIT);
			savePageAsync.execute();
		}
	}
	
	private void savePage(int pageState, int saveMode, DialogInterface dialog) {
		SavePageAsync savePageAsync = null;
		
		if(dialog == null) {
			savePageAsync = new SavePageAsync(pageState, saveMode);
		} else {
			savePageAsync = new SavePageAsync(pageState, saveMode, dialog);
		}
		
		savePageAsync.execute();
	}
	
	private void nextPage() {
		Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.slide_right_to_left);
		anim.setAnimationListener(new PageAlphaAnimListener(PAGE_NEXT));
		mRelativeScrpLayout.startAnimation(anim);
		
	}
	
	private void prevPage() {
		Animation anim = AnimationUtils.loadAnimation(mContext, R.anim.slide_left_to_right);
		anim.setAnimationListener(new PageAlphaAnimListener(PAGE_PREV));
		mRelativeScrpLayout.startAnimation(anim);
		
	}
	
	private static TextEditView getSelectedTextEditView() {
		if(mTextEditLayout != null) {
			for(int i=0; i < mTextEditLayout.getChildCount(); i++) {
				TextEditView childView = (TextEditView)mTextEditLayout.getChildAt(i);
				
				if(childView.getSelectedFlag()) {
					if(Constants.DEVELOPE_MODE)	{
						Utils.logPrint(ScrapPage.class, "현재 선택된 텍스트 뷰는 " + i + " 입니다.");
					}
					
					return childView;
				}
			}
		}
		
		return null;
	}
	
	/**
	 * 현재 설정된 TextEditView 의 설정을 변경하는 함수
	 * 
	 * @param fontSize
	 * @param color
	 */
	public static void setTextViewColorFontState(int fontSize, int color) {
		if(Constants.DEVELOPE_MODE)	{
			Utils.logPrint(ScrapPage.class, "폰트 색상(" + color + "), 크기(" + fontSize + ")");
		}
		
		mMessagePool.setCurrentColor(color);
		mPenView.setCurrentColor(color);
		mPenView.setCurrentWidth(fontSize - TextEditView.FONT_DEFAULT_SIZE + PenView.PEN_DEFAULT_WIDTH);
		mSeekWidth.setProgress(fontSize - TextEditView.FONT_DEFAULT_SIZE);
		
		TextEditView selectedTextEditView = getSelectedTextEditView();
		
		// 현재 선택된 TextEditView 가 존재한다면, 
		if(selectedTextEditView != null) {
			
			// 스타일을 변경
			setTextStyle(selectedTextEditView.getTextStyle());
			
			// 컬러 메뉴의 체크를 변경
			int textColor = selectedTextEditView.getTextColor();
			
			int colorIdx = 0;
			boolean isFixedColor = false;
			for(int i=0; i < mColorInteger.length; i++) {
				if(mColorInteger[i] == textColor) {
					colorIdx = i;
					isFixedColor = true;
				}
			}
			
			if(isFixedColor) {
				setColorStyle(SELECT_FIXED_COLOR, colorIdx);
			} else {
				setColorStyle(SELECT_PICKER_COLOR, textColor);
			}
		}
		
	}
	
	/**
	 * 단순히, 현재 페이지에 대한 정보를 저장한다.
	 * 
	 * @author Acsha
	 */
	private class SavePageAsync extends AsyncTask<Void, Void, Void> {

		ProgressDialog proDig;
		DialogInterface dialog;
		int pageState;
		int saveMode;
		
		public SavePageAsync(int pageState, int saveMode) {
			this.pageState = pageState;
			this.saveMode = saveMode;
		}
		
		public SavePageAsync(int pageState, int saveMode, DialogInterface dialog) {
			this.pageState = pageState;
			this.saveMode = saveMode;
			this.dialog = dialog;
		}
		
		@Override
		protected void onPreExecute() {
			mRelativeRoot.invalidate();
			proDig = ProgressDialog.show(mContext, "", getString(R.string.scrap_page_dig_save_msg));
			
			// 페이지 저장 전, 페이지를 제외한 나머지 컴포넌트들은 전부 감췄다가 페이지 저장 후, 재표시해준다.
			mBtnDisplayMode.setVisibility(View.INVISIBLE);
			mBtnMenu.setVisibility(View.INVISIBLE);
			mViewQuickBarMenu.setVisibility(View.INVISIBLE);
			
			if(saveMode == SAVE_MODE_ADD) {
				if(Constants.DEVELOPE_MODE) {
					Utils.logPrint(getClass(), "단순히 페이지를 추가할 경우라도, 무조건 페이지 수정을 true 로 변경");
				}
				
				mCurrentPageModify = true;
			}
			
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			if(Constants.DEVELOPE_MODE)	{
				Utils.logPrint(getClass(), "저장 중인 페이지 : " + mCurrentPageIndex);
				Utils.logPrint(getClass(), "페이지 수정 상황 확인 : " + mCurrentPageModify);
			}
			
			// 페이지가 수정이 되어있는 상태라면, 데이터를 저장한다.
			if(mCurrentPageModify) {
			
				// 현재 페이지에 작성된 펜 뷰를 파일로 저장한다.
				Bitmap penViewBitmap = mPenView.getPenBitmap();
				
				if(Constants.DEVELOPE_MODE)	{
					Utils.logPrint(getClass(), "저장 할 펜뷰 값 : " + penViewBitmap + ", filePath : " + mMessagePool.getPenViewFilePath());
				}
				
				if(penViewBitmap != null) {
					Utils.saveBitmapToFileCache(penViewBitmap, mMessagePool.getPenViewFilePath());
				}
				
				// 현재 페이지 화면을 Bitmap 타입의 SnapView 로 변경하여, 파일로 저장해 놓는다
				Bitmap pageBitmap = Utils.getScreenShot(mRelativeRoot);
				if(Constants.DEVELOPE_MODE)	{
					Utils.logPrint(getClass(), "스크린 샷으로 가져온 결과 값 : " + pageBitmap);
				}
				
				String snapViewFilePath = Utils.getSnapViewFilePathName(mSbookId, mCurrentPageIndex);
				
				if(Constants.DEVELOPE_MODE)	{
					Utils.logPrint(getClass(), "저장 파일 : " + pageBitmap + ", 스냅샷 파일 경로 : " + snapViewFilePath);
				}
				
				if(pageBitmap != null) {
					if(snapViewFilePath != null) {
						if(mPageState == LOAD_NEW_PAGE) {
							// 페이지 중간에 새로 추가할 경우, 스냅샷 파일명을 변경해준다.
							if(mCurrentPageIndex < mOriginScrapArray.size()) {
								for(int i=mOriginScrapArray.size()-1; i >= mCurrentPageIndex; i--) {
									
									File file = new File(Utils.getSnapViewFilePathName(mSbookId, i));
									file.renameTo(new File(Utils.getSnapViewFilePathName(mSbookId, i+1)));
								}
							}
						}
						
						// 현재 페이지에 해당하는 스냅샷을 저장한다.
						Utils.saveBitmapToFileCache(pageBitmap, snapViewFilePath);
					} else {
						Toast.makeText(mContext, getString(R.string.scrap_page_dig_snap_shot_save_fail), Toast.LENGTH_LONG);
					}
				}
				
				OriginScrapData originData = 
					new OriginScrapData(mCurrentPageIndex, mCurrentPageInfo, 
							(ArrayList<CropViewPos>) mMessagePool.getCropViewPosArray().clone(), 
							(ArrayList<TextViewPos>) mMessagePool.getTextViewPosArray().clone(), 
							mMessagePool.getPenViewFilePath());
				
				// 데이터베이스에 저장한다.
				SaveScrapData saveData = Utils.convertDataOriginToSave(originData);
				Utils.saveScrapPageToDB(mContext, mSbookTitle, saveData, pageState);
				Utils.updatePageTheme(mContext, mSbookTitle, mCurrentPageThemeIndex);
				
				if(mOriginScrapArray.isEmpty()) {
					if(Constants.DEVELOPE_MODE)	{
						Utils.logPrint(getClass(), "mOriginScrapArray 가 비어있으므로, originData 를 추가합니다.");
					}
					
					mOriginScrapArray.add(originData);
				} else {
					if(mPageState == LOAD_NEW_PAGE) {
						if(Constants.DEVELOPE_MODE)	{
							Utils.logPrint(getClass(), "현재 페이지 상태는 새 페이지 작성 상태임으로, originData 를 추가합니다.");
						}
						
						// 만약 페이지의 중간에 새로운 페이지를 추가해야 한다면, 뒷 페이지들의 넘버링을 바꿔준 뒤, 해당 index 에 add 합니다.
						if(mCurrentPageIndex < mOriginScrapArray.size()) {
							if(Constants.DEVELOPE_MODE)	{
								Utils.logPrint(getClass(), "페이지 넘버링 변경 시작");
							}
							
							for(int i = mCurrentPageIndex; i < mOriginScrapArray.size(); i++) {
								OriginScrapData item = mOriginScrapArray.get(i);
								mOriginScrapArray.get(i).setIndex(item.getIndex() + 1);
							}
							
							if(Constants.DEVELOPE_MODE)	{
								Utils.logPrint(getClass(), "페이지 넘버링 변경 완료");
								for(int i=0; i < mOriginScrapArray.size(); i++) {
									Utils.logPrint(getClass(), "변경된 페이지 넘버링 : " + mOriginScrapArray.get(i).getIndex());
								}
							}
							
							mOriginScrapArray.add(mCurrentPageIndex, originData);
							
							if(Constants.DEVELOPE_MODE)	{
								Utils.logPrint(getClass(), "새로운 페이지 추가 후 현재 상태");
								for(int i=0; i < mOriginScrapArray.size(); i++) {
									Utils.logPrint(getClass(), "새로운 페이지 추가 후 넘버링 : " + mOriginScrapArray.get(i).getIndex());
								}
							}
						} 
						// 현재 저장해야하는 페이지가, 스크랩 북의 마지막 페이지에 추가되는 것이라면, ArrayList 에 단순히 add 합니다.
						else if(mCurrentPageIndex == mOriginScrapArray.size()) {
							mOriginScrapArray.add(originData);
						}
						
					} else {
						if(Constants.DEVELOPE_MODE)	{
							Utils.logPrint(getClass(), "현재 페이지 상태는 작성된 페이지 수정중임으로, originData 를 " + mCurrentPageIndex + "에 재설정합니다.");
						}
						
						mOriginScrapArray.set(mCurrentPageIndex, originData);
					}
				}
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(proDig.isShowing()) {
				proDig.dismiss();
			}
			
			if(dialog != null) {
				dialog.dismiss();
			}
			
			// 스크린 샷을 위해 감췄던 메뉴들 재표시
			mBtnDisplayMode.setVisibility(View.VISIBLE);
			mBtnMenu.setVisibility(View.VISIBLE);
			mViewQuickBarMenu.setVisibility(View.VISIBLE);
			
			switch(saveMode) {
				case SAVE_MODE_ONLY:				//	단지 페이지를 저장만 한다.
					if(Constants.DEVELOPE_MODE)	{
						Utils.logPrint(getClass(), "페이지를 단순히 저장만 합니다.");
					}
					
					mPageState = LOAD_MODIFY_PAGE;
					mCurrentPageModify = false;
					break;
			
				case SAVE_MODE_ADD:					//	새로운 페이지를 추가한다.
					if(Constants.DEVELOPE_MODE)	{
						Utils.logPrint(getClass(), "페이지를 새롭게 추가합니다.");
					}
					
					mMessagePool.setPenViewFilePath(null);
					
					mPageState = LOAD_NEW_PAGE;
					mCurrentPageIndex = mCurrentPageIndex+1;
					mCurrentPageInfo = null;
					mCurrentPageModify = false;

					// 스크랩 페이지 상태값 초기화
					initMessagePool();
					
					// 페이지 재설정
					setScrapViewLayout();
					
					
					break;
					
				case SAVE_MODE_SHARE:
					if(Constants.DEVELOPE_MODE)	{
						Utils.logPrint(getClass(), "페이지 공유를 위해 단순히 저장합니다.");
					}
					
					mPageState = LOAD_MODIFY_PAGE;
					mCurrentPageModify = false;
					
					Intent intent = new Intent(ScrapPage.this, ShareImageActivity.class);
					intent.putExtra(ShareImageActivity.PUT_SHARE_SBOOK_ID, mSbookId);
					startActivity(intent);
					
					break;
					
				case SAVE_MODE_EXIT:				//	저장한 뒤 종료한다.
					if(Constants.DEVELOPE_MODE)	{
						Utils.logPrint(getClass(), "페이지를 저장 뒤 종료합니다.");
					}
					
					initAndFinish();
					
					break;
			}
		} 
	}
	
	/**
	 * 현재 페이지를 데이터베이스에서 삭제한 뒤, 다음 이벤트를 처리합니다.
	 * 
	 * @author Acsha
	 */
	class DeletePageAsync extends AsyncTask<Void, Void, Void> {

		ProgressDialog proDig;
		DialogInterface dialog;
		
		public DeletePageAsync(DialogInterface dialog) {
			this.dialog = dialog;
		}
		
		@Override
		protected void onPreExecute() {
			proDig = ProgressDialog.show(mContext, "", getString(R.string.scrap_page_dig_del_msg));
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			if(Constants.DEVELOPE_MODE)	{
				Utils.logPrint(getClass(), "삭제할 페이지 인덱스 : " + mCurrentPageIndex);
			}
			
			// 현재 저장된 펜 뷰 파일을 삭제합니다.
			String pViewFilePath = mMessagePool.getPenViewFilePath();
			if(pViewFilePath != null) {
				File file = new File(pViewFilePath);
				file.delete();
			}
			
			// 현재 저장된 스냅 샷 파일을 삭제합니다.
			String snapViewFilePath = Utils.getSnapViewFilePathName(mSbookId, mCurrentPageIndex);
			if(snapViewFilePath != null) {
				File file = new File(snapViewFilePath);
				file.delete();
			}
			
			Utils.deleteAndLoadPage(mContext, mSbookTitle, mCurrentPageIndex);
			
			// 중간 페이지가 삭제 되었을 경우,
			if(mCurrentPageIndex < (mOriginScrapArray.size() - 1)) {
				if(Constants.DEVELOPE_MODE) {
					Utils.logPrint(getClass(), "중간 페이지가 삭제 되었습니다.");
				}
				
				for(int i = mCurrentPageIndex+1; i < mOriginScrapArray.size(); i++) {
					// 페이지 인덱스 순차 감소
					OriginScrapData item = mOriginScrapArray.get(i);
					mOriginScrapArray.get(i).setIndex(item.getIndex() - 1);
					
					// 스냅샷 파일명 순차 감소
					File file = new File(Utils.getSnapViewFilePathName(mSbookId, i));
					file.renameTo(new File(Utils.getSnapViewFilePathName(mSbookId, i-1)));
				}
				mOriginScrapArray.remove(mCurrentPageIndex);
			} 
			// 마지막 페이지가 삭제 되었을 경우,
			else if(mCurrentPageIndex == (mOriginScrapArray.size() - 1)) {
				if(Constants.DEVELOPE_MODE) {
					Utils.logPrint(getClass(), "마지막 페이지가 삭제 되었습니다.");
				}
				
				mOriginScrapArray.remove(mCurrentPageIndex);
				
				if(mOriginScrapArray.size() == 0) {
					mCurrentPageModify = false;
					mCurrentPageIndex = 0;
				} else {
					mCurrentPageIndex = mCurrentPageIndex - 1;
				}
			} 
			// 첫 페이지가 삭제 되었을 경우,
			else {
				if(Constants.DEVELOPE_MODE) {
					Utils.logPrint(getClass(), "첫 페이지를 삭제 합니다.");
				}
				
				// 1페이지가 삭제 되었을 경우,
				if(mCurrentPageIndex > 0) {
					if(Constants.DEVELOPE_MODE) {
						Utils.logPrint(getClass(), "1페이지가 삭제 되었을 경우");
					}
					
					mCurrentPageIndex = mCurrentPageIndex - 1;
				} 
				// 한번도 작성 되지 않은 0 페이지가 삭제 되었을 경우,
				else {
					if(Constants.DEVELOPE_MODE) {
						Utils.logPrint(getClass(), "한번도 작성되지 않은 혹은 아직 저장되지 않은, 0페이지가 삭제 되었을 경우");
					}
					
					mCurrentPageModify = false;
					mCurrentPageIndex = 0;
				}
			}
			
			if(Constants.DEVELOPE_MODE)	{
				Utils.logPrint(getClass(), "변경 후 페이지 인덱스 : " + mCurrentPageIndex);
				Utils.logPrint(getClass(), "현재 originScrapArray 크기 : " + mOriginScrapArray.size());
			}
			
			if(mOriginScrapArray.size() > 0) {
				mPageState = LOAD_MODIFY_PAGE;
			} else {
				mPageState = LOAD_NEW_PAGE;
			}
			
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(proDig.isShowing()) {
				proDig.dismiss();
			}
			
			dialog.dismiss();
			
			if(Constants.DEVELOPE_MODE)	{
				Utils.logPrint(getClass(), "화면에 표시해줄 페이지 인덱스 : " + mCurrentPageIndex);
			}
			
			if(mPageState == LOAD_NEW_PAGE) {
				// 페이지 상태 값 초기화
				initMessagePool();
				mCurrentPageInfo = null;
			} else if(mPageState == LOAD_MODIFY_PAGE) {
				applyOriginDataToLayout(mOriginScrapArray.get(mCurrentPageIndex));
			}
			
			setScrapViewLayout();
		}
	}
	
	private class PageAlphaAnimListener implements AnimationListener {
		
		int pageAction;
		
		public PageAlphaAnimListener(int pageAction) {
			this.pageAction = pageAction;
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			if(Constants.DEVELOPE_MODE)	{
				Utils.logPrint(getClass(), "o	nAnimationEnd 호출");
			}
			
			switch(pageAction) {
				case PAGE_PREV:
					mPageState = LOAD_MODIFY_PAGE;
					mCurrentPageIndex = mCurrentPageIndex - 1;
					applyOriginDataToLayout(mOriginScrapArray.get(mCurrentPageIndex));
					setScrapViewLayout();

					Animation prevAnim = AnimationUtils.loadAnimation(mContext, R.anim.slide_out_right_to_left);
					mRelativeScrpLayout.startAnimation(prevAnim);
					
					break;
					
				case PAGE_NEXT:
					mPageState = LOAD_MODIFY_PAGE;
					mCurrentPageIndex = mCurrentPageIndex + 1;
					applyOriginDataToLayout(mOriginScrapArray.get(mCurrentPageIndex));
					setScrapViewLayout();
					
					Animation nextAnim = AnimationUtils.loadAnimation(mContext, R.anim.slide_out_left_to_right);
					mRelativeScrpLayout.startAnimation(nextAnim);
					
					break;
			}
		}

		@Override
		public void onAnimationRepeat(Animation animation) {}
		public void onAnimationStart(Animation animation) {}
		
	}
	
	class ToolMenuClickListener implements OnClickListener {
		
		@Override
		public void onClick(View v) {
			
			int id = v.getId();
			
			// 모드 관련 버튼(뷰, 펜, 텍스트, 에디트 모드) 클릭 시 이벤트 처리
			// View 버튼 클릭 시,
			if(id == mBtnToolMenuView.getId()) {
				setCurrentMode(Constants.MENU_MODE_VIEW);
			} 
			// Pen 버튼 클릭 시,
			else if(id == mBtnToolMenuPen.getId()) {
				setCurrentMode(Constants.MENU_MODE_PEN);
			} 
			// Text 버튼 클릭 시,
			else if(id == mBtnToolMenuText.getId()) {
				setCurrentMode(Constants.MENU_MODE_TEXT);
			} 
			// Edit 버튼 클릭 시,
			else if(id == mBtnToolMenuEdit.getId()) {
				setCurrentMode(Constants.MENU_MODE_EDIT);
			}
			// 스크랩 버튼 클릭 시 이벤트 처리
			else if(id == mBtnToolMenuScrap.getId()) {
				setToolMenuVisibility(false);
				setStyleMenuVisibility(false);
				setDecoMenuVisibility(false);
				
				boolean enableMemory = Utils.getHeapMemoryCheck(mContext);

				if(enableMemory) {
					// 이미지 불러오기를 선택하기 위한 AlertDialog 호출
					String[] items = getResources().getStringArray(R.array.scrap_load_index);
					AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
					builder.setItems(items, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							
							switch(which) {
								case ScrapCrop.LOAD_GALLERY:
//									setPenMenuVisibility(false);
//									setColorButtonVisibility(false);
									
									Intent cropIntent = new Intent(ScrapPage.this, ScrapCrop.class);
									cropIntent.putExtra(ScrapCrop.PUT_SCRAP_CROP_SELECT, which);
									startActivity(cropIntent);
									
									break;
									
								case ScrapCrop.LOAD_CAMERA:
//									setPenMenuVisibility(false);
									
									Intent cameraIntent = new Intent(ScrapPage.this, CameraPicture.class);
									startActivity(cameraIntent);
									
									break;
							}
							
							mCurrentPageModify = true;
						}
					});
					
					builder.show();
				}
			}
			// 정보(도움말) 버튼 클릭 시 이벤트 처리
			else if(id == mBtnToolMenuInfo.getId()) {
				setToolMenuVisibility(false);
				
				DialogTutorial digTutorial = new DialogTutorial(mContext, DialogTutorial.TUTORIAL_PAGE_SECOND);
				
				WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
				lp.copyFrom(digTutorial.getWindow().getAttributes());
				
				int width = (int) (mScreenWidth * 0.9);
				int height = (int) ((width / DialogTutorial.DIALOG_IMAGE_RATIO_WIDTH) * DialogTutorial.DIALOG_IMAGE_RATIO_HEIGHT);
				
				lp.width = width;
				lp.height = height;
				
				digTutorial.show();
				
				Window window = digTutorial.getWindow();
				window.setAttributes(lp);
				
			}
			// 추가 버튼 클릭 시 이벤트 처리
			else if(id == mBtnToolMenuPageAdd.getId()) {
				setToolMenuVisibility(false);
				
				// 페이지를 추가할 거냐고 묻는 다이얼로그 출력
				AlertDialog.Builder addBuilder = new AlertDialog.Builder(mContext);
				addBuilder.setTitle(getString(R.string.scrap_page_dig_add_page_title));
				addBuilder.setMessage(getString(R.string.scrap_page_dig_add_page_msg));
				addBuilder.setPositiveButton(getString(R.string.confirm_dig_ok), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						setPenMenuVisibility(false);
						setColorButtonVisibility(false);
						
						// 현재까지 데이터를 저장한뒤, 새로운 페이지를 추가한다.
						savePage(mPageState, SAVE_MODE_ADD, dialog);
						
					}
				});
				addBuilder.setNegativeButton(getString(R.string.confirm_dig_cancel), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				
				addBuilder.show();
			}
			// 삭제 버튼 클릭 시 이벤트 처리
			else if(id == mBtnToolMenuPageDel.getId()) {
				setToolMenuVisibility(false);
				
				// 페이지를 삭제할 거냐고 묻는 다이얼로그 출력
				AlertDialog.Builder delBuilder = new AlertDialog.Builder(mContext);
				delBuilder.setTitle(getString(R.string.scrap_page_dig_del_page_title));
				delBuilder.setMessage(getString(R.string.scrap_page_dig_del_page_msg));
				delBuilder.setPositiveButton(getString(R.string.confirm_dig_ok), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						setPenMenuVisibility(false);
						setColorButtonVisibility(false);
						
						// 데이터베이스에서 현재 페이지를 삭제 한뒤, 이 후 페이지들의 페이지 넘버링을 변경한다.
						DeletePageAsync deleteAndLoadAsync = new DeletePageAsync(dialog);
						deleteAndLoadAsync.execute();
						
					}
				});
				delBuilder.setNegativeButton(getString(R.string.confirm_dig_cancel), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				
				delBuilder.show();
			}
			// Share 버튼 클릭 시 이벤트 처리
			else if(id == mBtnToolMenuShare.getId()) {
				setToolMenuVisibility(false);
				setPenMenuVisibility(false);
				setColorButtonVisibility(false);

				/*
				 * Dev. ver0.5893 (무조건 공유 버튼을 클릭 시, 현재 페이지를 저장하도록 한다.)
				 */
//				if(mCurrentPageModify) {
					mCurrentPageModify = true;
					
					SavePageAsync savePageAsync = new SavePageAsync(mPageState, SAVE_MODE_SHARE);
					savePageAsync.execute();
					
//				} else {
//					Intent intent = new Intent(ScrapPage.this, ShareImageActivity.class);
//					intent.putExtra(ShareImageActivity.PUT_SHARE_SBOOK_ID, mSbookId);
//					startActivity(intent);
//				}
			}
		}
	}
	
	class QuickBarMenuItemClickListener implements OnClickListener {
		
		@Override
		public void onClick(View v) {
			
			int id = v.getId();
			
			// 스크랩 퀵 버튼 클릭 시,
			if(id == mBtnQuickScrap.getId()) {
				initSelectedLayer();
				
				mBtnToolMenuScrap.performClick();
				
				mLiQuickStyleDetail.setVisibility(View.GONE);
			}
			// 데코레이션 퀵 버튼 클릭 시,
			else if(id == mBtnQuickDecoration.getId()) {
				initSelectedLayer();
				
				mDecoMenuShowFlag = mViewQuickDecoLayout.getVisibility() == View.VISIBLE ? false: true;
				setToolMenuVisibility(false);
				setStyleMenuVisibility(false);
				
				setDecoMenuVisibility(mDecoMenuShowFlag);
			}
			// 스타일 퀵 버튼 클릭 시,
			else if(id == mBtnQuickStyle.getId()) {
				mStyleMenuShowFlag = mLiQuickStyleDetail.getVisibility() == View.VISIBLE ? false : true;
				setToolMenuVisibility(false);
				setDecoMenuVisibility(false);
				
				if(getCurrentMode() == Constants.MENU_MODE_PEN || getCurrentMode() == Constants.MENU_MODE_TEXT) {
					setStyleMenuVisibility(mStyleMenuShowFlag);
				}
				
				mCurrentPageModify = true;
			}
			
		}
		
	}
	
	class DecoMenuClickListener implements OnClickListener {
		
		@Override
		public void onClick(View v) {
			
			int id = v.getId();
			
			// 데코 - 페이퍼 버튼 클릭 시,
			if(id == mBtnDecoPaper.getId()) {
				Utils.logPrint(getClass(), "클릭 페이퍼");
				
				mBtnDecoPaper.setBackgroundResource(R.drawable.deco_menu_icon_paper_on);
				mBtnDecoTape.setBackgroundResource(R.drawable.deco_menu_icon_tape_off);
				mBtnDecoSticker.setBackgroundResource(R.drawable.deco_menu_icon_sticker_off);
				
				// 데코레이션 페이퍼에 맞는 레이아웃 설정
				mViewDecoDetailPaper.setVisibility(View.VISIBLE);
				mViewDecoDetailTape.setVisibility(View.GONE);
				mViewDecoDetailSticker.setVisibility(View.GONE);
				
				
			} 
			// 데코 - 테이프 버튼 클릭 시,
			else if(id == mBtnDecoTape.getId()) {
				Utils.logPrint(getClass(), "클릭 테프");
				
				mBtnDecoPaper.setBackgroundResource(R.drawable.deco_menu_icon_paper_off);
				mBtnDecoTape.setBackgroundResource(R.drawable.deco_menu_icon_tape_on);
				mBtnDecoSticker.setBackgroundResource(R.drawable.deco_menu_icon_sticker_off);
				
				// 데코레이션 테이프 맞는 레이아웃 설정
				mViewDecoDetailPaper.setVisibility(View.GONE);
				mViewDecoDetailTape.setVisibility(View.VISIBLE);
				mViewDecoDetailSticker.setVisibility(View.GONE);
				
			}
			// 데코 - 스티커 버튼 클릭 시,
			else if(id == mBtnDecoSticker.getId()) {
				Utils.logPrint(getClass(), "클릭 스티커");
				
				mBtnDecoPaper.setBackgroundResource(R.drawable.deco_menu_icon_paper_off);
				mBtnDecoTape.setBackgroundResource(R.drawable.deco_menu_icon_tape_off);
				mBtnDecoSticker.setBackgroundResource(R.drawable.deco_menu_icon_sticker_on);
				
				// 데코레이션 스티커에 맞는 레이아웃 설정
				mViewDecoDetailPaper.setVisibility(View.GONE);
				mViewDecoDetailTape.setVisibility(View.GONE);
				mViewDecoDetailSticker.setVisibility(View.VISIBLE);
				
			}
			
		}
		
	}
	
	class DecoMenuItemClickListener implements OnClickListener {
		
		@Override
		public void onClick(View v) {
			
			int id = v.getId();
			
			// 페이퍼 버튼 클릭 시 이벤트 처리
			for(int i=0; i < Constants.DECO_PAPER_ITEM_SIZE; i++) {
				if(id == mBtnDecoPaperItem[i].getId()) {
					if(mCurrentPageThemeIndex != i) {
						mCurrentPageModify = true;
					}
					
					mCurrentPageThemeIndex = i;
					mRelativeRoot.setBackgroundResource(Utils.getId(mContext, "drawable", Constants.THEME_PAGE_PREFIX, mCurrentPageThemeIndex));
				}
			}
			
			// 테이프 버튼 클릭 시 이벤트 처리
			for(int i=0; i < Constants.DECO_TAPE_ITEM_SIZE; i++) {
				if(id == mBtnDecoTapeItem[i].getId()) {
					
					boolean enableMemory = Utils.getHeapMemoryCheck(mContext);
					
					if(enableMemory) {
						// Resource 에서 해당 이미지를 불러온 뒤,
						String filePath = Utils.getFilePathName(mContext, Constants.SAVE_FILE_PATH_SCRAP);
						int resId = getResources().getIdentifier("deco_tape_" + i, "drawable", getPackageName());
						
						// 비트맵으로 변환하여, 실제 SD 카드에 저장한다.
						Bitmap saveBitmap = BitmapFactory.decodeResource(getResources(), resId);
						Utils.saveBitmapToFileCache(saveBitmap, filePath);
						Utils.recycleBitmap(saveBitmap);
						
						// 저장된 파일 경로를 이용하여, CropViewPos 객체를 생성한 뒤,
						CropViewPos cropViewPos = new CropViewPos(filePath, 100, 100, 0);
						cropViewPos.setNoDegreeResize(saveBitmap.getWidth(), saveBitmap.getHeight());
						cropViewPos.setDegreeResize(saveBitmap.getWidth(), saveBitmap.getHeight());
						
						// 스크랩 페이지에 추가 한 뒤,
						mMessagePool.getCropViewPosArray().add(cropViewPos);
						
						// 스크랩 페이지를 갱신한다.
						ScrapViewLayout.setScrapViewToLayout();		//	ScrapViewLayout 재설정
						
						// 스크랩 페이지 갱신이 끝나면, 현재 모드를 에디트 모드로 변경한다.
						setCurrentMode(Constants.MENU_MODE_EDIT);
					} 
				}
			}
			
			// 스티커 버튼 클릭 시 이벤트 처리
			for(int i=0; i < Constants.DECO_STICKER_ITEM_SIZE; i++) {
				if(id == mBtnDecoStickerItem[i].getId()) {
					
					boolean enableMemory = Utils.getHeapMemoryCheck(mContext);
					
					if(enableMemory) {
						// Resource 에서 해당 이미지를 불러온 뒤,
						String filePath = Utils.getFilePathName(mContext, Constants.SAVE_FILE_PATH_SCRAP);
						int resId = getResources().getIdentifier("deco_sticker_" + i, "drawable", getPackageName());
						
						// 비트맵으로 변환하여, 실제 SD 카드에 저장한다.
						Bitmap saveBitmap = BitmapFactory.decodeResource(getResources(), resId);
						Utils.saveBitmapToFileCache(saveBitmap, filePath);
						Utils.recycleBitmap(saveBitmap);
						
						// 저장된 파일 경로를 이용하여, CropViewPos 객체를 생성한 뒤,
						CropViewPos cropViewPos = new CropViewPos(filePath, 100, 100, 0);
						cropViewPos.setNoDegreeResize(saveBitmap.getWidth(), saveBitmap.getHeight());
						cropViewPos.setDegreeResize(saveBitmap.getWidth(), saveBitmap.getHeight());
						
						// 스크랩 페이지에 추가 한 뒤,
						mMessagePool.getCropViewPosArray().add(cropViewPos);
						
						// 스크랩 페이지를 갱신한다.
						ScrapViewLayout.setScrapViewToLayout();		//	ScrapViewLayout 재설정
						
						// 스크랩 페이지 갱신이 끝나면, 현재 모드를 에디트 모드로 변경한다.
						setCurrentMode(Constants.MENU_MODE_EDIT);
					} 
					
				}
			}
			
		}
		
	}
	
	class StyleMenuClickListener implements OnClickListener {
		
		@Override
		public void onClick(View v) {
			
			int id = v.getId();
			
			// 칼라 버튼 클릭 시 이벤트 처리
			for(int i=0; i < mBtnPickColor.length; i++) {
				if(id == mBtnPickColor[i].getId()) {
					setColorStyle(SELECT_FIXED_COLOR, i);
				} 
			}
			
			// 펜 스타일 버튼 클릭 시 이벤트 처리
			for(int i=0; i < mBtnPenBrushStyle.length; i++) {
				if(id == mBtnPenBrushStyle[i].getId()) {
					setPenBrush(i);
				}
			}
			
			if(id == mBtnPickerColorLayout.getId()) {
				// Color Detail 단순히 클릭 만 했을 경우,
				initColorCheck();
				mColorStyleType = SELECT_PICKER_COLOR;
				
				// 컬러 픽커를 체크 표시
				mBtnPickerColorLayout.setBackgroundResource(R.drawable.style_color_select_icon);
				
				// 저장되어 있는 컬러를 불러와서 현재 컬러로 설정
				setColorStyle(SELECT_PICKER_COLOR, mPickedColor);
				
				// Color Detail View 레이아웃 컨트롤 및 설정
				if(mLiColorPickerLayout.getVisibility() == View.VISIBLE) {
					mLiColorPickerLayout.setVisibility(View.GONE);
				} else {
					mLiColorPickerLayout.removeAllViews();
					mLiColorPickerLayout.setVisibility(View.VISIBLE);
					
					mLiColorPickerLayout.post(new Runnable() {
						@Override
						public void run() {
							
							// Color Picker가 들어갈 레이어가 생성되면, Color Picker 를 생성한 뒤, 마진을 설정하여 추가한다. 
							final ColorPickerView colorPickerView = new ColorPickerView(mContext, mLiColorPickerLayout.getWidth(), mLiColorPickerLayout.getHeight(), mSelectedPoint);
							LinearLayout.LayoutParams lp = 
								new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
							
							int marginPx = (int) Utils.convertDpToPixel(mContext, 10);
							lp.setMargins(marginPx, marginPx, marginPx, marginPx);
							colorPickerView.setLayoutParams(lp);
							
							colorPickerView.setOnColorChangedListenner(new OnColorChangedListener() {
								@Override
								public void onColorChanged(int color, PointF selectedPoint) {
									mPickedColor = color;
									mSelectedPoint = selectedPoint;
									
									mBtnColorDetail.setBackgroundColor(color);
									setColorStyle(SELECT_PICKER_COLOR, color);
									
									mBtnColorDetail.invalidate();
								}
							});
							
							mLiColorPickerLayout.addView(colorPickerView);
							mLiColorPickerLayout.invalidate();
						}
					});
				}
			}
		}
	}
	
	class TextStyleCheckBoxListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			
			int style = TextEditView.TEXT_STYLE_NORMAL;
			
			
			for(int i=0; i < mCheckTextViewStyle.length; i++) {
				if(mCheckTextViewStyle[i].getId() == v.getId()) {
					mCheckTextViewStyle[i].setChecked(mCheckTextViewStyle[i].isChecked());
					
					if(!mCheckTextViewStyle[i].isChecked()) {
						if(mCheckTextViewStyle[1].isChecked() == false &&
								mCheckTextViewStyle[2].isChecked() == false) {
							
							mCheckTextViewStyle[0].setChecked(true);
							style = TextEditView.TEXT_STYLE_NORMAL;
						} else if(mCheckTextViewStyle[1].isChecked()) {
							style = TextEditView.TEXT_STYLE_BOLD;
						} else if(mCheckTextViewStyle[2].isChecked()) {
							style = TextEditView.TEXT_STYLE_ITALIC;
						}
					} else {
						if(i == TextEditView.TEXT_STYLE_NORMAL) {
							mCheckTextViewStyle[1].setChecked(false);
							mCheckTextViewStyle[2].setChecked(false);
							
							style = TextEditView.TEXT_STYLE_NORMAL;
							
						} else if(i == TextEditView.TEXT_STYLE_BOLD) {
							mCheckTextViewStyle[0].setChecked(false);
							
							style = TextEditView.TEXT_STYLE_BOLD;
							
							if(mCheckTextViewStyle[2].isChecked()) {
								style = TextEditView.TEXT_STYLE_ALL;
							}
							
							
						} else if(i == TextEditView.TEXT_STYLE_ITALIC) {
							mCheckTextViewStyle[0].setChecked(false);
							
							style = TextEditView.TEXT_STYLE_ITALIC;
							
							if(mCheckTextViewStyle[1].isChecked()) {
								style = TextEditView.TEXT_STYLE_ALL;
							}
						}
					}
					
				}
			}
			
			if(Constants.DEVELOPE_MODE) {
				Utils.logPrint(getClass(), "설정된 텍스트 스타일 : " + style);
			}
				
			
			// 텍스트 모드 일 경우, 컬러 설정 창을 닫지 않고 설정 값을 바로 적용해준다.
			if(getCurrentMode() == Constants.MENU_MODE_TEXT) {
				TextEditView selectedTextEditView = getSelectedTextEditView();
				
				// 현재 선택된 텍스트뷰가 존재한다면, 색상 값을 변경한다.
				if(selectedTextEditView != null) {
					selectedTextEditView.setTextStyle(style);
				} else {
					setStyleMenuVisibility(false);
				}
			} 
			
		}
		
	}
	
	
	class SeekBarListener implements OnSeekBarChangeListener {

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
			if(Constants.DEVELOPE_MODE) {
				Utils.logPrint(getClass(), "onPreogressChanged 호출");
			}
			
			int value = progress + PenView.PEN_DEFAULT_WIDTH;
			int fontSize = progress + TextEditView.FONT_DEFAULT_SIZE;
			
			mMessagePool.setCurrentWidth(value);
			mMessagePool.setFontSize(fontSize);
			mPenView.setCurrentWidth(value);
			
			// 현재 선택된 TextEditView 각 각에 맞는 폰트 사이즈를 설정한다.
			TextEditView selectedTextEditView = getSelectedTextEditView();
			
			if(selectedTextEditView != null) {
				selectedTextEditView.setTextSize(fontSize);
			}
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {}
		public void onStopTrackingTouch(SeekBar seekBar) {}
		
	}
	
	/**
	 * 페이지의 제스쳐(Fling)를 감지하기 위한 리스너
	 * 
	 * @author Acsha
	 */
	class PageGestureListener implements OnGestureListener {

		private final int SWIPE_MIN_DISTANCE = 120;
	    private final int SWIPE_MAX_OFF_PATH = 250;
	    private final int SWIPE_THRESHOLD_VELOCITY = 200;
	    
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,	float velocityY) {
			try {
	            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH) {
	                return false;
	            }

	            // 다음 페이지를 불러온다.
	            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	            	if(mCurrentPageIndex >= mOriginScrapArray.size() - 1) {
						Toast.makeText(mContext, getString(R.string.scrap_page_last_page), Toast.LENGTH_SHORT).show();
					} else {
						nextPage();
					}
	            	
	            }
	            // left to right swipe (이전 페이지로)
	            else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
	            	
	            	// 이전 페이지를 불러온다.
	            	if(mCurrentPageIndex <= 0) {
	            		boolean indexExistFlag = false;
	            		int originArraySize = mOriginScrapArray.size();
	            		
	            		for(int i=0; i < originArraySize; i++) {
	            			if(mOriginScrapArray.get(i).getInfo() != null) {
	            				if(mOriginScrapArray.get(i).getInfo().length() > 0) {
	            					indexExistFlag = true;
	            					break;
	            				}
	            			}
	            		}
	            		
	            		if(indexExistFlag) {
	            			if(Constants.DEVELOPE_MODE) {
	            				Utils.logPrint(getClass(), "인덱스 페이지가 존재합니다.");
	            			}
	            			
	            			Intent indexPageIntent = new Intent(ScrapPage.this, ScrapIndexPage.class);
	            			indexPageIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
	            			startActivity(indexPageIntent);
	            			
	            			finish();
	            		} else {
	            			if(Constants.DEVELOPE_MODE) {
	            				Utils.logPrint(getClass(), "인덱스 페이지가 존재하지 않습니다.");
	            			}
	            			
	            			/*
	            			 * Dev. ver0.5891
	            			 */
	            			
	            			Toast.makeText(mContext, getString(R.string.scrap_page_first_page), Toast.LENGTH_SHORT).show();
	            			
//	            			if(ScrapIndexPage.ACTIVITY != null) {
//	            				ScrapIndexPage.ACTIVITY.finish();
//	            			}
//	            			
//	            			finish();
	            		}
	            		
					} else {
						prevPage();
					}
	            	
	            }
	        } catch (Exception e) {
	            
	        }
			
			return true;
		}
		
		@Override
		public void onLongPress(MotionEvent e) {}
		public void onShowPress(MotionEvent e) {}
		public boolean onSingleTapUp(MotionEvent e) {	return false;	}
		public boolean onDown(MotionEvent e) {	return false;	}
		public boolean onScroll(MotionEvent e1, MotionEvent e2,	float distanceX, float distanceY) {	return false;	}
		
	}

}























