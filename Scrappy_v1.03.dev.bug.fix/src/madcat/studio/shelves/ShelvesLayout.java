package madcat.studio.shelves;

import madcat.studio.constants.Constants;
import madcat.studio.scrap.R;
import madcat.studio.shelvesbooks.ShelvesBooks;
import madcat.studio.shelvesbooks.aShelvesBooksAdapter;
import madcat.studio.utils.Utils;
import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;

public class ShelvesLayout extends TableLayout {
	
	public static final int MAX_ROW_COUNT							=	3;
	public static final int MAX_BOOKS_COUNT							=	3;

	private final int LAYOUT_INVALIDATE								=	0;
	private final int LAYOUT_UPDATE									=	1;
	
	private final int MAX_ANIM_DURATION								=	150;
	
	private Context mContext;
	private aShelvesBooksAdapter mAdapter;
	
	private ShelvesLayout mShelvesLayout;
	
	private int mCurrentPage;
	
	public ShelvesLayout(Context context) {	
		super(context);
		this.mContext = context;
		this.mShelvesLayout = this;
	}
	
	public ShelvesLayout(Context context, AttributeSet attrs) {	
		super(context, attrs);
		this.mContext = context;
		this.mShelvesLayout = this;
	}
	
	public void setSideNAdapter(aShelvesBooksAdapter adapter, int page) {
		this.mAdapter = adapter;
		this.mCurrentPage = page;
		
		if(getChildCount() != MAX_ROW_COUNT) {
			invalidateLayout();
		} else {
			updateLayout();
		}
		
	}
	
	public void invalidateLayout() {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "call invalidateLayout");
		}
		
		final int layoutState;
		
		if(getChildCount() == MAX_ROW_COUNT) {
			// Row의 책들만 삭제한다.
			for(int i=0; i < MAX_ROW_COUNT; i++) {
				LinearLayout innerRow = getRows(i).getLinearInnerRow();
				
				if(innerRow != null) {
					innerRow.removeAllViews();
				}
			}
			
			layoutState = LAYOUT_UPDATE;
		} else {
			layoutState = LAYOUT_INVALIDATE;
			this.removeAllViews();
		}
		
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "layoutState : " + layoutState);
		}
		
		// 화면 레이아웃 구성
		mShelvesLayout.post(new Runnable() {
			@Override
			public void run() {
				createRows(MAX_ROW_COUNT, mCurrentPage, layoutState);
			}
		});
	}
	
	public void updateLayout() {
		if(Constants.DEVELOPE_MODE) {
			Utils.logPrint(getClass(), "@call updateLayout");
		}
		
		// Row의 책들만 삭제한다.
		if(getChildCount() == MAX_ROW_COUNT) {
			for(int i=(MAX_ROW_COUNT-1); i >= 0; i--) {
				LinearLayout innerRow = getRows(i).getLinearInnerRow();
				
				if(innerRow != null) {
					Animation alpha = new AlphaAnimation(1.0f, 0.0f);
					alpha.setInterpolator(AnimationUtils.loadInterpolator(mContext, android.R.anim.accelerate_interpolator));
					alpha.setDuration(MAX_ANIM_DURATION);
					alpha.setAnimationListener(new ShelvesAnimationListener(i));
					innerRow.startAnimation(alpha);
				}
			}
		}
		
		
		
	}
	
	
	public void createRows(final int count, int page, final int state) {
		if(state == LAYOUT_INVALIDATE) {
			TableLayout.LayoutParams tlParams = 
				new TableLayout.LayoutParams(
						TableLayout.LayoutParams.MATCH_PARENT, 
						TableLayout.LayoutParams.MATCH_PARENT, 
						1.0f);
			
			// TableRow 추가
			if(getChildCount() == 0) {
				if(Constants.DEVELOPE_MODE) {
					Utils.logPrint(getClass(), "테이블 로우를 추가합니다. (총 추가할 로우 : " + count + ")");
				}
				
				for(int i=0; i < count; i++) {
					ShelvesRows row = new ShelvesRows(mContext, i);
					row.setLayoutParams(tlParams);
					
					addView(row);
				}
			}
		}
		
		final ShelvesRows rows = getRows(0);
		
		if(rows != null) {
			rows.post(new Runnable() {
				public void run() {
					if(Constants.DEVELOPE_MODE) {
						Utils.logPrint(getClass(), "로우 가로, 세로 : " + rows.getWidth() + ", " + rows.getHeight());
					}
					
					if(state == LAYOUT_INVALIDATE) {
						for(int i=0; i < count; i++) {
							// 행의 가로, 세로 길이 설정
							ShelvesRows rows = getRows(i);
							rows.setInitOutterLayout(rows.getWidth(), ShelvesRows.ROW_DEFAULT_HEIGHT);
							
							// 바닥 이미지 추가
							rows.getRelativeOutterRow().addView(createBottomSeperator());
	
							// 책 이미지가 들어갈 레이아웃 추가
							rows.setInitInnerLayout();
						}
					}
					
					// 바닥 이미지의 크기 구하기
					final ImageView bottomRow = (ImageView) rows.getRelativeOutterRow().getChildAt(0);
					
					bottomRow.post(new Runnable() {
						@Override
						public void run() {
							if(Constants.DEVELOPE_MODE) {
								Utils.logPrint(getClass(), "바닥 가로, 세로 : " + bottomRow.getWidth() + ", " + bottomRow.getHeight());
							}
							
							// 바닥 이미지의 세로 높이를 구해와서, 마진 값을 구한다.
							int wValue = 0, hValue = 0;
							int sMargin = 0, bMargin = 0;
							
							int hRatioNum = (int) ((rows.getHeight() - bottomRow.getHeight()) / ShelvesBooks.RATIO_HEIGHT);
							
							for(int i=hRatioNum; i > 0; i--) {
								wValue = (int) (hRatioNum * ShelvesBooks.RATIO_WIDTH);
								
								if(wValue * 3 < rows.getWidth()) {
									hValue = (int) (hRatioNum * ShelvesBooks.RATIO_HEIGHT);
									break;
								}
								
								hRatioNum--;
							}
							
							sMargin = (int) ((float)(rows.getWidth() - (wValue * MAX_BOOKS_COUNT)) / (MAX_BOOKS_COUNT + 1));
							bMargin = (int) ((float)bottomRow.getHeight() / 2.5);
							
							if(Constants.DEVELOPE_MODE) {
								Utils.logPrint(getClass(), "value(w,h) : " + wValue + ", " + hValue + 
										", ratio : " + hRatioNum + ", sMargin : " + sMargin + ", tMargin : " + bMargin);
							}
							
							mAdapter.setBooksSize(wValue, hValue, sMargin, bMargin);
							
							// 실제 책을 추가하는 코드
							int startIdx = mCurrentPage * (MAX_BOOKS_COUNT * MAX_ROW_COUNT);
							int endIdx = (mCurrentPage + 1) * (MAX_BOOKS_COUNT * MAX_ROW_COUNT);
							
							if(endIdx > mAdapter.getCount()) {
								endIdx = mAdapter.getCount();
							}
							
							// 등록된 책들을 책장에 추가합니다.
							int rowCount = 0;
							for(int i=startIdx; i < endIdx; i++) {
								if(i != startIdx && i % MAX_BOOKS_COUNT == 0) {
									if(getRows(rowCount).getLinearInnerRow().getChildCount() == MAX_BOOKS_COUNT) {
										rowCount += 1;
									}
								}
								
								getRows(rowCount).getLinearInnerRow().addView(mAdapter.getView(i, null, mShelvesLayout));
							}
							
							// 나머지 빈 공간을 덤프 레이아웃으로 추가하는 코드
							for(int i=endIdx; i < startIdx + (MAX_BOOKS_COUNT * MAX_ROW_COUNT); i++) {
								if(i != 0 && i % MAX_BOOKS_COUNT == 0) {
									rowCount += 1;
								}
								
								LinearLayout emptyView = new LinearLayout(mContext);
								LinearLayout.LayoutParams liParams = new LinearLayout.LayoutParams(wValue, hValue);
								liParams.width = wValue;
								liParams.height = hValue;
								liParams.leftMargin = sMargin;
								liParams.topMargin = bMargin;
								
								emptyView.setLayoutParams(liParams);
								
								getRows(rowCount).getLinearInnerRow().addView(emptyView);
								
							}
							
//							if(state == LAYOUT_UPDATE) {
								if(Constants.DEVELOPE_MODE) {
									Utils.logPrint(getClass(), "Layout_update state 호출");
								}
								
								for(int i=0; i < MAX_ROW_COUNT; i++) {
									Animation alpha = new AlphaAnimation(0.0f, 1.0f);
									alpha.setInterpolator(AnimationUtils.loadInterpolator(mContext, android.R.anim.decelerate_interpolator));
									alpha.setDuration(MAX_ANIM_DURATION);
									
									getRows(i).getLinearInnerRow().startAnimation(alpha);
								}
//							}
							
							invalidate();
						}
					});
					
						
						
				}
			});
		}
		
		
	}
	
	private ImageView createBottomSeperator() {
		final ImageView ivSeperatorBottom = new ImageView(mContext);
		
		RelativeLayout.LayoutParams rl = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
    																	 RelativeLayout.LayoutParams.WRAP_CONTENT);
		
		rl.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		
    	ivSeperatorBottom.setLayoutParams(rl);
    	ivSeperatorBottom.setBackgroundResource(R.drawable.shelves_seperator);
    	
    	return ivSeperatorBottom;
	}
	
	private ShelvesRows getRows(int index) {
		return (ShelvesRows) getChildAt(index);
	}
	
	public void setCurrentPage(int page) {
		this.mCurrentPage = page;
	}
	
	class ShelvesAnimationListener implements android.view.animation.Animation.AnimationListener {
		
		private int index;
		
		public ShelvesAnimationListener(int index) {
			this.index = index;
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			
			if(index == 0) {
				for(int i=(MAX_ROW_COUNT-1); i >= 0; i--) {
					LinearLayout innerRow = getRows(i).getLinearInnerRow();
					
					if(innerRow != null) {
						innerRow.removeAllViews();
					}
				}
				
				// 화면 레이아웃 구성
				mShelvesLayout.post(new Runnable() {
					@Override
					public void run() {
						createRows(MAX_ROW_COUNT, mCurrentPage, LAYOUT_UPDATE);
					}
				});
			}
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
		}

		@Override
		public void onAnimationStart(Animation animation) {
			
		}
		
	}
	
	
}
















