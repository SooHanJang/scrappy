package madcat.studio.shelves;

import madcat.studio.scrap.R;
import android.content.Context;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;

public class ShelvesRows extends TableRow {

	public static final int ROW_DEFAULT_HEIGHT						=	TableRow.LayoutParams.WRAP_CONTENT;
	
	private Context mContext;
	
	private int mIndex;
	
	private RelativeLayout mRlOutterRow;
	private LinearLayout mLiInnerRow;
	
	
	public ShelvesRows(Context context, int index) {
		super(context);
		this.mContext = context;
		this.mIndex = index;
		
		setInitOutterLayout(0, 0);
		
	}
	
	public void setInitOutterLayout(int width, int height) {
		mRlOutterRow = new RelativeLayout(mContext);
		TableRow.LayoutParams tlParams = new TableRow.LayoutParams(width, height);
		mRlOutterRow.setLayoutParams(tlParams);
		
		addView(mRlOutterRow);
		
	}
	
	public void setInitInnerLayout() {
		// 구분선 그림자 추가
		ImageView ivShadow = new ImageView(mContext);
		RelativeLayout.LayoutParams rlIvParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
																				 RelativeLayout.LayoutParams.WRAP_CONTENT);
		rlIvParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		
		ivShadow.setLayoutParams(rlIvParams);
		ivShadow.setBackgroundResource(R.drawable.shelves_seperator_shadow);
		mRlOutterRow.addView(ivShadow);
		
		
		// 실제로 사용될 Row Layout 추가
		mLiInnerRow = new LinearLayout(mContext);
		mLiInnerRow.setOrientation(LinearLayout.HORIZONTAL);
		mLiInnerRow.setGravity(Gravity.BOTTOM);

		RelativeLayout.LayoutParams rlParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 
																			   RelativeLayout.LayoutParams.MATCH_PARENT);
		mLiInnerRow.setLayoutParams(rlParams);
		
		mRlOutterRow.addView(mLiInnerRow);
		
	}
	
	public RelativeLayout getRelativeOutterRow() {
		return mRlOutterRow;
	}
	
	public LinearLayout getLinearInnerRow() {
		return mLiInnerRow;
	}
	
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
		
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
	
	public int getIndex() {
		return mIndex;
	}

}










